## 常用命令

- gradle clean 清空 build 目录
- gradle classes 编译业务代码和配置文件
- gradle test 编译测试代码，生成测试报告
- gradle build 构建项目
- gradle build -x test 跳过测试构建


## 修改仓库源

在 build.gradle 中修改，默认上从上到下依次查找仓库

也可以在 gradle 安装目录中的 init.d 中修改

init.d 在 windows 下会比较好找一些，

但是macOS在类似这样的路径下 ~/.gradle/wrapper/dists/gradle-7.4.2-bin/48ivgl02cpt2ed3fh9dbalvx8/gradle-7.4.2

```gradle
repositories {
    // 阿里云
    maven { url 'https://maven.aliyun.com/repository/public/' }
    // 本地
    mavenLocal()
    // 默认仓库
    mavenCentral()
}
```

有些教程是这样的

```grovy
allprojects {
    repositories {
        maven { url 'https://maven.aliyun.com/repository/public/' }
        mavenLocal()
        mavenCentral()
    }
}
```