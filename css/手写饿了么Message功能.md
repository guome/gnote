# Vue3 通过调用函数的方式使用组件（手写饿了么 Message 功能）

## 需求

项目中可能需要频繁用到某一组件，每次使用时要定义到模板，并写相同的渲染逻辑,例如消息提示组件。
我们知道 element ui 可以通过引入 Message 方法或直接调用 this.$massage 使用它的消息提示组件，我们也可以手写该功能。

## 定义要调用的 Vue 组件

定义一个`MessageCom.vue`组件,接收两个属性

- type: 消息的提示类型
- text: 消息的提示文本

## 定义要引入的函数文件

定义一个 Message.js, 这是我们之后在组件中需要引入的函数文件。
详解可看注释。

```js
import Message from "./MessageCom"; // 引入组件
import { createVNode, render } from "vue"; //导入需要的vue函数
// DOM容器
const div = document.createElement("div"); // 创建一个dom容器节点div
div.setAttribute("class", "xtx-massage-container"); // 为dom添加一个唯一标识类（无实质功能）
document.body.appendChild(div); // 容器追加到body中
//以上我们定义好了承装该组件虚拟dom的容器，接下来要把该组件添加到容器中，并导出为一个函数

let timer = null; // 定时器标识（用来3秒后关闭消息提示）
export default ({ type, text }) => {
  // 未来在使用函数时需要传入的组件的props属性
  const vnode = createVNode(Message, { type, text }); // 将组件编译为虚拟dom节点
  render(vnode, div); // 将虚拟dom添加到div容器中
  //必要步骤以完成，以下为关闭消息提示的定时器功能
  clearInterval(timer);
  timer = setTimeout(() => {
    render(null, div);
  }, 3000);
};
```

到此组件转化为函数调用的方式已经完成，接下来我们可以在组件中通过引入 `Message.js` 来直接使用该函数，但想无需引入，直接使用 `$message` 还需特殊处理。

## 组件中使用

两种方法

### 1. 通过引入函数使用

```js
import Message from "@/components/Message";

Message({ type: "error", text: "用户名或密码错误" });
```

### 2. 通过$massage 使用

首先需要把函数注册为全局函数。
`components / index.js` 我写在了注册全局组件的文件中。

```js
import Message from "./Message"; // 引入之前定义好的函数
export default {
  install(app) {
    // 定义原型函数
    app.config.globalProperties.$message = Message; // vue3.0语法
  },
};
```

在 `main.js` 中挂载

```js
import Components from './components'
createApp(App).use(Components).
```

到此就可以在组件中无需引入直接使用了。

> 注意:组合 api 中没有 this,因此需要拿到组件实例。

### 选项 api 中

```js
   created() {
     this.$message({ type: 'error', text: '用户名或密码错误' })
   }
```

### Vue3 组合 api 中

```js
setup(){
	const { proxy } = getCurrentInstance() // proxy为当前组件实例,getCurrentInstance为Vue方法
	proxy.$message({ type: 'error', text: '用户名或密码错误' })
}
```
