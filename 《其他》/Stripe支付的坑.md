```vue
<script setup lang="ts">
import { onMounted, onUnmounted, ref } from "vue";
import { checkoutSession } from "@/apis/pay.ts";
import { getLocalUserId } from "@/helps/business.ts";
import { useRoute, useRouter } from "vue-router";
import { openLoading } from "@/components/Loading/method.ts";
import { loadStripe, Stripe, StripeEmbeddedCheckout } from "@stripe/stripe-js";

const route = useRoute();
const router = useRouter();
const isLoadError = ref(false);

function back() {
  router.back();
}

// https://github.com/joshnuss/svelte-stripe/issues/97
onMounted(() => {
  createForm();
});

let stripe: Stripe | null;

let checkout: StripeEmbeddedCheckout | undefined;

async function createForm() {
  // This is a public sample test API key.
  // Don’t submit any personally identifiable information in requests made with this key.
  // Sign in to see your own test API key embedded in code samples.
  stripe = await loadStripe(import.meta.env.VITE_APP_STRIPE_KEY);

  await initialize();

  // Create a Checkout Session as soon as the page loads
  async function initialize() {
    const productId = Number(route.query.product_id);
    const backUrl = <string>route.query.back_url ?? "/";

    // ?back_url=${backUrl}&product_id=${productId}
    const url = window.location.origin + `/pay-result`;

    localStorage.setItem("pay-return-back-url", backUrl);

    const loading = openLoading();
    try {
      isLoadError.value = false;
      const { clientSecret } = await checkoutSession({
        cancelUrl: url,
        successUrl: url,
        checkoutUrl: url,
        productId: productId,
        userId: Number(getLocalUserId()),
      });

      checkout = await stripe?.initEmbeddedCheckout({
        clientSecret,
      });

      // Mount Checkout
      checkout?.mount("#checkout");
    } catch (e) {
      isLoadError.value = true;
    }
    loading.close();
  }
}
// svelte-stripe 库的解决方案
// https://github.com/joshnuss/svelte-stripe/pull/99/files
onUnmounted(() => {
  // 这玩意必须被销毁，不然就报以下错误:
  // EmbeddedCheckout IntegrationError: You cannot have multiple Embedded Checkout objects
  checkout?.destroy();
});

function reload() {
  window.location.reload();
}
</script>

<template>
  <!-- Display a payment form -->
  <div class="pay-page">
    <div class="header">
      <var-icon name="chevron-left" size="40" @click="back" />
    </div>
    <div id="checkout" v-show="!isLoadError">
      <!-- Checkout will insert the payment form here -->
    </div>
    <div class="error" v-show="isLoadError">
      <div class="error_text">error</div>
      <var-button type="primary" text @click="reload">reload page</var-button>
    </div>
  </div>
</template>

<style scoped lang="scss">
.pay-page {
  display: flex;
  flex-direction: column;
  height: 100vh;
}

.header {
  padding-top: 10px;
}

.error {
  display: flex;
  align-items: center;
  justify-content: center;
  flex-direction: column;
  flex: 1;
  color: #9e9e9e;

  &__text {
    margin-bottom: 20px;
  }
}

#checkout {
  flex: 1;
  overflow-y: auto;
}
</style>
```
