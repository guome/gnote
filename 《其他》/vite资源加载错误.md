# Failed to load module script: Expected a JavaScript module script but the server responded with a MIME type of "text/html". Strict MIME type

解决方案一：

```js
export default {
  // base: './',
  // 改为 /
  base: "/",
};
```

如果上面不行，就加这段代码

> Vite 在加载动态导入失败时会发出事件。 包含原始导入错误。如果调用 ，则不会抛出错误。

> Vite emits vite:preloadError event when it fails to load dynamic imports. event.payload contains the original import error. If you call event.preventDefault(), the error will not be thrown.

> 发生新部署时，托管服务可能会从以前的部署中删除资产。因此，在新部署之前访问过您的站点的用户可能会遇到导入错误。发生此错误的原因是，该用户设备上运行的资产已过时，并且它尝试导入相应的旧区块，该区块已被删除。此事件可用于解决此情况。

```js
window.addEventListener("vite:preloadError", (event) => {
  window.reload(); // for example, refresh the page
});
```
