# 调试 iOS Safari 崩溃“重复出现问题”

自从上次 iOS 更新以来，我也遇到了同样的问题，我发现这是一个内存问题，是由动态加载内容时发生的 CSS 不透明度转换引起的，这导致 iOS 浏览器以这种方式运行。我首先删除了内容上的所有 CSS 动画，然后将 will-change: opacity;添加到动态加载的内容中，这就解决了问题。

您可以尝试使用检查器工具监控页面的性能，并在触发动画时查看是否发生了崩溃。如果是这样的话，也许可以尝试禁用 CSS 代码。如果这解决了问题，但你想保留动画，你可以查看https://developer.mozilla.org/en-US/docs/Web/CSS/will-change。

据我所知，这次浏览器崩溃是由内存问题引起的，这有点模糊。CSS 动画可能是造成这种情况的原因，但这里可能有许多其他 memory-intensive 任务出错。

# varlet-ui 这个库有 这样的 will-change: opacity; 代码，会导致 ios 奔溃
