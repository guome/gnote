# iframe 点击.谷歌广告点击监听

```ts
interface CallbackType {
  (iframe: Iframe): any;
}

class Iframe {
  public element: HTMLIFrameElement;
  public cb: CallbackType;
  public hasTracked: boolean;

  constructor(element: HTMLIFrameElement, cb: CallbackType) {
    this.element = element;
    this.cb = cb;
    this.hasTracked = false;
  }
}

export class FrameOnClick {
  private readonly resolution: number;
  private iframes: Array<Iframe>;
  private interval: NodeJS.Timeout | null;

  constructor() {
    this.resolution = 200;
    this.iframes = [];
    this.interval = null;
  }

  /**
   * 延迟执行,
   * 有时会一上来就执行回调，为了避免这个情况才调用这个函数，一般来说调用 track 就可以
   * @param element
   * @param cb
   */
  public trackDelay(element: HTMLIFrameElement, cb: CallbackType) {
    let num = 0;
    this.track(element, (iframe) => {
      if (num === 0) {
        num++;
        iframe.hasTracked = false;
      } else {
        cb(iframe);
      }
    });
  }

  /**
   * 追踪 iframe 点击
   * @param element
   * @param cb
   */
  public track(element: HTMLIFrameElement, cb: CallbackType): void {
    const iframeItem = new Iframe(element, cb);
    this.iframes.push(iframeItem);

    // 点击广告时重置标志
    document.addEventListener("visibilitychange", () => {
      if (document.visibilityState === "hidden") {
        iframeItem.hasTracked = false;
      }
    });

    if (!this.interval) {
      this.interval = setInterval(() => this.checkClick(), this.resolution);
    }
  }

  // 检查当前 activeElement 是否为 iframe 并执行回调
  private checkClick(): void {
    const activeElement = document.activeElement as HTMLIFrameElement | null;

    if (activeElement) {
      this.iframes.forEach((iframe) => {
        if (activeElement === iframe.element) {
          if (!iframe.hasTracked) {
            iframe.cb.apply(window, [iframe]);
            iframe.hasTracked = true;
          }
        } else {
          iframe.hasTracked = false;
        }
      });
    }
  }

  // 清除 interval，防止内存泄漏
  public stopTracking(): void {
    if (this.interval) {
      clearInterval(this.interval);
      this.interval = null;
    }
  }
}
```

## 使用例子

```vue
<script setup lang="ts">
import { FrameOnClick } from "~/utils/FrameOnClick";

interface Props {
  adLayoutKey?: string;
  adSlot?: string;
  adClient?: string;
  width?: string;
  height?: string;
}

const props = withDefaults(defineProps<Props>(), {
  adSlot: "8552129318",
  adLayoutKey: "",
  width: "auto",
  height: "h-20",
});

const emits = defineEmits<{
  loadSuccess: [];
  loadFailed: [];
  adClick: [];
}>();

const runtimeConfig = useRuntimeConfig();

const clientId = computed(() => {
  return props.adClient || runtimeConfig.public.ADSENSE_CLIENT_ID;
});

const adsByGoogleRef = ref<HTMLElement>();

let observer: MutationObserver;

const emitAdClick = () => {
  // 使用例子
  const frameTracker = new FrameOnClick();
  const iframeEl = adsByGoogleRef.value?.querySelector("iframe");
  frameTracker.track(iframeEl as HTMLIFrameElement, () => {
    console.log("ad clicked!");

    // @ts-ignore
    dataLayer?.push({ event: "adclick" });
    emits("adClick");
  });
};

onMounted(() => {
  // 选择你想要监测的元素
  const targetElement = adsByGoogleRef.value;

  // 创建 MutationObserver 实例
  observer = new MutationObserver((mutationsList) => {
    mutationsList.forEach((mutation) => {
      if (
        mutation.type === "attributes" &&
        mutation.attributeName === "data-ad-status"
      ) {
        const newStatus = targetElement?.getAttribute("data-ad-status");
        if (newStatus === "filled") {
          emits("loadSuccess");
          emitAdClick();
        } else if (newStatus === "unfilled") {
          console.warn("ad load error", props.adSlot);
          emits("loadFailed");
          emitAdClick();
        }
      }
    });
  });

  // 配置观察选项
  const config = {
    attributes: true, // 监测属性变化
    attributeFilter: ["data-ad-status", "childList"], // 只监测 data-ad-status childList
  };

  // 开始观察目标元素
  targetElement && observer.observe(targetElement, config);
});

onUnmounted(() => {
  observer?.disconnect();
});
</script>

<template>
  <div class="flex justify-center">
    <!--<component-->
    <!--  is="script"-->
    <!--  async-->
    <!--  :src="`https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js?client=${clientId}`"-->
    <!--  crossorigin="anonymous"-->
    <!--/>-->
    <!--data-ad-format="auto"-->
    <!--
      不要加 data-ad-format 否则容器会被撑大
    -->
    <ins
      ref="adsByGoogleRef"
      class="adsbygoogle flex justify-center w-full bg-white"
      :class="[height]"
      :data-ad-client="clientId"
      :data-ad-slot="adSlot"
      data-full-width-responsive="true"
    />
    <component is="script">
      (adsbygoogle = window.adsbygoogle || []).push({});
    </component>
  </div>
</template>

<style scoped lang="scss">
ins.adsbygoogle[data-ad-status="unfilled"] {
  display: none !important;
}
</style>
```
