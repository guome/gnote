# 跳出到谷歌的链接

[intent:https://www.baidu.com#Intent;package=com.android.chrome;end]()

> package 指定包名

在 html 中添加以下代码

```html
<meta
  http-equiv="refresh"
  content="0; url=intent:https://iotv.dramavideos.top/download#Intent;package=com.android.chrome;action=android.intent.action.VIEW;end"
/>
```

之间跳出到浏览器，但是会有弹窗拦截

在 facebook 中用这个链接也可以

```html
<a href="fb://open-in-browser?url=https://iotv.dramavideos.top/download"
  >点击这里在浏览器中打开</a
>
```
