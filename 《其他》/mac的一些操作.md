# brew 安装的 java 目录

/opt/homebrew/Cellar/openjdk

# mac 环境变量

`~/.zprofile` 文件

> 备注：原先的.bash_profile 不生效了; 目前看是都生效的

例子：

```
export ANDROID_HOME=/Users/guojun/Library/Android/sdk
export PATH=${PATH}:${ANDROID_HOME}/tools
export PATH=${PATH}:${ANDROID_HOME}/platform-tools
```

# 查看当前使用的 shell

```shell
echo $SHELL
```

• 如果返回 /bin/zsh，则说明你在使用 Zsh。
• 如果返回 /bin/bash，则说明你在使用 Bash。

## Zsh

> 环境变量位置在 ~/.zshrc

## Bash

> 环境变量位置在 ~/.bash_profile

> 目前看 ~/.zshrc 和 ~/.bash_profile 都有效

# 全局 $PATH 设置

编辑 /etc/paths 文件

# java 的目录

/Users/guojun/Library/Java/JavaVirtualMachines
