```vue
<script setup lang="ts">
import { PlusIcon } from "@heroicons/vue/24/outline";
</script>

<template>
  <div
    class="relative overflow-hidden border size-10 flex justify-center items-center"
  >
    <PlusIcon class="size-6" />
    <input type="file" class="absolute inset-0 opacity-0" />
  </div>
</template>
```
