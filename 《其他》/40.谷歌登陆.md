# 在这个图中，你需要重点关注几个 url

- https://accounts.google.com/o/oauth2/v2/auth，这个是前端需要跳转到的页面，也就是 google 的登录页
- https://accounts.google.com/o/oauth2/token，这个是后端用 code 换 token 的链接
- https://www.googleapis.com/oauth2/v1/userinfo，这个是用 access_token 换 userInfo 对象的链接
- https://oauth2.googleapis.com/token，这个是用 refresh_token 置换新的 access_token 的链接

文档

https://developers.google.com/identity/protocols/oauth2/web-server?hl=zh-cn#httprest_1

```js
const params = new URLSearchParams({
  client_id: "xxxx",
  redirect_uri: "http://localhost:3005/login",
  response_type: "code",
  scope: "email",
  access_type: "offline",
});
const url = `https://accounts.google.com/o/oauth2/v2/auth?${params.toString()}`;
window.location.href = url;
```

# 查看授权

https://developers.google.com/oauthplayground/?hl=zh-cn

# 新建项目

https://console.cloud.google.com/apis/dashboard?project=vidoes-playlet&supportedpurview=project

# 配置

https://www.justauth.cn/guide/oauth/google/#_1-%E7%94%B3%E8%AF%B7%E5%BA%94%E7%94%A8
