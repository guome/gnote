# 网页端视频编辑

部署上线需要 nginx 配置

```nginx
location / {
    # 设置响应头
    add_header 'Cross-Origin-Embedder-Policy' 'require-corp';
    add_header 'Cross-Origin-Opener-Policy' 'same-origin';
}
```
