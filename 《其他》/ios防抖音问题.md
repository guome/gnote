# 仿抖音

使用 swipe 加 video 标签

## 几个坑

- 一个页面渲染多个 video 标签会有问题
- 默认 video 标签会不会响应点击事件，也不会冒泡到外层元素
- video 不能自动播放
- ios 下奔溃风险

## 填坑

一个页面渲染多个 video 标签会有问题

如果给每个 `swipe-item` 都渲染一个 `video` 会出现很多问题，最好是指渲染一个 `video`; 如下伪代码:

```vue
<script setup language="ts">
const currentIndex = ref(0);

function handleChange(index: number) {
  currentIndex.value = index;
}
</script>

<template>
  <van-swipe
    class="swipe my-swipe"
    ref="swipe"
    vertical
    :duration="duration"
    :loop="false"
    :show-indicators="false"
    @change="handleChange"
  >
    <van-swipe-item
      v-for="(item, index) in list"
      :key="item.url"
      class="my-swipe-item"
    >
      <!-- 不要这么写，会有很多问题 -->
      <!-- <video></video> -->
      <!-- 渲染多个video 标签问题可以记录当前索引 currentIndex 解决 -->
      <video v-if="index === currentIndex"></video>
    </van-swipe-item>
  </van-swipe>
</template>
```

> 在安卓下可以这么做，虽然也会出现很多问题，但是还是能解决的；但是在 ios 下就会出现很多坑；

以上代码会导致一个问题，屏幕所有的点击事件都无效了；(因为我们视频是全屏的); 需要解决第二个问题：

默认 `video` 标签会不会响应点击事件，也不会冒泡到外层元素

```vue
<script setup language="ts">
const currentIndex = ref(0);

function handleChange(index: number) {
  currentIndex.value = index;
}
</script>

<template>
  <van-swipe
    class="swipe my-swipe"
    ref="swipe"
    vertical
    :duration="duration"
    :loop="false"
    :show-indicators="false"
    @change="handleChange"
  >
    <van-swipe-item
      v-for="(item, index) in list"
      :key="item.url"
      class="my-swipe-item"
    >
      <!-- 渲染多个video 标签问题可以记录当前索引 currentIndex 解决 -->
      <video
        v-if="index === currentIndex"
        class="video"
        :src="item.url"
      ></video>
      <!-- 加一层div，盖住 video,不要让用户点到video,因为点到 video 会导致事件无效 -->
      <div class="video-box"></div>
    </van-swipe-item>
  </van-swipe>
</template>

<style>
.video-box,
.video {
  position: absolute;
  left: 0;
  top: 0;
  width: 100%;
  height: 100%;
}

.video-box {
  z-index: 2;
}
</style>
```

> 以上问题还没有解决：因为我们每次切换视频的时候需要让视频自动播放(除了第一个视频)

video 不能自动播放

每次滑动视频的时候主动触发一下 `play`，第一次触发也没用，需要用户点一下，但是用户点过以后，播放第二个视频就可以了自动播放了

```vue
<script setup language="ts">
const currentIndex = ref(0);
const video = ref();

function handleChange(index: number) {
  currentIndex.value = index;
  video.value.play();
}
</script>

<template>
  <van-swipe
    class="swipe my-swipe"
    ref="swipe"
    vertical
    :duration="duration"
    :loop="false"
    :show-indicators="false"
    @change="handleChange"
  >
    <van-swipe-item
      v-for="(item, index) in list"
      :key="item.url"
      class="my-swipe-item"
    >
      <!-- 渲染多个video 标签问题可以记录当前索引 currentIndex 解决 -->
      <video
        ref="video"
        v-if="index === currentIndex"
        class="video"
        :src="item.url"
      ></video>
      <!-- 加一层div，盖住 video,不要让用户点到video,因为点到 video 会导致事件无效 -->
      <div class="video-box"></div>
    </van-swipe-item>
  </van-swipe>
</template>

<style>
.video-box,
.video {
  position: absolute;
  left: 0;
  top: 0;
  width: 100%;
  height: 100%;
}

.video-box {
  z-index: 2;
}
</style>
```

> 其实到目前为止，在安卓下是没什么问题的；但是在 IOS, 自动播放是无效的；因为 video 标签每次都 `v-if` 销毁重建了；ios 必须是同一个 video 标签才会自动播放(第一次还是需要用户点击)

所以需要解决 ios video 不能自动播放

思路是 video 不能销毁；让它新建了以后就一直存在; 看代码：

```vue
<script setup language="ts">
const teleportEl = ref("body");

function setTeleportEl(index: number) {
  nextTick(() => {
    teleportEl.value = `.my-swipe-item:nth-child(${index})`;
  });
}

function handleChange(index: number) {
  // 每次 change 的时候修改节点, 就是挂载到对应的 swipe-item 中去
  setTeleportEl(index + 1);
}
</script>

<template>
  <van-swipe
    class="swipe my-swipe"
    ref="swipe"
    vertical
    :duration="duration"
    :loop="false"
    :show-indicators="false"
    @change="handleChange"
  >
    <van-swipe-item
      v-for="item in list"
      :key="item.url"
      class="my-swipe-item"
      ref="swipeItemRef"
    >
    </van-swipe-item>
  </van-swipe>

  <!-- 利用 Teleport 挂载到指定的 dom 节点去 -->
  <Teleport :to="teleportEl">
    <div class="video-play-box">
      <!-- 视频播放器, 把上面的 video 代码封装一下 -->
      <videoPlay
        v-if="currentEpisode"
        :src="currentEpisode.url"
        :poster="currentEpisode.cover"
      />
    </div>
  </Teleport>
</template>
```

> 以上就基本解决了 ios 和安卓的问题了

但是，在 ios 在可能会遇到 “重复出现问题” 这种问题，这个问题和项目没关系，是因为 ios 对这个 css `will-change: opacity;` 不兼容，只要有它存在，就会页面奔溃

所以删了它就好了。

但是，如果你用的第三方框架有这种代码就麻烦了；比如 `varlet`； 解决它要么换第三方框架，要么想办法删了它
