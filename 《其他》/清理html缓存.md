```nginx
location ~ .*\.(htm|html)?$ {
  #原来这样设置的不管用
  #expires -1;
  #现在改为，增加缓存
  add_header Cache-Control "private, no-store, no-cache, must-revalidate, proxy-revalidate";
  access_log on;
}
```
