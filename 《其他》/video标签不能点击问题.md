# video 不能点击， 以及在某些浏览器下 swipe 组件不能滑动问题

```vue
<template>
  <var-swipe
    class="swipe my-swipe"
    ref="swipe"
    vertical
    :duration="duration"
    :indicator="false"
    :loop="false"
    @change="handleChange"
  >
    <var-swipe-item
      v-for="(item, index) in list"
      :key="item.url"
      class="my-swipe-item"
    >
      <!-- 注意swipe内部最好同时只能有一个video标签，这样更容易处理 -->
      <template v-if="currentIndex === index && item.url">
        <!-- 这个很关键, video 标签必须在上层覆盖一层div, 下面样式也是关键的 , 这个在IOS上不管用 -->
        <div class="video-mask" @touchstart="handleClickVideoMask" />
        <!-- 
            video标签点击时间是无效的
            如果 video 是全屏的话，那么从 video 开始一直冒泡的所有元素的点击事件都会失效
            所以需要在上面盖一层，
            然后用css样式， video-mask:active  然后上层的div ， display: none
            伪类 :active 测试下来触发的时机是在 touchstart, 所以上面div的click事件是无效的
            但是video标签却可以响应点击事件，大概率是因为透传事件了
         -->
        <video
          v-if="item.url"
          :src="item.url"
          :poster="item.cover"
          width="100%"
          height="100%"
          :volume="1"
          @play="showControlBtn = false"
          @pause="showControlBtn = true"
          @ended="handleVideoEnd"
          @loadedmetadata="handleLoadedmetadata(item)"
          @error="handleVideoError(item)"
          @loadstart="handleVideoLoadStartError(item)"
        />
      </template>
      <!-- 其他情况最好是显示封面 -->
      <div v-else></div>
    </var-swipe-item>
  </var-swipe>
</template>

<style>
.video-mask {
  position: absolute;
  left: 0;
  top: 0;
  bottom: 70px;
  width: 100%;
  background-color: rgba(0, 0, 0, 0);
  z-index: 3;
}

.video-mask:active {
  display: none;
}
</style>
```
