https://www.cnblogs.com/cisum/p/10675367.html

demo

```vue
<script setup lang="ts">
const list = ref(new Array(52).fill(1));
// 一共三列
const cols = 3;
// 从哪行开始需要夸行合并。从1开始算
const rowStart = 2;
// 要合并的列，从哪列开始
const colStart = 1;
// 要合并几行
const mergeRow = 2;
// 要合并几列
const mergeCol = 2;
// 每隔几行就一个出现夸单元格
const intervalRow = 2;
// 上一次夸到了第几行
let lastRowEnd = 0;
// 上一次需要跨行的索引
let lastCellIndex = 0;

const listWithStyle = computed(() => {
  return list.value.map((it, index) => {
    const gridStyle: {
      gridRowStart?: number;
      gridRowEnd?: number;
      gridColumn: string;
    } = {
      gridColumn: `${colStart} / ${colStart + mergeCol}`,
    };
    let isStyle = false;

    const genStyle = (start = rowStart) => {
      lastCellIndex = index;
      isStyle = true;
      const end = start + intervalRow;
      lastRowEnd = end;
      Object.assign(gridStyle, { gridRowStart: start, gridRowEnd: end });
      console.log("gridStyle", gridStyle);
    };

    if (index >= (rowStart - 1) * cols + (colStart - 1)) {
      if (lastCellIndex === 0) {
        genStyle(rowStart);
      } else if (
        index %
          (lastCellIndex +
            cols * (intervalRow + mergeRow) -
            (mergeRow * mergeCol - 1)) ===
        0
      ) {
        genStyle(lastRowEnd + mergeRow);
      }
    }

    return {
      item: it,
      isStyle,
      gridStyle,
    };
  });
});
</script>

<template>
  <div class="index-page">
    <header>
      <img src="~/assets/images/header_img.png" alt="Discover Nuxt 3" />
    </header>
    <div class="grid-container">
      <div
        class="grid-item"
        v-for="(item, index) in listWithStyle"
        :style="[item.isStyle ? item.gridStyle : {}]"
      >
        <img src="~@/assets/images/header_img.png" />
      </div>
    </div>
  </div>
</template>

<style lang="scss" scoped>
$headerHeight: 65px;
.index-page {
  padding-top: $headerHeight;
  background: #02022c;
  min-height: 100vh;
  box-sizing: border-box;
}
header {
  position: fixed;
  left: 0;
  top: 0;
  display: flex;
  align-items: center;
  justify-content: center;
  width: 100%;
  height: $headerHeight;
  background: #ff8230;

  img {
    width: 128px;
  }
}

.grid-container {
  display: grid;
  grid-template-columns: repeat(v-bind(cols), 1fr);
  grid-gap: 15px;
  padding: 15px;
}

.grid-item {
  display: flex;
  min-height: 105px;
  align-items: center;
  justify-content: center;
  border-radius: 5px;
  background: #c8c7c7;

  img {
    width: 100%;
  }
}
</style>
```
