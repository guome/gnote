# textarea 自动高度

```vue
<script lang="ts" setup>
interface Props {
  modelValue: string;
  placeholder?: string;
  minHeight?: string;
}

const props = withDefaults(defineProps<Props>(), {
  modelValue: "",
  placeholder: "请输入内容",
  minHeight: "1.5em",
});

// 定义组件发出的事件
const emit = defineEmits<{
  (e: "update:modelValue", value: string): void;
  (e: "focus", event: FocusEvent): void;
  (e: "blur", event: FocusEvent): void;
  (e: "keydown", event: KeyboardEvent): void;
}>();

// 获取 textarea 元素的引用
const textareaRef = ref<HTMLTextAreaElement | null>(null);

// 内部数据，用于双向绑定
const internalValue = ref<string>(props.modelValue);

// 处理输入事件
const handleInput = (event: Event) => {
  const target = event.target as HTMLTextAreaElement;
  internalValue.value = target.value;
  emit("update:modelValue", target.value);
};

// 处理焦点事件
const handleFocus = (event: FocusEvent) => {
  emit("focus", event);
};

// 处理失焦事件
const handleBlur = (event: FocusEvent) => {
  emit("blur", event);
};
</script>

<template>
  <div class="relative common-style">
    <textarea
      ref="textareaRef"
      :placeholder="placeholder"
      class="resize-none w-full h-full absolute left-0 top-0 box-border common-style"
      v-model="internalValue"
      @input="handleInput"
      @focus="handleFocus"
      @blur="handleBlur"
    />
    <div class="invisible" :style="{ minHeight: minHeight }">
      {{ internalValue }}
    </div>
  </div>
</template>

<style scoped lang="scss">
.common-style {
  @apply p-2; // 外层的 div 的 padding 需要和 textarea 保持一致
}
</style>
```
