https://www.xiaojingge.com/archives/ef008289-8ab8-485c-8019-2703267c7beb#ds%E6%94%BE%E5%9C%A8%E5%93%AA%E9%87%8C%E5%90%88%E9%80%82

# dynamic-datasource 文档

dynamic-datasource-spring-boot-starter 是一个基于 springboot 的快速集成多数据源的启动器。

## 特性

- 支持 数据源分组 ，适用于多种场景，纯粹多库、读写分离、一主多从、混合模式。
- 支持数据库敏感配置信息 加密(可自定义) ENC()。
- 支持每个数据库独立初始化表结构 schema 和数据库 database。
- 支持无数据源启动，支持懒加载数据源（需要的时候再创建连接）。
- 支持 自定义注解 ，需继承 DS(3.2.0+)。
- 提供并简化对 Druid，HikariCp，BeeCp，Dbcp2 的快速集成。
- 提供对 Mybatis-Plus，Quartz，ShardingJdbc，P6sy，Jndi 等组件的集成方案。
- 提供 自定义数据源来源 方案（如全从数据库加载）。
- 提供项目启动后 动态增加移除数据源 方案。
- 提供 Mybatis 环境下的 纯读写分离 方案。
- 提供使用 spel 动态参数 解析数据源方案。内置 spel，session，header，支持自定义。
- 支持 多层数据源嵌套切换 。（ServiceA >>> ServiceB >>> ServiceC）。
- 提供 基于 seata 的分布式事务方案 。
- 提供 本地多数据源事务方案。

## 约定

- dynamic-datasource 只做 切换数据源 这件核心的事情，并不限制你的具体操作，切换了数据源可以做任何 CRUD。
- 配置文件所有以下划线 \_ 分割的数据源 首部 即为组的名称，相同组名称的数据源会放在一个组下。
- 切换数据源可以是组名，也可以是具体数据源名称。组名则切换时采用负载均衡算法切换。
- 默认的数据源名称为 master ，你可以通过 spring.datasource.dynamic.primary 修改。
- 方法上的注解优先于类上注解。
- DS 支持继承抽象类上的 DS，暂不支持继承接口上的 DS。

## 使用方法

1、引入 dynamic-datasource-spring-boot-starter

spring-boot 1.5.x、2.x.x

```xml
<dependency>
  <groupId>com.baomidou</groupId>
  <artifactId>dynamic-datasource-spring-boot-starter</artifactId>
  <version>${version}</version>
</dependency>
```

spring-boot3 及以上

```xml
<dependency>
  <groupId>com.baomidou</groupId>
  <artifactId>dynamic-datasource-spring-boot3-starter</artifactId>
  <version>${version}</version>
</dependency>
```

2、配置数据源

```yaml
spring:
  datasource:
    dynamic:
      primary: master # 设置默认的数据源或者数据源组,默认值即为master
      strict: false # 严格匹配数据源,默认false. true未匹配到指定数据源时抛异常,false使用默认数据源
      datasource:
        master:
          url: jdbc:mysql://xx.xx.xx.xx:3306/dynamic
          username: root
          password: 123456
          driver-class-name: com.mysql.cj.jdbc.Driver # 3.2.0开始支持SPI可省略此配置
        slave_1:
          url: jdbc:mysql://xx.xx.xx.xx:3307/dynamic
          username: root
          password: 123456
          driver-class-name: com.mysql.cj.jdbc.Driver
        slave_2:
          url: ENC(xxxxx) # 内置加密,使用请查看详细文档
          username: ENC(xxxxx)
          password: ENC(xxxxx)
          driver-class-name: com.mysql.cj.jdbc.Driver
        # ......省略
        # 以上会配置一个默认库master，一个组slave下有两个子库slave_1,slave_2
```

```yaml
# 多主多从
spring:
  datasource:
    dynamic:
      datasource:
        master_1:
        master_2:
        slave_1:
        slave_2:
        slave_3:
```

```yaml
# 纯粹多库（记得设置primary）
spring:
  datasource:
    dynamic:
      datasource:
        mysql:
        oracle:
        sqlserver:
        postgresql:
        h2:
```

```yaml
# 混合配置
spring:
  datasource:
    dynamic:
      datasource:
        master:
        slave_1:
        slave_2:
        oracle_1:
        oracle_2:
```

3、使用 @DS 切换数据源

@DS 可以注解在方法上或类上，同时存在就近原则，方法上注解 优先于 类上注解。

| 注解          | 结果                                                                          |
| ------------- | ----------------------------------------------------------------------------- |
| 没有@DS       | 默认数据源                                                                    |
| @DS("dsName") | dsName 可以为组名也可以为具体某个库的名称，组名则切换时采用负载均衡算法切换。 |

```java
@Service
@DS("slave")
public class UserServiceImpl implements UserService {

  @Autowired
  private JdbcTemplate jdbcTemplate;

  // 使用slave数据源
  public List selectAll() {
    return  jdbcTemplate.queryForList("select * from user");
  }

  // 使用slave_1数据源
  @Override
  @DS("slave_1")
  public List selectByCondition() {
    return  jdbcTemplate.queryForList("select * from user where age >10");
  }
}
```

## DS 放在哪里合适

DS 作为切换数据源核心注解，我应该把他注解在哪里合适？

这其实是初次接触多数据源的人常问的问题。

这其实没有一定的要求，只是有一些经验之谈。

首先开发者要了解的基础知识是，DS 注解是基于 AOP 的原理实现的，aop 的常见失效场景应清楚，比如内部调用失效，shiro 代理失效。

通常建议 DS 放在 serviceImpl 的方法上，如事务注解一样。

### 注解在 Controller 的方法上或类上

并不是不可以，作者并不建议的原因主要是 controller 主要作用是参数的检验等一些基础逻辑的处理，这部分操作常常并不涉及数据库。

### 注解在 service 的实现类的方法或类上

这是作者建议的方式，service 主要是对业务的处理， 在复杂的场景涉及连续切换不同的数据库。

如果你的方法有通用性，其他 service 也会调用你的方法。 这样别人就不用重复处理切换数据源。

### 注解在 Mapper 上

通常如果你某个 Mapper 对应的表只在确定的一个库，也是可以的，但是建议只注解在 Mapper 的类上。

我之前出现多线程失效场景的时候，就是在 Mapper 上加了注解解决的。

### 其他使用方式

#### 继承抽象类上的 DS

比如我有一个抽象 Service，我想实现继承我这个抽象 Service 下的子 Service 的所有方法除非重新指定，都用我抽象 Service 上注解的数据源。是否支持？

答：支持。

#### 继承接口上的 DS

3.4.1 开始支持， 但是需要注意的是，一个类能实现多个接口，如果多个接口都有 DS 会如何？

不知道，别这么干。一般不会有人这么干吧，想一想都知道会出问题。

## 连接池集成

传统多数据源集成连接池如果需要配置的参数较多，则手动编码量大，编程复杂。

dynamic-datasource 实现了常见数据源的参数配置，支持全局配置每个数据源继承。

通过本章您可以快速掌握不同连接池的集成配置方案和继承中可能遇见的问题。

### 连接池必读

- 每个数据源都有一个 type 来指定连接池。
- 每个数据源甚至可以使用不同的连接池，如无特殊需要并不建议。
- type 不是必填字段。

在没有设置 type 的时候系统会自动按以下顺序查找并使用连接池：

Druid > HikariCp > BeeCp > DBCP2 > Spring Basic。

```yml
spring:
  datasource:
    dynamic:
      primary: db1
      datasource:
        db1:
          url: jdbc:mysql://xx.xx.xx.xx:3306/dynamic
          username: root
          password: 123456
          driver-class-name: com.mysql.cj.jdbc.Driver
          type: com.zaxxer.hikari.HikariDataSource # 使用Hikaricp
        db2:
          url: jdbc:mysql://xx.xx.xx.xx:3307/dynamic
          username: root
          password: 123456
          driver-class-name: com.mysql.cj.jdbc.Driver
          type: com.alibaba.druid.pool.DruidDataSource # 使用Druid
        db3:
          url: jdbc:mysql://xx.xx.xx.xx:3308/dynamic
          username: root
          password: 123456
          driver-class-name: com.mysql.cj.jdbc.Driver
          type: cn.beecp.BeeDataSource # 使用beecp
```

### 集成 Druid

dynamic-datasource 能简单高效完成对 Druid 的集成并完成其参数的多元化配置。

- 各个库可以使用不同的数据库连接池，如 master 使用 Druid，slave 使用 HikariCP。
- 如果项目同时存在 Druid 和 HikariCP 并且未配置连接池 type 类型，默认 Druid 优先于 HikariCP。

#### 集成步骤

1、项目引入 druid-spring-boot-starter 依赖

```xml
<dependency>
    <groupId>com.alibaba</groupId>
    <artifactId>druid-spring-boot-starter</artifactId>
    <version>${version}</version>
</dependency>
```

2、排除原生 Druid 的快速配置类

注意：v3.3.3 及以上版本不用排除了。

```java
@SpringBootApplication(exclude = DruidDataSourceAutoConfigure.class)
public class Application {

  public static void main(String[] args) {
    SpringApplication.run(Application.class, args);
  }

}
```

某些 SpringBoot 的版本上面可能无法排除可用以下方式排除。

```yml
spring:
  autoconfigure:
    exclude: com.alibaba.druid.spring.boot.autoconfigure.DruidDataSourceAutoConfigure
```

3、参数配置

- 如果参数都未配置，则保持原组件默认值。
- 如果配置了全局参数，则每一个数据源都会继承对应参数。
- 每一个数据源可以单独设置参数覆盖全局参数。

```yml
spring:
  datasource:
    druid:
      stat-view-servlet:
        enabled: true
        loginUsername: admin
        loginPassword: 123456
    dynamic:
      druid: # 以下是支持的全局默认值
        initial-size:
        max-active:
        min-idle:
        max-wait:
        time-between-eviction-runs-millis:
        time-between-log-stats-millis:
        stat-sqlmax-size:
        min-evictable-idle-time-millis:
        max-evictable-idle-time-millis:
        test-while-idle:
        test-on-borrow:
        test-on-return:
        validation-query:
        validation-query-timeout:
        use-global-datasource-stat:
        async-init:
        clear-filters-enable:
        reset-stat-enable:
        not-full-timeout-retry-count:
        max-wait-thread-count:
        fail-fast:
        phyTimeout-millis:
        keep-alive:
        pool-prepared-statements:
        init-variants:
        init-global-variants:
        use-unfair-lock:
        kill-when-socket-read-timeout:
        connection-properties:
        max-pool-prepared-statement-per-connection-size:
        init-connection-sqls:
        share-prepared-statements:
        connection-errorretry-attempts:
        break-after-acquire-failure:
        filters: stat # 注意这个值和druid原生不一致，默认启动了stat
        wall:
          noneBaseStatementAllow:
          callAllow:
          selectAllow:
          selectIntoAllow:
          selectIntoOutfileAllow:
          selectWhereAlwayTrueCheck:
          selectHavingAlwayTrueCheck:
          selectUnionCheck:
          selectMinusCheck:
          selectExceptCheck:
          selectIntersectCheck:
          createTableAllow:
          dropTableAllow:
          alterTableAllow:
          renameTableAllow:
          hintAllow:
          lockTableAllow:
          startTransactionAllow:
          blockAllow:
          conditionAndAlwayTrueAllow:
          conditionAndAlwayFalseAllow:
          conditionDoubleConstAllow:
          conditionLikeTrueAllow:
          selectAllColumnAllow:
          deleteAllow:
          deleteWhereAlwayTrueCheck:
          deleteWhereNoneCheck:
          updateAllow:
          updateWhereAlayTrueCheck:
          updateWhereNoneCheck:
          insertAllow:
          mergeAllow:
          minusAllow:
          intersectAllow:
          replaceAllow:
          setAllow:
          commitAllow:
          rollbackAllow:
          useAllow:
          multiStatementAllow:
          truncateAllow:
          commentAllow:
          strictSyntaxCheck:
          constArithmeticAllow:
          limitZeroAllow:
          describeAllow:
          showAllow:
          schemaCheck:
          tableCheck:
          functionCheck:
          objectCheck:
          variantCheck:
          mustParameterized:
          doPrivilegedAllow:
          dir:
          tenantTablePattern:
          tenantColumn:
          wrapAllow:
          metadataAllow:
          conditionOpXorAllow:
          conditionOpBitwseAllow:
          caseConditionConstAllow:
          completeInsertValuesCheck:
          insertValuesCheckSize:
          selectLimit:
        stat:
          merge-sql:
          log-slow-sql:
          slow-sql-millis:
      datasource:
        master:
          username: root
          password: 123456
          driver-class-name: com.mysql.cj.jdbc.Driver
          url: jdbc:mysql://xx.xx.xx.xx:3306/dynamic?characterEncoding=utf8&useSSL=false
          druid: # 以下是独立参数，每个库可以重新设置
            initial-size: 20
            validation-query: select 1 FROM DUAL # 比如oracle就需要重新设置这个
            public-key: #（非全局参数）设置即表示启用加密,底层会自动帮你配置相关的连接参数和filter，推荐使用本项目自带的加密方法。
```

以下是我曾经配置过的示例：

```yml
# 数据源配置
spring:
    # 排除掉druid原生的自动配置
    autoconfigure:
        exclude: com.alibaba.druid.spring.boot.autoconfigure.DruidDataSourceAutoConfigure
    datasource:
    	druid:
            # 初始连接数
			initialSize: 5
			# 最小连接池数量
			minIdle: 10
			# 最大连接池数量
			maxActive: 20
			# 配置获取连接等待超时的时间
			maxWait: 60000
			# 配置连接超时时间
			connectTimeout: 30000
			# 配置网络超时时间
			socketTimeout: 60000
			# 配置间隔多久才进行一次检测，检测需要关闭的空闲连接，单位是毫秒
			timeBetweenEvictionRunsMillis: 60000
			# 配置一个连接在池中最小生存的时间，单位是毫秒
			minEvictableIdleTimeMillis: 300000
			# 配置一个连接在池中最大生存的时间，单位是毫秒
			maxEvictableIdleTimeMillis: 900000
			# 配置检测连接是否有效
			validationQuery: SELECT 1 FROM DUAL
			testWhileIdle: true
			testOnBorrow: false
			testOnReturn: false
			webStatFilter:
				enabled: true
			statViewServlet:
				enabled: true
				# 设置白名单，不填则允许所有访问
				allow:
				url-pattern: /druid/*
				# 控制台管理用户名和密码
				login-username: admin
				login-password: 123456
			filter:
				stat:
					enabled: true
					# 慢SQL记录
					log-slow-sql: true
					slow-sql-millis: 2000
					merge-sql: true
				wall:
					config:
						multi-statement-allow: true
        dynamic:
            druid:
                # 初始连接数
                initial-size: 5
                # 最小连接池数量
                min-idle: 10
                # 最大连接池数量
                max-active: 20
                # 配置获取连接等待超时的时间
                max-wait: 60000
                # 配置间隔多久才进行一次检测，检测需要关闭的空闲连接，单位是毫秒
                time-between-eviction-runs-millis: 60000
                # 配置一个连接在池中最小生存的时间，单位是毫秒
                min-evictable-idle-time-millis: 300000
                # 配置一个连接在池中最大生存的时间，单位是毫秒
                max-evictable-idle-time-millis: 900000
                # 配置检测连接是否有效
                validation-query: SELECT 1 FROM DUAL
                test-while-idle: true
                test-on-borrow: false
                test-on-return: false
                # 打开PSCache，并指定每个连接上PSCache的大小。
                # oracle设为true，mysql设为false。分库分表较多推荐设置为false
                pool-prepared-statements: false
                max-pool-prepared-statement-per-connection-size: 20
                filters: stat # 注意这个值和druid原生不一致，默认启动了stat
                stat:
                  merge-sql: true
                  log-slow-sql: true
                  slow-sql-millis: 2000
            primary: master # 设置默认的数据源或者数据源组
            strict: false # 设置严格模式,默认false不启动,启动后在未匹配到指定数据源时候回抛出异常,不启动会使用默认数据源
            datasource:
                master:
                    url: jdbc:mysql://192.168.56.101:3306/master?useUnicode=true&characterEncoding=utf8&zeroDateTimeBehavior=convertToNull&useSSL=true&serverTimezone=GMT%2B8
                    username: root
                    password: root
                    driver-class-name: com.mysql.cj.jdbc.Driver
                slave_1:
                    url: jdbc:mysql://192.168.56.102:3306/slave_1?useUnicode=true&characterEncoding=utf8&zeroDateTimeBehavior=convertToNull&useSSL=true&serverTimezone=GMT%2B8&rewriteBatchedStatements=true
                    username: root
                    password: root
                    driver-class-name: com.mysql.cj.jdbc.Driver
                slave_2:
                    url: jdbc:mysql://192.168.56.103:3306/slave_2?useUnicode=true&characterEncoding=utf8&zeroDateTimeBehavior=convertToNull&useSSL=true&serverTimezone=GMT%2B8&rewriteBatchedStatements=true
                    username: root
                    password: root
                    driver-class-name: com.mysql.cj.jdbc.Driver
                    druid: # 以下参数针对每个库可以重新设置druid参数
                        initial-size:
                        validation-query: select 1 FROM DUAL # 比如oracle就需要重新设置这个
                        public-key: #（非全局参数）设置即表示启用加密,底层会自动帮你配置相关的连接参数和filter。
```

如上即可配置访问用户和密码，访问 http://ip:端口/druid/index.html 查看 druid 监控。

### 集成 HikariCP

#### 集成步骤

1、项目引入 HikariCP 依赖

SpringBoot2.x.x 默认引入了 HikariCP，除非对版本有要求无需再次引入。

SpringBoot 1.5.x 需手动引入，对应的版本请根据自己环境和 HikariCP 官方文档自行选择。

```java
<dependency>
    <groupId>com.zaxxer</groupId>
    <artifactId>HikariCP</artifactId>
    <version>${version}</version>
</dependency>
```

2、参数配置

- 如果参数都未配置，则保持原组件默认值。
- 如果配置了全局参数，则每一个数据源都会继承对应参数。
- 每一个数据源可以单独设置参数覆盖全局参数。

特别注意，hikaricp 原生设置某些字段名和 dynamic-datasource 不一致，dynamic-datasource 是根据参数反射设置，而原生 hikaricp 字段名称和 set 名称不一致。

```yml
spring:
  datasource:
    dynamic:
      hikari: # 全局hikariCP参数，所有值和默认保持一致。(现已支持的参数如下,不清楚含义不要乱设置)
        catalog:
        connection-timeout:
        validation-timeout:
        idle-timeout:
        leak-detection-threshold:
        max-lifetime:
        max-pool-size:
        min-idle:
        initialization-fail-timeout:
        connection-init-sql:
        connection-test-query:
        dataSource-class-name:
        dataSource-jndi-name:
        schema:
        transaction-isolation-name:
        is-auto-commit:
        is-read-only:
        is-isolate-internal-queries:
        is-register-mbeans:
        is-allow-pool-suspension:
        data-source-properties:
        health-check-properties:
      datasource:
        master:
          username: root
          password: 123456
          driver-class-name: com.mysql.cj.jdbc.Driver
          url: jdbc:mysql://xx.xx.xx.xx:3306/dynamic?characterEncoding=utf8&useSSL=false
          hikari: # 以下参数针对每个库可以重新设置hikari参数
            max-pool-size:
            idle-timeout:
#           ......
```

#### 常见问题

Failed to validate connection com.mysql.jdbc.JDBC4Connection@xxxxx (No operations allowed after connection closed.)

https://github.com/brettwooldridge/HikariCP/issues/1034

核心意思就是 HikariCP 要设置 connection-test-query 并且 max-lifetime 要小于 mysql 的默认时间。

### 第三方集成

通过本章您可以掌握数据源在集成 MybatisPlus，Quartz，ShardingJdbc 的方案。

#### 集成 MybatisPlus

#### 集成步骤

1、项目引入 Mybatis-Plus 依赖

```xml
<dependency>
    <groupId>com.baomidou</groupId>
    <artifactId>mybatis-plus-boot-starter</artifactId>
    <version>${version}</version>
</dependency>
```

2、使用 DS 注解进行切换数据源

```java
@Service
@DS("mysql")
public class UserServiceImpl extends ServiceImpl<UserMapper, User> implements UserService {

    @DS("oracle")
    publid void addUser(User user){
        //do something
        baseMapper.insert(user);
    }
}
```

3、分页配置

```java
@Bean
public MybatisPlusInterceptor mybatisPlusInterceptor() {
    MybatisPlusInterceptor interceptor = new MybatisPlusInterceptor();
    //如果是不同类型的库，请不要指定DbType，其会自动判断。
    interceptor.addInnerInterceptor(new PaginationInnerInterceptor());
   // interceptor.addInnerInterceptor(new PaginationInnerInterceptor(DbType.MYSQL));
    return interceptor;
}
```

#### 注意事项

MybatisPlus 内置的 ServiceImpl 在新增，更改，删除等一些方法上自带事物导致不能切换数据源。

解决方法：

方法 1：复制 ServiceImpl 出来为自己的 MyServiceImpl，并去掉所有事务注解。

方法 2：创建一个新方法，并在此方法上加 DS 注解. 如上面的 addUser 方法。

#### FAQ

#### 为什么要单独拿出来说和 MybatisPlus 的集成？

因为 MybatisPlus 重写了一些核心类，必须通过解析获得真实的代理对象。

如果自己写多数据源，则很难完成与 mp 的集成。
