# keycloak 除了 admin 以外的 api

除了 Keycloak 的管理 API（admin API）之外，Keycloak 还提供了一些其他类型的 API，主要用于支持用户身份验证、授权以及与其他服务集成。这些 API 主要包括：

### 1. **认证 API (Authentication API)**

- **功能**：用于执行用户登录、登出、刷新令牌等操作，通常与 OpenID Connect 和 OAuth 2.0 协议兼容。
- **常用接口**：
  - **Token Endpoint**：用于获取访问令牌和刷新令牌。
    - URL：`/realms/{realm}/protocol/openid-connect/token`
    - 方法：`POST`
    - 参数：`grant_type`（可以是`password`、`authorization_code`、`refresh_token`等）
  - **User Info Endpoint**：用于获取当前用户的详细信息。
    - URL：`/realms/{realm}/protocol/openid-connect/userinfo`
    - 方法：`GET`
    - 认证方式：需要附加 `access_token`
  - **Logout Endpoint**：用户登出并使当前令牌失效。
    - URL：`/realms/{realm}/protocol/openid-connect/logout`
    - 方法：`POST`
    - 参数：`refresh_token`、`client_id` 和 `client_secret`
  - **Introspection Endpoint**：检查访问令牌或刷新令牌的状态和有效性。
    - URL：`/realms/{realm}/protocol/openid-connect/token/introspect`
    - 方法：`POST`
    - 参数：`token`、`client_id` 和 `client_secret`
  - **JWKs Endpoint**：获取公钥，用于验证令牌的签名。
    - URL：`/realms/{realm}/protocol/openid-connect/certs`
    - 方法：`GET`

### 2. **用户注册与账户管理 API (Account API)**

- **功能**：用于支持用户注册、个人账户管理等操作。用户可以自助修改个人信息，重置密码，查看会话信息等。
- **常用接口**：
  - **Account Management**：允许用户查看和更新个人信息、查看会话、管理应用授权等。
    - URL：`/realms/{realm}/account`
    - 通过 Keycloak 的 Account Console 可以实现账户管理操作。

### 3. **授权服务 API (Authorization API)**

- **功能**：用于实现资源保护、策略管理、权限管理等，通常用于细粒度的访问控制。
- **常用接口**：
  - **Permission Endpoint**：向资源服务器请求特定资源的权限。
    - URL：`/realms/{realm}/protocol/openid-connect/token`
    - 方法：`POST`
    - 参数：`grant_type=urn:ietf:params:oauth:grant-type:uma-ticket`
  - **Resource Registration**：注册应用的保护资源。
    - URL：`/realms/{realm}/authz/protection/resource_set`
    - 方法：`POST`、`PUT`、`DELETE`
    - 认证方式：资源服务器凭证 (access_token)
  - **Policy Management**：创建、更新和管理资源访问策略。

### 4. **服务账户 API (Service Account API)**

- **功能**：为服务账号获取访问令牌，通常用于系统之间的服务调用，无需用户参与。
- **常用接口**：
  - **Token Endpoint**：使用服务账号的 client_id 和 client_secret 获取 token。
    - URL：`/realms/{realm}/protocol/openid-connect/token`
    - 方法：`POST`
    - 参数：`grant_type=client_credentials`

### 5. **身份提供者 API (Identity Providers API)**

- **功能**：Keycloak 可以作为身份联邦的中介，与其他身份提供者（如 Google、Facebook、GitHub 等）集成，这些 API 支持身份联合认证。
- **常用接口**：
  - **认证重定向**：在 Keycloak 配置身份提供者后，用户可以通过 `Identity Provider Redirect` URL 开始身份验证。
    - URL：`/realms/{realm}/protocol/openid-connect/auth?client_id={client_id}&redirect_uri={redirect_uri}&response_type=code&scope=openid&kc_idp_hint={provider_id}`
    - 参数：`kc_idp_hint` 指定身份提供者 ID

### 6. **客户端应用的安全 URL**

- Keycloak 还提供了一些 URL 来支持不同的安全功能，比如登录、登出和错误页面等。
  - **登录 URL**：`/realms/{realm}/protocol/openid-connect/auth`
  - **登出 URL**：`/realms/{realm}/protocol/openid-connect/logout`
  - **注册 URL**：`/realms/{realm}/protocol/openid-connect/registrations`

### 7. **事件监听与管理 API (Events API)**

- **功能**：用于记录和管理 Keycloak 中的各种事件（如登录、登出、错误等），适合进行安全监控和审计。
- **常用接口**：
  - 事件主要是通过 Keycloak 管理控制台或配置文件中配置的日志监听器实现。
