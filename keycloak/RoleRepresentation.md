# RoleRepresentation 

`RoleRepresentation` 是 Keycloak 中用于表示角色的一个类，通常用于在 REST API 中创建、更新、删除角色，或用于查询角色的详细信息。下面是 `RoleRepresentation` 类的各个字段及其意义：

1. **`id`**  
   - 角色的唯一标识符。通常是一个 UUID 类型的字符串，Keycloak 自动生成。

2. **`name`**  
   - 角色的名称。用于标识和引用角色。在同一个角色容器（如 realm 或 client）中，角色名称应该是唯一的。

3. **`description`**  
   - 角色的描述。对该角色的简单文字描述，可以用来提供有关该角色的更多信息。

4. **`composite`**  
   - 一个布尔值，表示该角色是否为复合角色（composite role）。复合角色是由一个或多个其他角色组成的角色。通过设置为 `true`，可以使角色成为组合角色。

5. **`clientRole`**  
   - 另一个布尔值，表示该角色是否为客户端角色。客户端角色与应用程序或客户端相关联，而不是与整个 realm 相关。

6. **`containerId`**  
   - 角色所属的容器的 ID。对于一个客户端角色，它的容器就是该客户端的 ID；对于一个 realm 角色，它的容器就是 realm 本身的 ID。

7. **`realmId`**  
   - 角色所属的 realm 的 ID。此字段只有在角色属于 realm 时才有意义。

8. **`attributes`**  
   - 一个键值对对象，存储角色的自定义属性。可以用来存储与角色相关的额外信息，例如角色的过期时间或权限设置。

9. **`scopeParamRequired`**  
   - 一个布尔值，指示角色是否需要作用域参数。在某些情况下，角色与特定的 API 或客户端权限相关联，可能需要作用域。

### 示例
```json
{
  "id": "f3e3d62b-5b5f-45a1-b7d3-4a28adcae8d1",
  "name": "admin",
  "description": "Administrator role with full access",
  "composite": false,
  "clientRole": false,
  "containerId": "master",
  "realmId": "master",
  "attributes": {
    "custom-attribute": "value"
  },
  "scopeParamRequired": false
}
```

### 使用场景
- **`composite`** 字段在实现角色继承时非常有用，允许角色包含其他角色的权限。
- **`clientRole`** 字段用于区分客户端角色和 realm 角色，这样可以针对不同的客户端配置不同的角色权限。
- **`attributes`** 字段可以扩展角色的功能，允许存储和传递额外的元数据。


# RoleRepresentation.Composites

`RoleRepresentation.Composites` 是 Keycloak 中的一个字段，用于表示一个角色是否为**复合角色**（composite role）及其所包含的其他角色。在 Keycloak 中，复合角色是由多个角色组成的角色，通常用于角色继承或将多个权限合并到一个角色中。

### `composites` 字段的结构
在 `RoleRepresentation` 中，`composites` 字段是一个数组，包含了该角色作为复合角色所引用的所有其他角色。这些角色可以是其他 realm 角色或客户端角色，具体取决于角色的容器类型（realm 或 client）。

### `composites` 字段的含义
- **`composites`** 字段列出了当前角色所包含的所有其他角色，这些角色构成了复合角色的组成部分。
- 这些包含的角色通常被称为**子角色**，它们的权限会被合并到复合角色中，继而赋予复合角色的用户。

### 典型结构示例

```json
{
  "id": "f3e3d62b-5b5f-45a1-b7d3-4a28adcae8d1",
  "name": "super-admin",
  "description": "A super admin role that includes several other roles",
  "composite": true,
  "composites": [
    {
      "id": "a2e3b9ab-2d3d-4323-bb1b-1c8be45b0bc5",
      "name": "admin",
      "description": "Administrator role with full access"
    },
    {
      "id": "c9e3d6f7-59d6-4ccf-82f7-8ba388dc47f9",
      "name": "editor",
      "description": "Editor role with content editing permissions"
    }
  ],
  "clientRole": false,
  "containerId": "master",
  "realmId": "master",
  "attributes": {},
  "scopeParamRequired": false
}
```

### 解释

在这个示例中，角色 `super-admin` 是一个复合角色（`composite: true`），它包含了两个子角色：
1. `admin` — 管理员角色，具有完全访问权限。
2. `editor` — 编辑角色，具有内容编辑权限。

这些子角色的权限将被合并到 `super-admin` 角色中，从而使得拥有 `super-admin` 角色的用户拥有这两个角色的权限。

### `composites` 字段的使用场景
- **角色继承**：可以通过复合角色实现角色之间的权限继承。例如，创建一个 `super-admin` 角色，包含了 `admin` 和 `editor` 的权限。
- **权限组合**：通过复合角色将多个角色的权限组合在一起，简化角色的管理和权限分配。
- **角色管理**：如果有多个子角色共享一些相同的权限，可以将它们组合成复合角色，以便更方便地管理和分配给用户。

### 小结
`composites` 字段是一个用于定义复合角色所包含的其他角色的数组。通过复合角色的设计，Keycloak 提供了一种灵活的方式来组合和继承角色权限，便于在多角色和复杂权限模型下进行高效的权限管理。