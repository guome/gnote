# keycloakd的provider是什么

`Keycloak` 中的 **provider** 是一种扩展机制，允许开发者自定义和扩展 Keycloak 的功能。通过实现不同类型的 provider，可以增强 Keycloak 的核心功能或集成其他系统。以下是常见的 provider 类型和它们的作用：

---

### 1. **Authentication Provider**
   - **功能**：用于自定义 Keycloak 的认证流程，可以添加新的认证方式，比如 OTP、短信验证码、第三方身份提供商等。
   - **示例**：
     - Google/Facebook 登录
     - 自定义多因素认证（MFA）

---

### 2. **User Storage Provider**
   - **功能**：允许 Keycloak 通过外部存储（比如 LDAP、关系型数据库）来管理用户信息。
   - **示例**：
     - 连接到 LDAP 服务器
     - 使用自定义数据库存储用户信息

---

### 3. **Event Listener Provider**
   - **功能**：用于监听 Keycloak 中的事件，比如用户登录、登出、注册等。可以用来记录日志、触发外部系统的操作。
   - **示例**：
     - 记录用户登录事件
     - 通知其他系统用户注册成功

---

### 4. **Protocol Mapper Provider**
   - **功能**：用于自定义 OpenID Connect 或 SAML 协议的 token 映射。可以定义如何将用户属性映射到 token 中。
   - **示例**：
     - 在 JWT 中添加自定义用户属性
     - 修改默认 token 结构

---

### 5. **Realm Provider**
   - **功能**：对 Keycloak 的 `Realm`（域）进行定制化配置。可以扩展 Realm 的功能或管理多个 Realm。
   - **示例**：
     - 实现自定义 Realm 的功能扩展
     - 动态创建或删除 Realm

---

### 6. **Theme Provider**
   - **功能**：自定义 Keycloak 的 UI，包括登录页面、注册页面、管理控制台等。
   - **示例**：
     - 替换默认主题，定制企业风格
     - 添加多语言支持

---

### 7. **Policy Provider**
   - **功能**：用于定义自定义的授权策略。适用于 Keycloak 的授权服务。
   - **示例**：
     - 基于用户属性的动态策略
     - 自定义资源访问控制

---

### 8. **Identity Provider**
   - **功能**：实现对外部身份提供商的集成。
   - **示例**：
     - 支持 CAS 或 OAuth 2.0 的身份提供商
     - 集成企业内部单点登录（SSO）系统

---

### Provider 的实现步骤
1. **创建项目**：
   使用 Java 实现一个 JAR 包，包含你要定制的逻辑。
2. **实现接口**：
   Keycloak 提供了一组接口，如 `UserStorageProvider` 或 `AuthenticatorFactory`。
3. **注册 SPI**：
   在 `META-INF/services` 目录下声明对应的 SPI。
4. **部署 JAR**：
   将编译好的 JAR 包部署到 Keycloak 的 `standalone/deployments` 或 `providers` 文件夹。
5. **在 Admin Console 中启用**：
   配置并启用你开发的 provider。

---


# 自定义 provider 

需要实现两个接口 `EventListenerProviderFactory` 和 `EventListenerProvider`

```java

public class AddCustomAttrEventListener implements EventListenerProvider {

    private final KeycloakSession session;

    public AddCustomAttrEventListener(KeycloakSession session) {
        this.session = session;
    }

    @Override
    public void onEvent(Event event) {
        if (event != null && event.getType().equals(EventType.REGISTER)) {
            String userId = event.getUserId();
            String clientId = event.getClientId();
            if (userId != null) {
                UserModel user = session.users().getUserById(session.getContext().getRealm(), userId);

                if (user != null && clientId != null) {
                    // 每次注册时，给用户添加一个 client_id 方面后期以 client 维度区分用户
                    user.setSingleAttribute("client_id", clientId);
                }
            }
        }
    }

    @Override
    public void onEvent(AdminEvent event, boolean includeRepresentation) {

    }


    @Override
    public void close() {
    }
}



/**
 * 事件监听
 */
public class AddCustomAttrEventListenerProviderFactory implements EventListenerProviderFactory {

    @Override
    public EventListenerProvider create(KeycloakSession session) {
        return new AddCustomAttrEventListener(session);
    }

    @Override
    public void init(Config.Scope config) {
    }

    @Override
    public void postInit(KeycloakSessionFactory factory) {

    }

    @Override
    public void close() {
    }

    @Override
    public String getId() {
        return "add-custom-attr-listener";
    }
}
```

然后在 `resources/META-INF/services/org.keycloak.events.EventListenerProviderFactory` 这个文件下写一个全类名

```
org.keycloak.quickstart.event.listener.AddCustomAttrEventListenerProviderFactory
```

> `org.keycloak.events.EventListenerProviderFactory`这是一个文件，和a.txt一样的文件

最后，记得需要在 `keycloak` 后台添加事件，否则不生效

![alt text](./assets/image.png)

> `add-custom-attr-listener` 是 `getId()` 方法中定义的
> 
> 此时的 `keycload` 版本为 26.0.5