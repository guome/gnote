在 Keycloak 中，`rootUrl`、`baseUrl` 和 `adminUrl` 是客户端配置中的重要字段，它们用于不同的场景和用途，通常需要根据客户端的架构和需求正确设置。以下是它们的区别和用法：

---

### **1. rootUrl**

- **作用**：
  - 定义客户端的根 URL。
  - 是一个通用的基础 URL，其他 URL（如 `redirectUris` 和 `baseUrl`）可以基于它来设置。
  - 如果设置了 `rootUrl`，可以在配置其他 URL（如重定向 URI）时使用相对路径，否则需要使用完整路径。

- **典型场景**：
  - 用于简化配置，尤其是客户端有固定的根域名时。
  - 例如，如果客户端的所有 URI 都基于 `https://my-app.com`，可以将 `rootUrl` 设置为这个值。

- **示例配置**：
  ```json
  "rootUrl": "https://my-app.com"
  ```

- **举例**：
  如果 `rootUrl` 设置为 `https://my-app.com`，`redirectUris` 可以配置为相对路径：
  ```json
  "redirectUris": ["/callback"]
  ```
  最终会解析为 `https://my-app.com/callback`。

---

### **2. baseUrl**

- **作用**：
  - 定义客户端的基本 URL。
  - 通常指向客户端的主页面或默认入口地址。
  - Keycloak 会将用户的登录成功后默认重定向到 `baseUrl`。

- **典型场景**：
  - 登录成功后用户需要返回的页面。
  - 对于 SPA 或 Web 应用，这通常是应用的主页面。

- **示例配置**：
  ```json
  "baseUrl": "https://my-app.com/home"
  ```

- **注意**：
  - `baseUrl` 是可选的，如果未设置，则需要在登录请求中显式提供 `redirect_uri` 参数。
  - 如果设置了 `baseUrl`，Keycloak 会优先使用它作为默认重定向目标。

---

### **3. adminUrl**

- **作用**：
  - 定义客户端的管理端 URL。
  - Keycloak 在需要与客户端进行后台通信（如注销通知或会话管理）时会使用此 URL。
  - 主要用于 **后端通道注销（Back-Channel Logout）**。

- **典型场景**：
  - 后端应用中，用于接收 Keycloak 发出的通知或调用，例如在单点注销（SLO）时清理会话。
  - 通常指向客户端的服务端 API，而不是前端页面。

- **示例配置**：
  ```json
  "adminUrl": "https://my-app.com/api/keycloak"
  ```

- **注意**：
  - 如果客户端不需要支持后端操作（如注销通知），可以忽略该字段。
  - 需要确保 `adminUrl` 对 Keycloak 的服务器是可访问的。

---

### **总结对比**

| 属性        | 说明                                | 典型用途                       | 示例值                          |
|-------------|-------------------------------------|--------------------------------|---------------------------------|
| **rootUrl** | 客户端的根 URL，用于简化配置          | URI 基础路径                   | `https://my-app.com`           |
| **baseUrl** | 客户端的基本入口 URL，用户登录后重定向 | 登录成功后的默认跳转地址         | `https://my-app.com/home`      |
| **adminUrl**| 后端 URL，用于接收 Keycloak 的通知    | 后端操作（如注销通知）           | `https://my-app.com/api/keycloak` |

---

### **配置示例**

```json
{
  "rootUrl": "https://my-app.com",
  "baseUrl": "/dashboard",
  "adminUrl": "https://my-app.com/api/keycloak",
  "redirectUris": ["/callback", "/oauth/*"]
}
```

- 最终效果：
  - 登录成功后，用户会被重定向到：`https://my-app.com/dashboard`。
  - Keycloak 发出注销通知时，会调用：`https://my-app.com/api/keycloak`。
  - 重定向 URI `/callback` 最终解析为：`https://my-app.com/callback`。

---

### **实际应用建议**
1. **单页面应用（SPA）**：
   - 配置 `rootUrl` 为应用主域名，`baseUrl` 为应用的主页面。
   - 如果不需要后台操作，可以忽略 `adminUrl`。

2. **后端服务**：
   - 配置 `adminUrl` 接收 Keycloak 的后端通知。
   - `rootUrl` 和 `baseUrl` 可不配置，直接通过 API 设置 `redirect_uri`。

3. **多客户端环境**：
   - 确保 `rootUrl` 和 `adminUrl` 对 Keycloak 是正确且可访问的。

这三者配合使用可以让客户端在不同场景下与 Keycloak 更高效地集成。


# baseUrl 和 redirect_uri同时配置的话，登录成功会重定向哪里

如果在 Keycloak 客户端配置中同时设置了 **`baseUrl`** 和 **`redirect_uri`**，登录成功后的重定向行为将取决于以下情况：

---

### **Keycloak 的优先级规则**
1. **请求中提供了 `redirect_uri` 参数**：
   - 登录请求时如果显式指定了 `redirect_uri`，Keycloak 会优先使用此参数进行重定向，无论客户端是否配置了 `baseUrl`。
   
2. **未提供 `redirect_uri` 参数**：
   - 如果登录请求中未提供 `redirect_uri`，Keycloak 会使用客户端配置中的 `baseUrl` 作为默认重定向目标。

---

### **优先级总结**

| 条件                                | 重定向目标      |
|-------------------------------------|-----------------|
| 登录请求中指定了 `redirect_uri`       | 使用 `redirect_uri` |
| 登录请求中未指定 `redirect_uri`       | 使用 `baseUrl`      |
| 未配置 `baseUrl` 且未提供 `redirect_uri` | 登录失败或报错   |

---

### **实际示例**

#### **场景 1：同时配置 `baseUrl` 和 `redirect_uri`**
- 客户端配置：
  ```json
  {
    "baseUrl": "https://my-app.com/home",
    "redirectUris": ["https://my-app.com/callback"]
  }
  ```
- 登录请求：
  ```http
  GET https://keycloak-server/auth/realms/my-realm/protocol/openid-connect/auth?
      client_id=my-client&
      redirect_uri=https://my-app.com/callback&
      response_type=code
  ```

- 重定向行为：
  - **结果**：登录成功后用户会被重定向到 `https://my-app.com/callback`（`redirect_uri` 优先）。

#### **场景 2：仅配置 `baseUrl`，未提供 `redirect_uri`**
- 客户端配置：
  ```json
  {
    "baseUrl": "https://my-app.com/home",
    "redirectUris": ["https://my-app.com/*"]
  }
  ```
- 登录请求：
  ```http
  GET https://keycloak-server/auth/realms/my-realm/protocol/openid-connect/auth?
      client_id=my-client&
      response_type=code
  ```

- 重定向行为：
  - **结果**：登录成功后用户会被重定向到 `https://my-app.com/home`（使用 `baseUrl` 作为默认目标）。

#### **场景 3：未配置 `baseUrl`，仅在请求中指定 `redirect_uri`**
- 客户端配置：
  ```json
  {
    "redirectUris": ["https://my-app.com/callback"]
  }
  ```
- 登录请求：
  ```http
  GET https://keycloak-server/auth/realms/my-realm/protocol/openid-connect/auth?
      client_id=my-client&
      redirect_uri=https://my-app.com/callback&
      response_type=code
  ```

- 重定向行为：
  - **结果**：登录成功后用户会被重定向到 `https://my-app.com/callback`。

---

### **注意事项**
1. **`redirect_uri` 验证规则**：
   - 无论是显式传递还是隐式生成，`redirect_uri` 必须符合客户端配置中的 `redirectUris` 列表，否则会被拒绝。

2. **避免冲突**：
   - 如果 `baseUrl` 和 `redirect_uri` 不一致，可能导致意外行为。建议只设置其中一个，或者明确约定使用优先级。

3. **动态场景**：
   - 如果你的应用需要动态控制登录成功后的目标地址，可以通过在请求中显式传递 `redirect_uri` 实现灵活跳转。

---

### **最佳实践**
- **单一入口应用（SPA 或 Web）**：
  - 配置 `baseUrl` 为默认登录跳转地址。
  - 动态登录跳转时，通过请求中传递 `redirect_uri`。

- **多入口或动态场景**：
  - 不配置 `baseUrl`，只通过请求的 `redirect_uri` 参数控制目标地址。
  - 确保所有动态 URI 都在 `redirectUris` 中声明。

这样可以确保重定向目标始终按预期执行，同时提升安全性和灵活性。

# webOrigins是什么

在 **Keycloak** 中，**`webOrigins`** 是客户端配置中的一个字段，用于定义允许客户端与 Keycloak 进行跨域通信的来源（Origins）。它是 **CORS (Cross-Origin Resource Sharing)** 的一部分，用于处理浏览器中的跨域请求问题。

---

### **`webOrigins` 的作用**
`webOrigins` 指定了哪些来源的 Web 应用可以通过浏览器与 Keycloak 交互，确保跨域请求合法。这通常适用于以下情况：

1. **跨域请求**：
   - 当前端应用与 Keycloak 交互（如获取令牌或验证身份）时，浏览器会检查 CORS 策略。
   - Keycloak 根据 `webOrigins` 配置决定是否允许请求来源。

2. **OAuth2 和 OpenID Connect 流程**：
   - 用于限制哪些来源可以合法发起认证或令牌请求。

3. **安全性控制**：
   - 防止恶意网站通过跨域请求劫持用户的会话或敏感信息。

---

### **`webOrigins` 的配置**
- **值类型**：
  - `*`：允许所有来源（不推荐，安全性较低）。
  - 明确的 URL 列表：定义允许的来源（推荐）。
  - 空值：禁用所有跨域请求。

- **配置位置**：
  - 在 Keycloak 管理界面，客户端设置中的 **Web Origins** 字段。
  - 通过 API 配置时，可以在 `ClientRepresentation` 的 `webOrigins` 字段中设置。

---

### **示例**

#### 1. **允许特定来源**
```json
"webOrigins": ["https://example.com", "https://another-example.com"]
```
- 允许来自 `https://example.com` 和 `https://another-example.com` 的跨域请求。

#### 2. **允许所有来源（不推荐）**
```json
"webOrigins": ["*"]
```
- 允许任何来源的跨域请求。

#### 3. **禁用跨域请求**
```json
"webOrigins": []
```
- 不允许任何跨域请求。

---

### **应用场景**

1. **单页面应用（SPA）**：
   - SPA 通常通过浏览器直接与 Keycloak 交互，需要在 `webOrigins` 中配置应用的来源：
     ```json
     "webOrigins": ["https://my-spa.com"]
     ```

2. **多个前端应用**：
   - 如果有多个前端应用使用同一个 Keycloak 客户端，需要列出所有允许的来源：
     ```json
     "webOrigins": ["https://app1.com", "https://app2.com"]
     ```

3. **开发环境**：
   - 在本地开发时，可以允许 `http://localhost`：
     ```json
     "webOrigins": ["http://localhost:3000"]
     ```

---

### **注意事项**

1. **安全性**：
   - 避免使用 `"*"`，因为这会允许任何来源的跨域请求，增加安全风险。
   - 只允许可信任的来源，防止恶意网站滥用。

2. **与 `redirectUris` 的区别**：
   - **`redirectUris`** 定义可以重定向的目标 URI，用于 OAuth2 的登录流程。
   - **`webOrigins`** 定义允许进行跨域请求的来源，用于前端与 Keycloak 的直接通信。

3. **配置错误的后果**：
   - 如果 `webOrigins` 未正确配置，前端应用可能无法通过浏览器与 Keycloak 通信，导致认证失败或跨域请求被阻止。

---

### **常见问题**

- **为什么设置了 `redirectUris`，还需要 `webOrigins`？**
  - `redirectUris` 控制登录和回调的安全性，而 `webOrigins` 控制浏览器的跨域请求。

- **如何调试跨域问题？**
  - 检查浏览器开发工具中的 CORS 错误。
  - 确认请求来源是否包含在 `webOrigins` 中。

---

通过正确配置 `webOrigins`，可以确保前端应用与 Keycloak 的交互既安全又顺畅，避免不必要的跨域问题。