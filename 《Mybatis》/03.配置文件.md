## 配置文件说明

文档地址：https://mybatis.org/mybatis-3/zh/configuration.html

注意，各个配置是有顺序的，查看文档

```xml
<?xml version="1.0" encoding="UTF-8" ?>
<!DOCTYPE configuration
        PUBLIC "-//mybatis.org//DTD Config 3.0//EN"
        "https://mybatis.org/dtd/mybatis-3-config.dtd">
<configuration>

    <!-- 别名 -->
    <typeAliases>
        <!--
            扫描包
            这里配置了以后，UserMapper.xml 中 resultType 就不需要写包名了，直接写类名，且不区分大小写
            <select id="selectAll" resultType="User">
                select * from hm_tb_user
            </select>
         -->
        <package name="com.itheima.mybatis.pojo"/>
    </typeAliases>

    <!--
        数据库的环境配置信息
        default: 默认环境
     -->
    <environments default="development">
        <!-- 开发环境 -->
        <environment id="development">
            <!-- 事务管理 -->
            <transactionManager type="JDBC"/>
            <!-- 数据库连接池 -->
            <dataSource type="POOLED">
                <property name="driver" value="com.mysql.jdbc.Driver"/>
                <property name="url" value="jdbc:mysql://localhost:3306/sp_test?useSSL=false"/>
                <property name="username" value="root"/>
                <property name="password" value="123456"/>
            </dataSource>
        </environment>
        <!-- 测试环境 -->
        <environment id="test">
            <transactionManager type="JDBC"/>
            <dataSource type="POOLED">
                <property name="driver" value="com.mysql.jdbc.Driver"/>
                <property name="url" value="jdbc:mysql://localhost:3306/sp_test?useSSL=false"/>
                <property name="username" value="root"/>
                <property name="password" value="123456"/>
            </dataSource>
        </environment>
    </environments>
    <mappers>
        <!-- Mapper 代理方式: 扫描文件 -->
        <package name="com.itheima.mybatis.mapper"/>
    </mappers>
</configuration>
```