# Java 内存模型

JMM，全称 java memory model ，中文翻译为：java 内存模型。在了解它之前，首先要对一些知识进行铺垫。

# CPU 知识普及

线程是 CPU 调度的最小单位。线程中的字节码都将会在 CPU 中执行。Java 中的所有数据都是存放在主内存中的。 而 CPU 在执行字节码的过程中免不了要和 主内存打交道。随着 CPU 技术的发展，CPU 的速度越来越快，而内存的读写速度跟不上 CPU。

![cpu](./assets/cpu.png)

因此为了让 CPU 的性能最大化，达到高并发的效果。CPU 中添加了高速缓存 Cache 的概念。

![cpu](./assets/缓存.png)

执行任务时，CPU 会将运算所需数据 从 主内存复制到 高速缓存中，让运算快速进行。当运算完成之后，再将运算结果刷回主内存。这样，在运算过程中就不必这么频繁地与主内存交互了。
看似是提高了 CPU 的运行效率，但是问题随之而来。
每个处理器都有自己的高速缓存，而他们都在操作同一块主内存。当多个处理器同时操作主内存时，可能导致数据不一致。 这就是 缓存一致性问题。

# 缓存一致性问题

现在的移动设备很多都是 多个 CPU，每个 CPU 还有多核。每个 CPU 都能在一个时刻运行一个线程。也就意味着，如果你的 Java 程序是多线程的，就有可能存在 多个线程在同一时刻被不同的 cpu 执行的情况。 举例说明：

```java

public int x = 0;

public int y = 0;

Thread p1 = new Thread() {
    public void run() {
        int r1 = x;
        y = 1;
    }
};

Thread p2 = new Thread() {
    public void run() {
        int r2 = y;
        x = 2;
    }
};

p1.start();

p2.start();
```

以上代码展示了两个变量 x,y 被两个线程同时操作的情况。如果要在两个线程都执行完毕 之后，打印出 r1 和 r2 的值，那么结果会有几种呢？

如果 p1 先执行完毕，而后 p2 执行。那么结果就是 r1=0;r2=1;

如果 p2 先执行完毕，而后 p1 执行，那么结果是 r1=2;r2=0;

以上两种情况是理想状况，可是现实中，可能有 2 个 cpu c1 和 c2，分别来执行这两个线程,那么情况就会变得复杂：

p1 中 int r1=x;这句代码在 c1 中执行完毕，并且刷回主内存，那么 r1 就是 0. 然后紧接着 c2 执行了 int r2=y; 那么 r2 就是 0. 后续不管怎么执行，r1=0；r2=0 的结果是不受影响的.

出现以上多种情况的根本原因，就是 CPU 在执行完某个字节码之后，它将数据刷新回主内存的时机是不确定的。而 CPU 的调度受系统策略的影响，通常不受人为控制。

# 指令重排

为了使 CPU 的内部运算单元能够被充分利用，处理器可能对输入的字节码指令进行重新排序，这也叫处理器优化。（比如，jvm 的即时编译 JIT） 如下图所示代码：

```java
int a = 1;
int b = 2;
int a = a + 1;
```

指令重排之后的执行顺序可能就是：

```java
int a = 1;
int a = a + 1;
int b = 2;
```

也就是说，在 CPU 层面，代码并不会严格按照你写的顺序来执行。！！！

如果按照这种思路来看上面 r1,r2 的例子，那么还有第四种情况：

```java
Thread p1 = new Thread() {
    public void run() {
        int r1 = x; // 第二步执行
        y = 1; // 第三步执行
    }
};

Thread p2 = new Thread() {
    public void run() {
        x = 2;  // 第一步执行
        int r2 = y; // 第四步执行
    }
};
```

经过重排的线程 p2 如上图右边所示，如果按照这个顺序（图中红色 1，2，3，4）来执行的话， 结果就是 r1=2; r2=1;

# 内存模型的概念

为了解决这种一致性问题，内存模型应运而生。

所谓内存模型，就是一套共享内存系统中，多线程读写操作行为的规范。这套规范屏蔽了底层硬件和操作系统的内存访问的差异。解决了 CPU 多级缓存，CPU 优化，指令重排等因素导致的内存访问问题。从而保证 java 程序，尤其是多线程 java 程序 在各种平台下对内存访问的结果一致性。

在 java 内存模型中，CPU 的高速缓存，被抽象为：工作内存。每一个线程都有自己的 工作内存.

线程之间的共享数据存储在主内存中.

## 先行发生原则

也就是 happends-before 原则。它是 内存模型中最重要的原则。

它用于描述两个操作的内存可见性。通过可见性，让程序免于数据竞争的干扰。

它的定义为：如果一个操作 A happends-before 操作 B，那么操作 A 的结果将对 B 可见

反过来理解，如果想要让操作 A 的结果对操作 B 可见，那么必须让操作 A happends-before B

举例说明：

```java
private int value = 0;

public void setValue(int value) {
    value = 1;
}

public void getValue() {
    return value;
}
```

以上用 set 方法和 get 方法对 value 进行读写操作。

很有可能 这两个操作出现在不同的线程中。 那么假设，set 被 线程 A 执行， get 被线程 B 执行。那么 B 的执行结果是多少呢？

结果显然是不确定的。

这里分两个情况

- set 操作 happends-before get 操作; set 的结果对 get 可见，此时 get 的结果为 1
- set 操作 没有 happends-before get; 操作 set 的结果对 get 不可见，此时，get 的结果为 0

那么如何判定 java 中的两个操作符合 happends-before 规则呢?

## happends-before 先行发生原则

### 程序次序原则

单线程内部，如果一段代码的字节码顺序也隐式符合 happends-before 原则，那么逻辑靠前的字节码执行结果一定是对逻辑靠后的字节码 可见。

```java
int a = 10;
b = b + 1 ;
```

举例说明，如上代码中，单线程中执行了两个操作，`int a = 10;`以及 `b = b + 1;`, 按照此原则，`a=10` 这个结果，对 后面的操作，是可见的，但是，后面的`b=b+1;` 并没有对 a 变量有依赖。此时 cpu 就有可能发生指令重排，将对 a 有依赖的操作，挪到`int a=10;` 的后面. 就像下面的：

```java
int a = 10;
b = b + 1 ; // 原始顺序
a = a + 1 ;
```

在最终执行字节码时的顺序，可能就变成了：

```java
int a = 10;
a = a + 1; // 指令被重排之后
b = b + 1;
```

而如果是这样，b = b + a，直接对 a 有依赖，则不会指令重排优化：

```java
int a = 10;
b = b + a ; // 不会指令重排优化
a = a + 1 ;
```

### 锁定规则

如果一个锁，处于被锁定的状态，那么必须先执行 unlock，然后才能执行 lock 操作。

### 变量规则

volatile 关键字，保证了线程的可见性，如果一个线程写了 volatile 修饰的变量，然后另一个线程去读这个变量，那么结果一定是 写操作 happends-before 读操作。

### 线程启动规则

线程对象的 start 方法，先行发生于此线程的每一个动作。 如果线程 A，在执行过程中，通过 B.start 方法启动了线程 B, 那么在启动 B 之前的 A 对主内存中共享变量的操作操作结果，都对 B 可见。

解释一下：

```java
int x = 0;
int y = 1;

new Thread(new Runnable(){
        @override
        run(){
            x = 100;
            y = 200;


            new Thread(new Runnable(){
                    @override
                    run(){
                        System.out.println(x + " - " + y);

                    }
                }
            ).start();

        }
    }
).start();
```

上面代码中，打印出的 x 和 y 的值，一定是在 100 和 200.

### 线程中断规则

对线程 `interrupt()` 方法的调用先行发生于`被中断线程的代码检测`，直到中断事件的发生。 举例说明：

被中断线程的代码检测: `Thread.currentThread().isInterrupted()`

```java
public class Main {
    public static void main(String[] args) {
        Thread thread = new Thread(new LongRunningTask());
        thread.start();

        // 主线程休眠一段时间后，中断长时间执行的任务
        try {
            Thread.sleep(2000); // 休眠2秒钟
            thread.interrupt(); // 中断线程
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
class LongRunningTask implements Runnable {
    @Override
    public void run() {
        try {
            System.out.println("长时间执行的任务开始...");
            for (int i = 0; i < 10; i++) {
                System.out.println("执行第 " + i + " 步");
                Thread.sleep(1000); // 模拟耗时操作

                // 检测线程是否被中断
                // 这里是不会被调用的， 因为调用了 interrupt()， 它会直接走 catch
                if (Thread.currentThread().isInterrupted()) {
                    System.out.println("任务被中断，提前结束1");
                    return;
                }
            }
            System.out.println("长时间执行的任务完成");
        } catch (InterruptedException e) {
            System.out.println("任务被中断，提前结束2");
        }
    }
}
```

以上代码，模拟了 主线程 中启动了子线程，并且在主线程睡眠一段时间之后中断子线程的情况。
在 先行发生原则的作用下，当主线程调用了 子线程的中断方法 interrupt 之后，子线程立即就检测到了自己被中断，从而停止了代码执行。
如果没有这个先行发生原则，那么就有可能在 interrupt 之后，子线程继续执行一段代码之后才知道自己被中断了，而执行了多少，存在不确定性。
上面代码的打印结果为：

```
长时间执行的任务开始...
执行第 0 步
执行第 1 步
任务被中断，提前结束2
```

### 线程终结规则

线程中的所有操作，都发生在终止检测之前。

Thread.join()方法是 等待此线程结束。

Thread.isAlive()的返回值表示的是 此线程是否已终止。

如果线程 A 在执行过程中调用了线程 B 的 join 方法，那么 A 线程会等待 B 执行完，那么 B 对 主内存中共享变量的操作就是对 A 可见的。

这个很好理解。如下代码：

```java
public class Main {
    static int sharedVariable = 0;

    public static void main(String[] args) throws InterruptedException {
        Thread threadB = new Thread(() -> {
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            sharedVariable = 10;
        });

        Thread threadA = new Thread(() -> {
            try {
                System.out.println("线程A开始执行");
                threadB.start();
                threadB.join();
                System.out.println("线程B执行完毕");
                System.out.println("线程A中的 sharedVariable = " + sharedVariable);
                sharedVariable = 200;
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        });

        System.out.println("主线程开始");
        threadA.start();
        threadA.join();

        System.out.println("主线程结束");
        System.out.println("主线程中的 sharedVariable = " + sharedVariable);
    }
}
```

有 3 个线程，main，threadA，thread。
上面的代码描述了，main 启动 threadA,threadA 启动了 threadB，并且 A 中等待 B 执行完，main 则等待 A 执行完，最后分批打印 共享变量 sharedVariable 的值。
执行的结果为：

```
主线程开始
线程A开始执行
线程B执行完毕
线程A中的 sharedVariable = 10
主线程结束
主线程中的 sharedVariable = 200
```

虽然有多个线程，但是，B 的执行结果对 A 是可见的，A 的执行结果对 main 是可见的。所以共享变量的结果都是可预测的，没有不确定性。

### 对象终结原则

一个对象的初始化发生在它的 finalized 方法之前。

### happends-before 的可传递性

如果操作 A happends-before 操作 B，操作 B happends-before 操作 C，那么操作 A 一定 happends-before 操作 C。

## Java 内存模型的应用

上面提到的 happends-before 原则是判断 数据是否存在竞争，线程是否存存在竞争的主要依据。

Java 提供了一系列关键字，可以让原本不是 happends-before 的 情况，变得符合 happends-before 原则。

### volatile

以最简单的 多线程对同一个变量的 get set 操作为例：

```java
private volatile int value = 0;

public void setValue(int value) {
    value = 1;
}

public int getValue() {
    return value;
}
```

如果没有 volatile 原则的话，多线程读取 value 的结果是不可控的。 但是一旦给 value 加上了 volatile ，所有线程对于 value 的操作，对后续的线程都是可见的，因为每次操作完了都会将 value 写入到主内存。

### synchronized

```java
private int value = 0;

public void setValue(int value) {
    synchronized {
        value = 1;
    }
}

public int getValue() {
    synchronized {
        return value;
    }
}
```

同样，同步关键字 synchronized，也能保证对 value 的操作对后续的线程可见。

# 总结

JMM（Java Memory Model） java 内存模型 的诞生主要是因为 CPU 的 高速缓存 与 主内存的交互，指令重排等特性，会导致多线程执行结果的不可控。
JMM 本身是一套规范，这个规范中有一个最重要的 先行发生原则 happends-before。
JMM 的运用主要有两个关键字 volatile 和 synchronized 等， 让程序符合 happends-before 原则。
