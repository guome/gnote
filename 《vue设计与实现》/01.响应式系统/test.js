const obj = {
    foo: 1,
    get bar() {
        return this.foo
    }
}

console.log(obj.bar)
