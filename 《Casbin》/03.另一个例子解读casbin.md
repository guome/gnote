# 另一个例子解读 casbin

## 配置文件

```conf
[request_definition]
r = sub, obj, act

[policy_definition]
p = sub, obj, act

[role_definition]
g = _, _
g2 = _, _

[policy_effect]
e = some(where (p.eft == allow))

[matchers]
m = g(r.sub, p.sub) && g2(r.obj, p.obj) && r.act == p.act || r.sub == "root"
```

- `g(r.sub, p.sub)` 检查请求主体`（r.sub）`是否具有策略主体`（p.sub）`的角色。
- `g2(r.obj, p.obj)` 检查请求资源`（r.obj）`是否与策略资源`（p.obj）`相关。

## 策略文件

```csv
# superuser 角色可以对资源 user 进行 read:any 的操作
p, superuser, user, read:any
p, manager, user_roles, read:any
p, guest, user, read:own

# alice 继承了 superuser
# 可以认为 alice 这个"角色"(但是alice实际上有可能是一个用户名)继承了 superuser 角色
g, alice, superuser
g, tom, manager
g, bob, guest

# 更加高级的抽象是 users_list 继承了 user
# 但是实际上 user 是 obj ，它是一个资源
# 所以这里的意思是 users_list 是属于 user 这个资源组的
g2, users_list, user
g2, user_roles, user
g2, user_permissions, user
g2, roles_list, role
g2, role_permissions, role
```

## 请求

```
alice, user, read:any
alice, user_roles, read:any
alice, users_list, read:any
alice, roles_list, read:any
root, user_permissions, read:any
```

`alice, user, read:any` 的意思是 aclice 这个用户(或者说角色)要对 user 这个资源进行 read:any 的操作

梳理一下检查的流程:

- 首先检查请求格式是否正确 sub(alice), obj(user), act(read:any); 请求中都有
- 到策略文件中开始匹配
  - g(r.sub, p.sub) 检查请求主体 alice 是否具有策略主体的角色。策略文件中可以看到,alice 属于（或者说继承）superuser
  - `g2(r.obj, p.obj)` 检查请求资源 user 是否与策略资源相关。这里可以看到 superuser 角色可以对 user 进行 read:any 操作
  - r.act == p.act 检查请求资源的 act 是否和策略文件中的一致。这里是一致的，通过。

`alice, user_roles, read:any` 的意思是 aclice 这个用户(或者说角色)要对 user_roles 这个资源进行 read:any 的操作

梳理一下检查的流程:

- 首先检查请求格式是否正确 sub(alice), obj(user), act(read:any); 请求中都有
- 到策略文件中开始匹配
  - g(r.sub, p.sub) 检查请求主体 alice 是否具有策略主体的角色。策略文件中可以看到,alice 属于（或者说继承）superuser
  - `g2(r.obj, p.obj)` 检查请求资源 user_roles 是否与策略资源相关。这里可以看到 user_roles 属于（或者说继承/分组都行） user 的，而 superuser 角色可以对 user 进行 read:any 操作，通过
  - r.act == p.act 检查请求资源的 act 是否和策略文件中的一致。这里是一致的，通过。

## 结果

```
true Reason: ["superuser","user","read:any"]
true Reason: ["superuser","user","read:any"]
true Reason: ["superuser","user","read:any"]
false
true Reason: ["superuser","user","read:any"]
```
