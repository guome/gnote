## node端，流示例

```js
const fs = require("fs");
const http = require("http");
const express = require("express");
const cors = require("cors");

const app = express();

app.use(cors());


app.get("/", (req, res) => {
    // 代码执行到这里，建立了 tcp 连接
  const stream = fs.createReadStream("./fff.pdf"); // 替换为实际的文件路径

  res.setHeader("Access-Control-Allow-Origin", "*");

  // 设置响应头，指定流的内容类型
  res.writeHead(200, { "Content-Type": "application/octet-stream" });

  // 将流管道传输到响应对象
  // 把流发送给响应对象，也就是前端
  stream.pipe(res);

  // 监听流的结束事件，确保在流传输完毕后关闭响应对象
  stream.on("end", () => {
    // 关闭连接
    res.end();
  });

  // 监听流的错误事件，发生错误时向客户端发送错误信息
  stream.on("error", (error) => {
    res.writeHead(500, { "Content-Type": "text/plain" });
    res.end(`Error: ${error.message}`);
  });
});

// server.on("request", cors());

app.listen(3000, () => {
  console.log("Server is running on port 3000");
});

```

## 前端下载

```js
const start = Date.now()
console.log('start',start)
fetch('http://localhost:3000/')
    .then(response => {
        // 代码执行到了这里，说明已经与服务端连接了 tcp 连接, 所以这里 response 对象可以接受到服务端到流数据
        // 确保得到一个有效的响应对象
        if (!response.ok) {
            throw new Error('HTTP error ' + response.status);
        }
        // 这里的时间很短，可能只有13ms,但是浏览器显示的完全响应要 108 毫秒
        console.log('获取到响应', Date.now() - start)
        // 获取到可读流，但是此时连接还没有结束，这里会持续接受流数据
        // 返回一个可读的流
        return response.body.getReader();
    })
    .then(async reader => {
        // 读取流数据块
        let chunks = [];
        let totalSize = 0;

        // 使用一个 while 需要不断读取服务端传过来的流对象
        while (true) {
            const {done, value} = await reader.read()
            // console.log(value, done)
            if (done) {
                return chunks
            }

            chunks.push(value);
            totalSize += value.length;
            // console.log(`Received ${totalSize} bytes`);
        }
    })
    .then(blob => {
        console.log('blob', blob)
        // 处理接收到的文件（例如保存到本地）
        // const url = URL.createObjectURL(blob);
        // const link = document.createElement('a');
        // link.href = url;
        // link.download = 'file.txt'; // 设置下载的文件名
        // link.click();
    })
    .catch(error => {
        console.error('Error:', error);
    });
```