在创建 MySQL 数据库时，选择合适的字符集和排序规则（collation）非常重要，它会直接影响数据存储和查询的行为。以下是常用的推荐选择：

常用字符集 1. utf8mb4
• 用途：支持所有 Unicode 字符，包括表情符号和其他特殊字符。
• 推荐场景：几乎所有现代应用程序，尤其是需要支持多语言或者使用 Emoji 的场景。
• 注意事项：这是目前推荐的默认字符集，比 utf8 更全面（utf8 不支持 4 字节的 Unicode 字符，如表情符号）。 2. latin1
• 用途：只支持基本的西欧字符集。
• 推荐场景：需要优化存储且只处理英文或西欧语言的场景。
• 注意事项：一般不建议使用，因为全球化支持较差。

常用排序规则

排序规则决定了字符串如何比较和排序： 1. utf8mb4_general_ci
• 用途：大小写不敏感（\_ci 表示 case-insensitive），通用排序规则，性能好。
• 推荐场景：大多数情况下可以满足需求，尤其是只需要基础排序的场景。 2. utf8mb4_unicode_ci
• 用途：大小写不敏感，符合 Unicode 排序标准，支持更复杂的语言规则。
• 推荐场景：对语言排序规则敏感的场景，如需要正确排序多语言文本。 3. utf8mb4_bin
• 用途：二进制比较，大小写敏感。
• 推荐场景：需要严格区分大小写，或对字符串的二进制存储进行比较的场景。

推荐配置

如果你的项目需要支持多语言和特殊字符：

CREATE DATABASE mydb CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

如果对性能要求较高，且不涉及复杂排序：

CREATE DATABASE mydb CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;

其他注意事项 1. 表和列级别的字符集和排序规则：
• 数据库级别的设置是默认值，可以在创建表和列时单独指定字符集和排序规则。
• 示例：

CREATE TABLE mytable (
id INT PRIMARY KEY,
name VARCHAR(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci
);

    2.	对现有数据库修改字符集和排序规则：

如果需要调整已经存在的数据库，可以使用：

ALTER DATABASE mydb CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;
ALTER TABLE mytable CONVERT TO CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

    3.	数据库版本：

确保 MySQL 版本支持 utf8mb4，推荐使用 5.7 或更高版本，性能和功能支持更好。

根据你的具体需求选择最适合的配置即可。
