### 表复制

自我复制数据（蠕虫复制）

有时为了对某个 sql 语句进行效率测试，我们需要海量数据时，可以使用此法为表创建海量数据

把其他表的数据插入到 my_tab01 表中

```sql
INSERT INTO my_tab01
	(id, `name`)
	SELECT id, `name` FROM staff;
```

自我复制

```sql
INSERT INTO my_tab01 SELECT * FROM my_tab01;
```

### 去重

思路

1. 创建一张临时表 `my_tmp`, 改表的结构和要去重的表(`my_tab01`)的结构一致
2. 把 `my_tmp` 的记录通过 `distinct` 关键字处理后，把记录复制到 `my_tmp`中
3. 清理掉 `my_tab01` 记录
4. 把`my_tmp`表的记录复制到`my_tab01`
5. `drop`掉临时表`my_tmp`

实现

```sql
-- 创建临时表
CREATE TABLE my_tmp LIKE my_tab01;
-- 复制数据
INSERT INTO my_tmp SELECT DISTINCT * FROM my_tab01;
-- 清空表
DELETE FROM my_tab01;
-- 复制数据
INSERT INTO my_tab01 SELECT * FROM my_tmp;
-- 删除临时表
DROP TABLE my_tmp;
```
