### 一、浏览器四种常见的 post 请求方式

```
enctype 属性规定在发送到服务器之前应该如何对表单数据进行编码。默认地，表单数据会编码为 "application/x-www-form-urlencoded"
```

![img](https://img2018.cnblogs.com/blog/1500466/201906/1500466-20190611110428523-2038582698.png)

**（1）、**HTTP 协议是以 **ASCII 码** 传输，建立在 TCP/IP 协议之上的应用层规范。规范把 HTTP 请求分为三个部分：状态行、请求头、消息主体。
**（2）、**协议规定 POST 提交的数据必须放在消息主体（entity-body）中，但协议并没有规定数据必须 **使用什么编码方式** 。实际上，开发者完全可以自己决定消息主体的格式，只要最后发送的 HTTP 请求满足上面的格式就可以。
**（3）、**数据发送出去，还要服务端解析成功才有意义。一般服务端语言如 php、python 等，以及它们的 framework，都内置了自动解析常见数据格式的功能。服务端通常是根据请求头（headers）中的 Content-Type 字段来获知请求中的消息主体是用何种方式编码，再对主体进行解析。

**（1）application/x-www-form-urlencoded**
这应该是最常见的 POST 提交数据的方式了。浏览器的原生 <form> 表单，如果不设置 enctype 属性，那么最终就会以 application/x-www-form-urlencoded 方式提交数据。

```
<form action="form_action.asp" enctype="text/plain">
  <p>First name: <input type="text" name="fname" /></p>
  <p>Last name: <input type="text" name="lname" /></p>
  <input type="submit" value="Submit" />
</form>
```

此时可以看到，

```
Content-Type: application/x-www-form-urlencoded;charset=utf-8
title=test&sub%5B%5D=1&sub%5B%5D=2&sub%5B%5D=3
```

首先，Content-Type 被指定为 application/x-www-form-urlencoded；其次，提交的数据按照 key1=val1&key2=val2 的方式进行编码，key 和 val 都进行了 URL 转码。大部分服务端语言都对这种方式很好的支持，常用的如 jQuery 中的 ajax 请求，**Content-Type** 默认值都是**「application/x-www-form-urlencoded;charset=utf-8**

**（2）multipart/form-data**
这也是常见的 post 请求方式，一般用来上传文件，各大服务器的支持也比较好。所以我们使用表单 **上传文件** 时，必须让<form>表单的 enctype 属性值为 multipart/form-data.

> 注意：以上两种方式：application/x-www-form-urlencoded 和 multipart/form-data 都是浏览器原生支持的。

**（3）application/json**
application/json 作为响应头并不陌生，实际上，现在很多时候也把它作为请求头，用来告诉**服务端消息主体是序列化的 JSON 字符串**，除了低版本的 IE，基本都支持。除了低版本的 IE 都支持 JSON.stringify（）的方法，服务端也有处理 JSON 的函数，使用 json 不会有任何麻烦。例如：

[![复制代码](https://common.cnblogs.com/images/copycode.gif)](<javascript:void(0);>)

```
//请求数据
var data = {name:'jack',sex:'man'};
//请求数据序列化处理
JSON.stingify(data);

//结果：{'name':'jack','sex':'man'};
```

[![复制代码](https://common.cnblogs.com/images/copycode.gif)](<javascript:void(0);>)

### 二、postman 的几种参数格式

### form-data、x-www-form-urlencoded、raw、binary 的区别

![img](https://img2018.cnblogs.com/blog/1500466/201906/1500466-20190611102806076-1597662165.png)

1.form-data 对应的是页以 form 表单提交传值的情形

等价于 http 请求中的 multipart/form-data,它会将表单的数据处理为一条消息，以标签为单元，用分隔符分开。既可以上传键值对，也可以上传文件。当上传的字段是文件时，会有 Content-Type 来表名文件类型；content-disposition，用来说明字段的一些信息；
由于有 boundary 隔离，所以 multipart/form-data 既可以上传文件，也可以上传键值对，它采用了键值对的方式，所以可以上传多个文件

[![复制代码](https://common.cnblogs.com/images/copycode.gif)](<javascript:void(0);>)

```
POST  HTTP/1.1
Host: test.app.com
Cache-Control: no-cache
Postman-Token: 59227787-c438-361d-fbe1-75feeb78047e
Content-Type: multipart/form-data; boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW

------WebKitFormBoundary7MA4YWxkTrZu0gW
Content-Disposition: form-data; name="filekey"; filename=""
Content-Type:


------WebKitFormBoundary7MA4YWxkTrZu0gW
Content-Disposition: form-data; name="textkey"

tttttt
------WebKitFormBoundary7MA4YWxkTrZu0gW--
```

[![复制代码](https://common.cnblogs.com/images/copycode.gif)](<javascript:void(0);>)

请求体中的 boundary 参数指定的就是分隔体，可以看到请求内容被分为了两段，第一段对应 filekey，第二段对应 textkey。

\2. x-www-form-urlencoded
即 application/x-www-from-urlencoded，将表单内的数据转换为 Key-Value。

[![复制代码](https://common.cnblogs.com/images/copycode.gif)](<javascript:void(0);>)

```
POST  HTTP/1.1
Host: test.app.com
Content-Type: application/x-www-form-urlencoded
Cache-Control: no-cache
Postman-Token: e00dbaf5-15e8-3667-6fc5-48ee3cc89758

key1=value1&key2=value2
```

[![复制代码](https://common.cnblogs.com/images/copycode.gif)](<javascript:void(0);>)

form-data 与 x-www-form-urlencoded 的区别

- multipart/form-data：可以上传文件或者键值对，最后都会转化为一条消息
- x-www-form-urlencoded：只能上传键值对，而且键值对都是通过&间隔分开的

  3.raw 对应的是入参是任意格式的可以上传任意格式的【文本】，可以上传 text、json、xml、html 等

![img](https://img2018.cnblogs.com/blog/1500466/201906/1500466-20190611103605653-652169084.png)

(4)binary

相当于 Content-Type:application/octet-stream,只可以上传二进制数据，通常用来上传文件，但是一次只能上传一个文件

响应

1.点击 Send 即可发送请求

在下面的 response 模块显示返回信息
