# Java Servlet web.xml 配置详解

> 简介： 要从浏览器访问 Java servlet，必须告诉 servlet 容器要部署哪些 servlet 以及要将 servlet 映射到哪个 URL。 这是在 Java Web 应用程序的 web.xml 文件中完成的。

## 配置和映射 Servlet

我们来看一个例子：

```xml
<?xml version="1.0" encoding="ISO-8859-1"?>
<!DOCTYPE web-app
    PUBLIC "-//Sun Microsystems, Inc.//DTD Web Application 2.3//EN"
    "http://java.sun.com/dtd/web-app_2_3.dtd">

<web-app>
  <servlet>
    <servlet-name>controlServlet</servlet-name>
    <servlet-class>com.xxx.ControlServlet</servlet-class>
  </servlet>

  <servlet-mapping>
    <servlet-name>controlServlet</servlet-name>
    <!-- 所有以.html 结尾的 URL 都被发送到 servlet -->
    <url-pattern>*.html</url-pattern>
  </servlet-mapping>
</web-app>
```

首先配置 servlet。 这是使用 `<servlet>` 元素完成的。 在这里给 servlet 一个名字，并写下 servlet 的类名。

其次，将 servlet 映射到 URL 或 URL 模式。 这在 `<servlet-mapping>` 元素中完成。 在上面的例子中，所有以.html 结尾的 URL 都被发送到 servlet。

我们一般还可能使用的 servlet URL 映射是：

```
/myServlet

/myServlet.do

/myServlet*
```

`*` 是通配符，意思是任何文本。 如您所见，您可以使用通配符`（）`将 servlet 映射到单个特定的 URL 或 URL 的模式。 你将使用什么取决于 servlet 的功能。

## Servlet 初始参数

您可以从 web.xml 文件将参数传递给 servlet。 servlet 的 init 参数只能由该 servlet 访问。

如何在 web.xml 文件中配置它们的方法：

```xml
<servlet>
    <servlet-name>controlServlet</servlet-name>
    <servlet-class>com.xxxControlServlet</servlet-class>
        <init-param>
            <param-name>myParam</param-name>
            <param-value>paramValue</param-value>
        </init-param>
</servlet>
```

如何从 Servlet 内部读取 init 参数的方法 - 在 `Servlet init()` 方法中：

```java
public class SimpleServlet extends GenericServlet {

  protected String myParam = null;

  public void init(ServletConfig servletConfig) throws ServletException{
    this.myParam = servletConfig.getInitParameter("myParam");
  }

  public void service(ServletRequest request, ServletResponse response)
        throws ServletException, IOException {

    response.getWriter().write("<html><body>myParam = " +
            this.myParam + "</body></html>");
  }
}
```

servlet 容器首次加载 servlet 时会调用 `servlets init()` 方法。 在加载 servlet 之前，是不会允许访问该 servlet。

## Servlet 加载启动

`<servlet>` 元素有一个名为 `<load-on-startup>` 的子元素，您可以使用它来控制何时 servlet 容器应该加载 servlet。 如果不指定 `<load-on-startup>` 元素，那么 servlet 容器通常会在第一个请求到达时加载 servlet。

通过设置 `<load-on-startup>` 元素，可以告诉 servlet 容器在 servlet 容器启动后立即加载 servlet。 请记住，在加载 servlet 时调用 Servlet init() 方法。

这里是一个 `<load-on-startup>` 配置的例子：

```xml
<servlet>
    <servlet-name>controlServlet</servlet-name>
    <servlet-class>com.xxx.xxx.ControlServlet</servlet-class>
    <init-param>
        <param-name>container.script.static</param-name>
        <param-value>/WEB-INF/container.script</param-value>
    </init-param>
    <load-on-startup>1</load-on-startup>
</servlet>
```

`<load-on-startup>` 元素中的数字告诉 servlet 容器应该按什么顺序加载 servlet。 较低的数字首先被加载。 如果该值为负数或未指定，则 servlet 容器可以随时加载 servlet。

## Context 参数

可以设置一些上下文参数，这些参数可以从应用程序中的所有 servlet 中读取。
那该如何配置呢？

```xml
<context-param>
    <param-name>myParam</param-name>
    <param-value>the value</param-value>
</context-param>
```

如果获得这些参数呢？

```java
String myContextParam =
        request.getSession()
               .getServletContext()
               .getInitParameter("myParam");
```
