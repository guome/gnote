```xml
<com.google.android.material.bottomnavigation.BottomNavigationView
        android:id="@+id/bottom_navigation"
        android:layout_width="match_parent"
        android:layout_height="wrap_content"
        app:itemActiveIndicatorStyle="@style/App.Custom.Indicator"
        app:itemRippleColor="@null"
        android:background="@color/white"
        app:itemIconTint="@color/nav_color"
        app:itemTextColor="@color/nav_color"
        app:menu="@menu/bottom_navigation_menu" />
```

- itemRippleColor: 点击时候的水波纹颜色 @null 为移除水波纹(国内一般不需要水波纹)
- itemActiveIndicatorStyle: 点击时候文字上的背景 
- itemIconTint color 颜色
- itemTextColor 文字颜色

样式

```xml
<style name="App.Custom.Indicator" parent="Widget.Material3.BottomNavigationView.ActiveIndicator">
    <!-- 点击时 text 的背景图 -->
    <item name="android:color">@null</item>
</style>
```    

菜单

```xml
<?xml version="1.0" encoding="utf-8" ?>
<menu xmlns:android="http://schemas.android.com/apk/res/android">
    <item
        android:id="@+id/nav_home"
        android:enabled="true"
        android:icon="@drawable/ic_nav_home"
        android:title="@string/nav_home" />
    <item
        android:id="@+id/nav_day15"
        android:enabled="true"
        android:icon="@drawable/ic_nav_day15"
        android:title="@string/nav_day15" />
</menu>
```


```kotlin
// 默认选择哪个菜单
binding.bottomNavigation.selectedItemId = R.id.nav_day15
binding.bottomNavigation.setOnItemSelectedListener { item ->
    when (item.itemId) {
        R.id.nav_home -> {
            true
        }

        R.id.nav_day15 -> {
            true
        }

        else -> false
    }
}
// 角标
val badge = binding.bottomNavigation.getOrCreateBadge(R.id.nav_home)
// 显示角标
badge.isVisible = true
// 角标数字
badge.number = 99
```


```xml
<?xml version="1.0" encoding="utf-8"?>
<selector xmlns:android="http://schemas.android.com/apk/res/android">
    <item android:color="#5FA2FF" android:state_checked="true" />
    <item android:color="#999999" />
</selector>
```