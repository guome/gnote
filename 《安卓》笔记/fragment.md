## 创建

```kotlin

class MessageFragment : Fragment() {

    interface Callbacks {
        fun messageClick()
    }

    private var _binding: FragmentMessageBinding? = null

    private val binding get() = _binding!!

    private var callbacks: Callbacks? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentMessageBinding.inflate(layoutInflater, container, false)
        return _binding!!.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.callActivity.setOnClickListener {
            callbacks?.messageClick()
        }
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        // 这里这么写，只是给 activity 实现一下接口，方便通信，不过这么写其实不太好的
        // 把 context 赋值给 callbacks, 且强转为 Callbacks 类型
        // 注意：这里的 context 其实就是 activity 的实例，
        // 所以 activity 就必须实现 Callbacks 接口
        callbacks = context as Callbacks
    }

    override fun onDetach() {
        super.onDetach()
        // 这里必须要置为 null
        callbacks = null
    }

    fun show() {
        Toast.makeText(context, "activity 调用 fragment", Toast.LENGTH_SHORT).show()
    }


    companion object {
        @JvmStatic
        fun newInstance() = MessageFragment()
    }
}
```

在 activity 中添加

```kotlin

supportFragmentManager
                .beginTransaction()
                .add(R.id.fragment_container, MessageFragment.newInstance())
                .commit()

```


## 与 activity/fragment 通信

```kotlin
button.setOnClickListener {
    val result = "result"
    // Use the Kotlin extension in the fragment-ktx artifact.
    // 相当于 vue 中的 在 Fragment 往外抛出事件,相当于 this.$emit('requestKey', "data")
    // 这里说数据是给 fragmentManager 的
    setFragmentResult("requestKey", bundleOf("bundleKey" to result))
}
```

在 activity 中接受结果

```kotlin
class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        // 这里其实相当于 vue 中的 @requestKey="handleRes", 不过就是安卓中的写法会很麻烦
        // 如果是在 Fragment 中的话，fragmanager 就不是 supportFragmentManager 而是 childFragmentManager
        // 具体的 fragmanager 看 https://developer.android.com/guide/fragments/fragmentmanager?hl=zh-cn
        supportFragmentManager
                .setFragmentResultListener("requestKey", this) { requestKey, bundle ->
            // We use a String here, but any type that can be put in a Bundle is supported.
            val result = bundle.getString("bundleKey")
            // Do something with the result.
        }
    }
}
```