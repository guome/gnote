解析  json 一般使用 GSON

导包

```groovy
implementation("com.google.code.gson:gson:2.8.5")
```

```kotlin
fun main() {
    val app = """
            {
                "name": "apple",
                "age": 23
            }
        """.trimIndent()

    val appList = """
        [
            {
                "name": "apple",
                "age": 23
            },
            {
                "name": "google",
                "age": 13
            }
        ]
    """.trimIndent()

    val gson = Gson()
    // 解析 json
    val appInfo = gson.fromJson(app, AppInfo::class.java)
    println("name = ${appInfo.name}, age = ${appInfo.age}") // name = apple, age = 23

    // 解析数组
    val typeOf = object : TypeToken<List<AppInfo>>() {}.type
    val l = gson.fromJson<List<AppInfo>>(appList, typeOf)
    l.forEach {
      	// name = apple, age = 23
				// name = google, age = 13
        println("name = ${it.name}, age = ${it.age}")
    }
}

data class AppInfo(
    val name: String,
    val age: Int,
)
```

使用起来还是很方便的

但是需要注意一点，

```kotlin
val appList = """
    [
        {
            "name": "apple",
            "age": 23
        },
        {
            "name": "google",
            "age": 13
        }, //  这里不要加逗号，否则解析出来的数组，最后一个元素是 null
    ]
""".trimIndent()

```