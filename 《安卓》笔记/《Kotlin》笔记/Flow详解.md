# 什么是 Kotlin Flow

Flow 是 Kotlin 协程库的一部分，用于处理异步数据流。你可以把它看成是一种“冷”数据流，只有在开始收集时才会开始生成数据。相比传统的回调或 LiveData，Flow 提供了更流畅的异步数据处理方式，支持响应式编程的需求。

# Flow 的基础知识

• 冷流：Flow 不会主动推送数据，只有在调用 collect 时才开始生成数据。
• 协程支持：Flow 与协程紧密集成，支持在不同的协程作用域中异步处理。
• 取消支持：Flow 会自动响应协程取消，避免资源浪费。
• 背压处理：Flow 可以对数据流速率进行控制，适用于快速流式数据的处理。

# 基本使用示例

要开始使用 Flow，先要确保添加依赖。对于非 Android 项目，添加以下依赖即可：

```
implementation("org.jetbrains.kotlinx:kotlinx-coroutines-core:1.6.0")
```

如果是 Android 项目，则还需要添加 Android 的依赖：

```
implementation("org.jetbrains.kotlinx:kotlinx-coroutines-android:1.6.0")
```

# 如何创建一个 Flow

Flow 的基本构建器是 `flow {}`，通过 emit() 函数逐步发送数据。以下是一个简单的 Flow 构建和收集的例子。

创建一个简单的 Flow

```kotlin
import kotlinx.coroutines.*
import kotlinx.coroutines.flow.*

fun simpleFlow(): Flow<Int> = flow {
    for (i in 1..3) {
        emit(i) // 发送数据项
        delay(100) // 模拟延迟
    }
}

fun main() = runBlocking {
    // 收集 Flow 中的数据
    simpleFlow().collect { value ->
        println("Received $value")
    }
}
```

输出：

> Received 1
> Received 2
> Received 3

# Flow 常用操作符

Flow 提供了一些操作符来处理数据流，这些操作符类似于 RxJava 里的操作符，比如 map、filter 等。

常用操作符

> map：将数据流中的每个元素转换成另一种类型。
> filter：筛选出符合条件的数据。
> take：只接收流的前几个元素。
> reduce：将流中的元素聚合为一个值。

操作符示例

```kotlin
fun main() = runBlocking {
    simpleFlow()
        .map { it * 2 } // 每个元素乘以 2
        .filter { it > 2 } // 只保留大于 2 的元素
        .collect { value -> println(value) } // 收集结果
}
```

输出：

> 4
> 6

# Flow 的上下文切换

Flow 的每个操作符都是在调用它的协程上下文中运行的。可以使用 flowOn 指定 Flow 的执行上下文，比如将 Flow 的生成部分放在 `Dispatchers.IO`，而收集部分仍然在 Main 线程上运行。

上下文切换示例

```kotlin
fun simpleFlowWithContext(): Flow<Int> = flow {
    for (i in 1..3) {
        emit(i)
    }
}.flowOn(Dispatchers.IO) // 指定在 IO 线程上执行

fun main() = runBlocking {
    simpleFlowWithContext().collect { value ->
        println("Received $value on ${Thread.currentThread().name}")
    }
}
```

# 异常处理

可以使用 catch 操作符来捕获 Flow 中的异常，并在 onCompletion 操作符中执行收尾工作。

```kotlin
fun flowWithError(): Flow<Int> = flow {
    emit(1)
    throw RuntimeException("An error occurred") // 模拟错误
}

fun main() = runBlocking {
    flowWithError()
        .catch { e -> println("Caught exception: $e") }
        .onCompletion { println("Flow completed") }
        .collect { value -> println(value) }
}
```

输出：

> 1
> Caught exception: java.lang.RuntimeException: An error occurred
> Flow completed

# 流的生命周期管理

Flow 会在协程作用域被取消时自动停止生成数据。在 Android 中，通常会与 Lifecycle 配合使用，在生命周期变化时控制 Flow 的启动和取消。

# 高级操作符

• combine：可以将多个 Flow 的值组合在一起，适用于多个数据流的情况。
• zip：按顺序组合两个 Flow 中的值。
• flatMapConcat、flatMapMerge、flatMapLatest：用来处理嵌套 Flow 或发射新 Flow 的情况。

# 使用场景

Kotlin Flow 适用于以下几种情况：

• 网络请求：适合处理网络数据流的情况，比如动态更新的天气信息或新闻数据。
• 数据库监听：可以与 Room 数据库结合，通过 Flow 实现数据变化的响应式更新。
• 用户交互事件流：可以用 Flow 处理点击事件、手势识别等一系列异步事件。

# 总结

• Flow 是 Kotlin 协程的一部分，用于处理异步数据流。
• 基本操作：创建 Flow（flow）、收集数据（collect）、操作符（map、filter）。
• 流的控制：利用 flowOn 指定执行上下文，使用 catch 和 onCompletion 处理异常。
• 高级操作符：利用 combine、zip 和 flatMap 等操作符处理复杂的数据流。

Kotlin Flow 提供了更简洁、灵活的异步处理方式，能够高效应对现代开发中的流式数据处理需求。

# flow

`flow {}` 是 Kotlin 中用来创建 Flow 的构建器，它允许我们在协程内定义一个数据流，逐步发射数据。`flow {}` 是 Flow 的一个工厂函数，它会返回一个 Flow 实例，我们可以在 collect 中收集到这个 Flow 的数据。

`flow {}` 是如何工作的？

`flow {}` 构建器会启动一个代码块，并在其中使用 `emit()` 函数逐步发射数据。每当 `emit()` 被调用时，Flow 就会发射一个新数据项。当 collect 被调用时，数据流将开始生成数据，每次 emit 发射的数据会依次传递到 collect 的 lambda 表达式中。

基本示例

下面是一个简单的 Flow 示例，其中通过 `flow {}` 创建了一个整数流，并在 collect 时收集并打印这些数据。

```kotlin
import kotlinx.coroutines.*
import kotlinx.coroutines.flow.*

fun createSimpleFlow(): Flow<Int> = flow {
    for (i in 1..3) {
        emit(i) // 逐个发射 1, 2, 3
        delay(100) // 模拟延迟
    }
}

fun main() = runBlocking {
    createSimpleFlow().collect { value -> // 收集流中的数据
        println(value)
    }
}
```

输出：

1
2
3

在这个示例中：

1. createSimpleFlow 函数返回了一个 Flow，它在每次循环中调用 emit() 发射数据。
2. collect 函数用于接收 Flow 中的数据，并在收到每个数据项时执行给定的代码块。

`flow {}` 的用途

• 逐步发射数据：适合处理逐步生成的数据，比如连续的网络请求结果、实时数据流。
• 控制数据生成时机：flow 是冷的，只有当 collect 被调用时才会真正执行。
• 与协程集成：`flow {}` 中可以使用协程函数，比如 delay，来异步生成数据流。

`flow {}` 的注意事项

• `flow {}` 内的代码在 collect 时才会开始执行，这是 冷流 的特性。
• `flow {}` 需要在协程作用域中运行，因此必须在 runBlocking 或 CoroutineScope 内调用。
• `flow {}` 可以通过 emit 发射多个数据项，可以按需引入延迟或异步逻辑，确保数据按时序流出。

使用 `flow {}` 结合异常处理

`flow {}` 支持异常处理，我们可以在 catch 中捕获异常，在 onCompletion 中添加完成逻辑。

```kotlin
fun createFlowWithException(): Flow<Int> = flow {
    emit(1)
    throw RuntimeException("Error in flow!")
}

fun main() = runBlocking {
    createFlowWithException()
        .catch { e -> println("Caught exception: $e") }
        .collect { value -> println(value) }
}
```

## 总结

`flow {}` 是创建 Flow 的核心构建器，它允许我们定义一个流式数据源，按需生成异步数据。利用 emit 可以发射多个数据项，通过 collect 可以逐步收集这些数据，并进行处理。

# collect 详解

collect 是 Kotlin Flow 的一个终端操作符，用于从 Flow 中逐个收集数据。可以把 collect 想象成一个接收器，所有流中的数据会经过 collect 进入到它的代码块中进行处理。collect 的调用也是 Flow 的一个生命周期的起点，只有当调用 collect 时，Flow 才会开始执行数据流的发射逻辑。

## collect 的基本用法

通常，我们在创建 Flow 后，调用 collect 来接收并处理流中的每个数据项：

```kotlin
import kotlinx.coroutines.*
import kotlinx.coroutines.flow.*

fun simpleFlow(): Flow<Int> = flow {
    emit(1)
    emit(2)
    emit(3)
}

fun main() = runBlocking {
    simpleFlow().collect { value ->
        println("Received $value") // 对流中的每个值进行处理
    }
}
```

输出：

Received 1
Received 2
Received 3

在这个例子中：

• collect { value -> println(value) } 表示我们想要收集 Flow 中的每个数据项，并在接收到数据时执行 println 操作。
• collect 是阻塞的，它会等待当前 Flow 的所有数据都被收集完毕后才会继续执行后面的代码。

## collect 的用途

    •	终端操作符：collect 是一个终端操作符，表示一个 Flow 的终止。调用 collect 后，数据才会从 Flow 中开始流动，前面的所有操作符（如 map、filter）都会在 collect 的数据收集过程中触发。
    •	处理每个数据项： collect 是 Flow 数据项的处理接收器，可以在 collect 内部处理流中发射的数据，比如打印、计算或发送到其他函数中。
    •	配合其他操作符：可以将 collect 与其他操作符（例如 map、filter 等）结合，进行复杂的数据流操作。

## collect 的用法示例

使用操作符后收集数据

可以在 Flow 中先用 map 转换数据，然后在 collect 中处理。

```kotlin
fun main() = runBlocking {
    (1..5).asFlow()
        .map { it * 2 }
        .collect { value ->
            println("Doubled value: $value")
        }
}
```

输出：

Doubled value: 2
Doubled value: 4
Doubled value: 6
Doubled value: 8
Doubled value: 10

## collect 的取消和异常处理

collect 支持协程的取消和异常处理。Flow 中的 collect 可以在协程被取消时停止数据收集，同时我们可以使用 try-catch 或 catch 操作符来处理异常。

```kotlin
fun faultyFlow(): Flow<Int> = flow {
    emit(1)
    emit(2)
    throw RuntimeException("Error!")
    emit(3)
}

fun main() = runBlocking {
    try {
        faultyFlow().collect { value ->
            println("Collected: $value")
        }
    } catch (e: Exception) {
        println("Caught exception: $e")
    }
}
```

输出：

Collected: 1
Collected: 2
Caught exception: java.lang.RuntimeException: Error!

## collect 与并发操作

可以使用 collectLatest 来处理最新的数据项，而在新数据项到来时取消旧的处理过程，避免数据处理阻塞。

```kotlin
fun main() = runBlocking {
    (1..3).asFlow().onEach { delay(100) }
        .collectLatest { value ->
            println("Processing $value")
            delay(150) // 模拟长时间处理
            println("Done $value")
        }
}
```

输出：

Processing 1
Processing 2
Processing 3
Done 3

这里，只有最新的数据项会被处理完成，前一个数据项的处理会在新数据项到来时被取消。

## collect 的背压控制

在高频数据流中，可以通过 collect 和操作符（例如 buffer、conflate）来控制数据的收集速率，避免背压问题。例如使用 conflate 来丢弃多余的数据，或用 buffer 来对数据进行缓存：

```kotlin
fun main() = runBlocking {
    (1..5).asFlow().onEach { delay(100) }
        .conflate() // 忽略一些中间值
        .collect { value ->
            delay(300) // 模拟处理延迟
            println(value)
        }
}
```

## 总结

• collect 是 Flow 的终端操作符，用来从流中接收数据。
• 它在调用时会启动数据流的发射，逐个收集 Flow 中的数据项。
• collect 支持与其他操作符配合使用，也支持协程的取消与异常处理。
• 在需要处理并发数据流或控制背压时，可以结合 collectLatest、buffer、conflate 等操作符来优化收集逻辑。

# collectLatest 详解

collectLatest 是 Kotlin Flow 中的一个终端操作符，用于从 Flow 中收集最新的数据项。在处理高频率的数据流时，collectLatest 可以确保每次只处理最新的数据项，如果在处理一个数据项时又有新的数据项到达，collectLatest 会取消当前的处理并立即处理最新的项。

collectLatest 的关键特点是：在新数据项到来时，会自动取消对旧数据项的处理。这对于需要高响应性的场景非常有用，比如用户频繁操作时的响应处理、数据实时更新的 UI 场景等。

collectLatest 的用法

```kotlin
fun main() = runBlocking {
    (1..3).asFlow().onEach { delay(100) } // 每100ms发射一个数据项
        .collectLatest { value ->
            println("Processing $value")
            delay(200) // 模拟一个长时间的处理
            println("Done $value")
        }
}
```

输出：

Processing 1
Processing 2
Processing 3
Done 3

## 工作原理

在这个示例中：

• 每 100 毫秒发射一个数据项，依次是 1、2 和 3。
• 每个数据项的处理时间为 200 毫秒。因为 collectLatest 的特性，在处理第一个数据项 1 时，数据项 2 很快到达，导致第一个处理被取消，同样，2 的处理也被 3 取消。
• 只有最新的数据项（即 3）最终被完整处理。

collectLatest 使用场景

1. 用户输入或滚动事件：比如在搜索栏输入时，只处理最新的输入，避免重复触发。
2. 实时数据更新：在快速变动的数据源中（如传感器数据、实时数据），仅关注最新数据，忽略过时的数据。
3. 避免资源浪费：在网络请求中，防止因数据流密集而进行不必要的计算和资源消耗。

collect 与 collectLatest 的区别

• collect：会处理流中的所有数据项，不会取消当前的处理任务。
• collectLatest：在新数据项到达时会取消当前数据项的处理，只处理最新的数据项。

## 实战示例

示例 1：用户快速输入场景

```kotlin
import kotlinx.coroutines.*
import kotlinx.coroutines.flow.*

fun userTypingFlow(): Flow<String> = flow {
    val words = listOf("Hello", "Hello W", "Hello Wo", "Hello Wor", "Hello World")
    for (word in words) {
        emit(word)
        delay(100) // 模拟用户输入的延迟
    }
}

fun main() = runBlocking {
    userTypingFlow()
        .collectLatest { value ->
            println("Searching for $value")
            delay(300) // 模拟搜索延迟
            println("Search complete for $value")
        }
}
```

输出：

Searching for Hello World
Search complete for Hello World

在这个示例中：

• collectLatest 确保只处理最新的搜索查询，避免了不必要的处理。
• 每次新输入到达时，旧的搜索会被取消，最终只对最后输入的 “Hello World” 执行完整的搜索逻辑。

示例 2：网络请求的优化

当多个数据项代表一系列快速到达的请求时（例如用户快速切换标签页或页面时），可以利用 collectLatest 限制只加载最新请求的数据。

```kotlin
fun dataRequestFlow(): Flow<Int> = flow {
    for (i in 1..5) {
        emit(i)
        delay(50) // 模拟请求发起的间隔
    }
}

fun main() = runBlocking {
    dataRequestFlow()
        .collectLatest { value ->
            println("Loading data for page $value")
            delay(100) // 模拟请求数据加载时间
            println("Loaded data for page $value")
        }
}
```

输出：

Loading data for page 5
Loaded data for page 5

在这个示例中：

• 由于网络请求的加载时间较长，只会完成最新的请求（即 page 5），从而避免了不必要的加载。

## 总结

• collectLatest 是 Flow 的终端操作符，适用于只想处理最新数据的场景。
• 在新数据项到达时，collectLatest 会取消当前正在处理的项，从而提高响应速度。
• 常用于用户输入、滚动事件和频繁更新的 UI 数据等高频场景。

这使得 collectLatest 成为在处理实时数据流、UI 更新和高频事件等方面的一个非常高效的工具。
