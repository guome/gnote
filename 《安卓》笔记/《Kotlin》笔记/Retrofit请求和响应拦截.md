# Retrofit 请求和响应拦截

在使用 Retrofit 进行网络请求时，可能会有一些场景需要对请求或响应进行拦截处理，比如添加统一的请求头、记录日志、修改请求或响应数据等。Retrofit 本身并不直接提供请求和响应的拦截功能，但是它使用了 OkHttp 作为底层 HTTP 客户端，而 OkHttp 提供了非常灵活的拦截器机制，可以实现对请求和响应的拦截。

请求和响应拦截的两种方式：

- 应用拦截器（Application Interceptor）：在 Retrofit 发起请求之前和收到响应之后对请求和响应进行修改。
- 网络拦截器（Network Interceptor）：在请求发送到网络之前和从网络接收到响应之后进行拦截，可以看到网络传输的数据。

## 应用拦截器和网络拦截器的区别

| 特点                         | 应用拦截器                      | 网络拦截器                        |
| ---------------------------- | ------------------------------- | --------------------------------- |
| 触发时机                     | 在所有重定向和重试之前          | 在网络调用和接收数据时            |
| 是否拦截重定向               | 否                              | 是                                |
| 是否拦截缓存响应             | 是                              | 否                                |
| 是否影响请求和响应的重复使用 | 是（会使请求/响应不可重复使用） | 否（不会影响请求/响应的重复使用） |

添加拦截器的步骤

- 在 OkHttpClient 中添加拦截器。
- 使用 Retrofit 的 client() 方法将 OkHttpClient 与 Retrofit 关联。

## 如何实现请求和响应的拦截

### 请求和响应拦截器示例

以下是一个拦截器的示例，可以在请求时添加一个通用的 Authorization 头，并在响应中记录返回的状态码和响应时间。

```kotlin
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.io.IOException

// 创建请求拦截器，添加通用的 Authorization 头
class AuthInterceptor : Interceptor {
    @Throws(IOException::class)
    override fun intercept(chain: Interceptor.Chain): Response {
        // 获取原始请求
        val originalRequest = chain.request()

        // 添加头信息到请求中
        val newRequest = originalRequest.newBuilder()
            .header("Authorization", "Bearer your_token_here") // 添加 Authorization 头
            .build()

        // 继续请求链
        return chain.proceed(newRequest)
    }
}

// 创建响应拦截器，记录请求时间和状态码
class LoggingInterceptor : Interceptor {
    @Throws(IOException::class)
    override fun intercept(chain: Interceptor.Chain): Response {
        // 记录请求的开始时间
        val request = chain.request()
        val startTime = System.nanoTime()

        // 执行请求
        val response = chain.proceed(request)

        // 记录响应时间和状态码
        val endTime = System.nanoTime()
        val duration = (endTime - startTime) / 1e6

        println("Request time: $duration ms")
        println("Response code: ${response.code}")

        return response
    }
}

// 构建 OkHttpClient，添加拦截器
val client = OkHttpClient.Builder()
    .addInterceptor(AuthInterceptor())     // 添加请求拦截器
    .addInterceptor(LoggingInterceptor())  // 添加响应拦截器
    .build()

// 使用 OkHttpClient 构建 Retrofit 实例
val retrofit = Retrofit.Builder()
    .baseUrl("https://api.example.com")
    .client(client)
    .addConverterFactory(GsonConverterFactory.create())
    .build()
```

## 自定义拦截器使用场景

### 添加统一请求头

在每个请求中添加固定的请求头信息（如 Authorization 令牌或 User-Agent）。

```kotlin
class HeaderInterceptor : Interceptor {
    override fun intercept(chain: Interceptor.Chain): Response {
        val originalRequest = chain.request()
        val newRequest = originalRequest.newBuilder()
            .addHeader("Authorization", "Bearer token_here")
            .addHeader("User-Agent", "Retrofit-Sample-App")
            .build()
        return chain.proceed(newRequest)
    }
}
```

### 记录日志

使用 OkHttp 的 LoggingInterceptor，能够方便地记录请求和响应的日志。

```kotlin
val logging = HttpLoggingInterceptor().apply {
    level = HttpLoggingInterceptor.Level.BODY
}

val client = OkHttpClient.Builder()
    .addInterceptor(logging)
    .build()
```

### 处理响应错误

拦截响应，处理 HTTP 错误状态码，例如，当遇到 401（未授权）或 500（服务器错误）时进行特定的处理。

```kotlin
class ErrorInterceptor : Interceptor {
    override fun intercept(chain: Interceptor.Chain): Response {
        val response = chain.proceed(chain.request())

        // 检查状态码
        if (response.code == 401) {
            // 处理未授权错误，可能需要刷新 Token
            println("Unauthorized! Please login again.")
        }

        return response
    }
}
```

## 使用 OkHttp 内置的 HttpLoggingInterceptor

OkHttp 提供了内置的 HttpLoggingInterceptor，用于方便地记录 HTTP 请求和响应的详细信息，包括请求头、请求体、响应头和响应体。这在调试过程中非常有用。

```kotlin
import okhttp3.logging.HttpLoggingInterceptor

val logging = HttpLoggingInterceptor().apply {
    level = HttpLoggingInterceptor.Level.BODY  // 可以选择 BODY, HEADERS, BASIC 等不同的日志级别
}

val client = OkHttpClient.Builder()
    .addInterceptor(logging)
    .build()
```

## 应用场景总结

• 身份验证：通过拦截器给所有请求添加 Authorization 头，进行身份验证。
• 日志记录：记录请求和响应的日志信息，方便调试和排查问题。
• 错误处理：对响应状态码进行统一处理，比如 401（未授权）时自动刷新 Token。
• 缓存控制：通过拦截器设置缓存策略，避免频繁请求网络。

## 总结

通过 OkHttp 的拦截器机制，我们可以非常灵活地处理 Retrofit 请求和响应的各个阶段。无论是添加统一的请求头、记录日志、处理响应错误，还是修改请求和响应数据，拦截器都能提供强大的扩展能力。
