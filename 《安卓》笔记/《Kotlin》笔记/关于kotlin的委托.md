# 关于 kotlin 的委托

Kotlin 的 委托（Delegation） 是一种设计模式，允许对象将其部分职责委托给另一个对象来处理。Kotlin 提供了内置的委托支持，包括类委托和属性委托，简化了某些设计模式的实现，并让代码更加简洁和易于维护。

## 1. 类委托（Class Delegation）

Kotlin 支持使用 by 关键字来实现类委托。通过类委托，一个类可以将其接口的实现委托给另一个对象，这样避免了重复编写同样的代码。

语法

```kotlin
interface Worker {
    fun work()
}

class Employee : Worker {
    override fun work() {
        println("Employee is working")
    }
}

class Manager(worker: Worker) : Worker by worker
```

在这个例子中：

• Manager 类没有显式地实现 work() 方法，而是将其职责委托给了传入的 Worker 对象。
• Manager 通过 by worker 关键字实现了委托，从而复用了 Employee 中的 work() 实现。

示例解释

```kotlin
fun main() {
    val employee = Employee()
    val manager = Manager(employee)
    manager.work() // 输出: Employee is working
}
```

• Manager 接收到的 work() 调用被委托给了 Employee，因为我们通过 by worker 将 work() 的实现委托给了 Employee。

## 2. 属性委托（Property Delegation）

属性委托是一种机制，允许你将属性的 getter 和 setter 逻辑委托给另一个对象。Kotlin 内置了对属性委托的支持，例如 lazy、observable 等委托方法。

属性委托的语法

```kotlin
class Example {
    var myProp: String by Delegate()
}

class Delegate {
    operator fun getValue(thisRef: Any?, property: KProperty<*>): String {
        return "This is the value of '${property.name}'"
    }

    operator fun setValue(thisRef: Any?, property: KProperty<*>, value: String) {
        println("The new value of '${property.name}' is $value")
    }
}
```

• getValue 和 setValue：委托类需要实现 getValue 和 setValue 运算符函数，分别用于读取和写入属性的值。

```kotlin
fun main() {
    val e = Example()
    println(e.myProp)  // 输出: This is the value of 'myProp'
    e.myProp = "New Value" // 输出: The new value of 'myProp' is New Value
}
```

在这个例子中，Example 类将 myProp 属性的访问委托给了 Delegate 对象。

## Kotlin 提供的常用委托

1. lazy：惰性初始化，只有在第一次访问时才会计算值，之后会缓存该值。

```kotlin
val lazyValue: String by lazy {
    println("Computed!")
    "Hello, Kotlin"
}

fun main() {
    println(lazyValue) // 输出: Computed! Hello, Kotlin
    println(lazyValue) // 输出: Hello, Kotlin
}
```

• 第一次访问时会计算并输出“Computed!”。
• 第二次访问时，lazyValue 已经缓存了值，不会重新计算。

2. observable：当属性的值发生变化时，会触发回调函数。

```kotlin
var observableProp: String by Delegates.observable("Initial Value") {
    property, oldValue, newValue ->
    println("Property ${property.name} changed from $oldValue to $newValue")
}

fun main() {
    observableProp = "New Value"  // 输出: Property observableProp changed from Initial Value to New Value
}
```

• Delegates.observable 允许你在属性值变化时执行某些操作，比如日志记录或其他行为。

3. vetoable：与 observable 类似，但可以通过回调决定是否接受新的属性值。

```kotlin
var vetoableProp: Int by Delegates.vetoable(0) {
    _, oldValue, newValue -> newValue > oldValue
}

fun main() {
    vetoableProp = 5  // 成功赋值
    vetoableProp = 3  // 被拒绝，不会改变
}
```

• 这个委托允许你通过返回 true 或 false 来决定是否接受属性的新值。上例中，newValue 必须比 oldValue 大才会被接受。

## 3. 自定义属性委托

Kotlin 允许你创建自己的属性委托，前面已经展示了一个简单的自定义属性委托示例。委托类需要实现 getValue 和 setValue 运算符函数。

示例：缓存属性的自定义委托

```kotlin
class CacheDelegate {
    private var cache: String? = null

    operator fun getValue(thisRef: Any?, property: KProperty<*>): String {
        return cache ?: "Default Value"
    }

    operator fun setValue(thisRef: Any?, property: KProperty<*>, value: String) {
        cache = value
    }
}

class Data {
    var cachedData: String by CacheDelegate()
}

fun main() {
    val data = Data()
    println(data.cachedData)  // 输出: Default Value
    data.cachedData = "New Cached Data"
    println(data.cachedData)  // 输出: New Cached Data
}
```

在这个示例中，CacheDelegate 将属性的值缓存下来，提供了一种懒加载或缓存属性的机制。

## 4. 类委托 vs 属性委托

• 类委托：适用于将接口或功能的实现委托给另一个对象或类。常用于设计模式中的组合或装饰器模式（如在类中引入第三方库的功能）。
• 属性委托：适用于为属性的 getter/setter 提供自定义逻辑，或者使用 lazy、observable 等委托类简化属性的生命周期管理。

## 总结

• 类委托：通过 by 关键字将接口的实现委托给另一个对象，简化了组合模式的实现。
• 属性委托：允许你自定义属性的读取和写入行为，或者使用现有的委托（如 lazy、observable）来简化属性的管理。

# 为什么 @Composable 的方法可以用作属性委托

在 Jetpack Compose 中，@Composable 函数与 Kotlin 的委托机制可以很好地结合使用。尽管 @Composable 本身并不是一种属性委托，但 Compose 提供了一种特殊的模式，允许你将 @Composable 函数的结果存储为属性并自动重组。这种模式是通过使用 Compose 特定的内置委托来实现的，例如 remember 和 rememberDelegate。

## 核心机制

Compose 的 remember 和 rememberDelegate 提供了一种方式，让你在 @Composable 函数中保存状态或计算值，并在组合过程中缓存这些值。remember 允许将 @Composable 函数返回的值缓存为属性，从而避免每次重新组合时都重新计算这些值。

1. remember 的工作方式

在 Compose 中，remember 是一种特殊的委托，它会在组合的生命周期中保存某个值，并且当 Composable 函数重新组合时，保持该值不变，直到组合树被销毁。

使用 remember 的方式可以看作是 一种属性委托的形式，它允许我们像对待常规属性一样对待 @Composable 的值。

remember 的示例

```kotlin
@Composable
fun MyComposable() {
    val state by remember { mutableStateOf("Hello") }

    Text(text = state)
}
```

在这个示例中：

• `remember { mutableStateOf("Hello") }` 会在 Composable 函数首次执行时计算并保存这个值，而不是在每次重新组合时都重新计算它。
• by 关键字用于委托给 remember 的返回值。这意味着 state 是一个被 remember 缓存的值。

这表明 @Composable 函数的结果或状态能够通过 remember 委托来缓存和管理。

2. rememberDelegate 的工作原理

在 Jetpack Compose 中，remember 实际上实现了类似于 Kotlin 属性委托的功能，它将其返回值作为属性来使用，并确保这些值在 Composable 函数生命周期内保持一致。在实际代码中，remember 的结果能够通过 by 语法委托给一个属性。

这是因为 remember 会在第一次组合时计算某个值，并在随后的重组过程中返回相同的值，而不会重新计算。这样，remember 的行为与 Kotlin 的属性委托机制非常类似，因此可以用作属性委托。

具体实现

```kotlin
@Composable
fun MyButton() {
    var counter by remember { mutableStateOf(0) }

    Button(onClick = { counter++ }) {
        Text("Clicked $counter times")
    }
}
```

在这个示例中：

• counter 属性被 remember 缓存，并且在每次组合时保持不变。
• remember 的行为类似于委托，它会在初次组合时执行计算，并将值缓存下来，以便后续访问时使用。

为什么 @Composable 可以用作属性委托？

这是因为 Jetpack Compose 内部为 remember、mutableStateOf 等函数提供了委托支持，而这些函数实现了类似属性委托的功能。

其原理如下：

1. 组合模型：@Composable 函数本质上是声明式 UI 函数，它会在界面状态变化时触发重组。为了避免在每次重组时重新计算某些值，Compose 通过 remember 实现了缓存机制。
2. 状态管理：mutableStateOf 返回一个 State 对象，允许通过 by 委托来自动获取或更新状态值。remember 会记住（缓存）这个状态对象，使其在重新组合时保持不变。
3. 属性委托机制：Kotlin 提供了标准的属性委托机制，即 getValue 和 setValue。Jetpack Compose 扩展了这一机制，并将其用于 @Composable 的状态管理。

因此，remember 和 mutableStateOf 实现了一个类似于属性委托的系统，允许你将 @Composable 函数的值通过 by 关键字委托给属性，并在重新组合过程中保持这些值不变。

## 总结

• remember 和 mutableStateOf 是 Jetpack Compose 提供的特定功能，它们的行为与属性委托类似，允许你将 @Composable 函数的值缓存为属性，并在重组时复用它们。
• @Composable 函数本身并不是属性委托，但通过 remember，你可以将它们的结果委托给属性，并在组合过程中保持这些值的稳定性。
