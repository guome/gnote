# kotlin 内链函数

在 Kotlin 中，内链函数（also known as inline functions）是通过 inline 关键字修饰的函数。它的主要作用是在编译时将函数的调用替换为函数的实际代码，避免函数调用的开销，尤其是在高阶函数（函数作为参数或返回值）的场景中提高性能。

以下是关于 Kotlin 内链函数的详细介绍：

1. 基本用法

当你使用高阶函数时，函数调用通常会有额外的开销（创建函数对象和闭包）。通过使用 inline 关键字，Kotlin 可以减少这些开销，因为函数的调用会被编译器替换为实际的函数体。

```kotlin
inline fun inlineExample(action: () -> Unit) {
    println("Before action")
    action() // 内链函数，在调用时不会有函数调用的开销
    println("After action")
}

fun main() {
    inlineExample {
        println("Inside action")
    }
}
```

输出：

Before action
Inside action
After action

在这里，inlineExample 是一个内链函数，action 是一个高阶函数。编译器会在调用 inlineExample 时，将 action 的函数体直接嵌入到调用处，从而减少性能损耗。

2. 非内链参数 noinline

内链函数的所有参数默认都会被内联到调用点，但有时你可能不希望某些参数被内联，这时可以使用 noinline 关键字。

```kotlin
inline fun inlineWithNoInline(inlineLambda: () -> Unit, noinline nonInlineLambda: () -> Unit) {
    inlineLambda() // 这个会被内联
    nonInlineLambda() // 这个不会被内联
}
```

3. crossinline 关键字

有时候你可能希望传递的 lambda 表达式不能有返回语句，这时可以使用 crossinline。它要求传入的 lambda 表达式必须在当前上下文中执行，不能使用非局部返回。

```kotlin
inline fun crossinlineExample(crossinline action: () -> Unit) {
    val runnable = Runnable {
        action() // 不能返回到外层函数
    }
    runnable.run()
}
```

4. 内联和递归函数

内联函数不能是递归的，因为编译器无法在编译时知道递归的调用深度。但你可以使用常规的函数调用递归。

总结

• 内联函数：避免函数调用的开销，尤其是在高阶函数场景中。
• noinline：用于避免某些 lambda 被内联。
• crossinline：确保 lambda 不能进行非局部返回。

内链函数在 Kotlin 中是一种优化高阶函数性能的手段，特别适合需要频繁调用的小函数。

## 举个例子，内链函数编译后的代码

为了更好地理解 Kotlin 的内链函数是如何在编译后工作的，我们可以从理论上展示 Kotlin 编译器如何处理内联函数。由于 Kotlin 编译器会将内联函数展开到调用处，因此不会有额外的函数调用开销。

以下是一个简单的 Kotlin 内联函数例子，以及编译后可能生成的等效 Java 代码。

Kotlin 代码

```kotlin
inline fun performAction(action: () -> Unit) {
    println("Action starts")
    action()
    println("Action ends")
}

fun main() {
    performAction {
        println("Doing something")
    }
}
```

内联后的等效 Java 代码

假设我们用 Java 来模拟 Kotlin 编译器的内联行为，编译后的代码会直接展开 performAction 函数体，而不是生成一次函数调用。以下是等效的 Java 代码：

```kotlin
public class InlineExample {
    public static void main(String[] args) {
        // 相当于内联了 performAction 函数的代码
        System.out.println("Action starts");
        System.out.println("Doing something");  // 这里是 action 的函数体被内联了
        System.out.println("Action ends");
    }
}
```

在上面的 Java 代码中，Kotlin 编译器已经将 performAction 函数的内容直接插入到 main 函数中，而不是创建一个额外的函数调用。这就避免了调用 performAction 的开销。

Kotlin 内联函数编译后的实际效果

在真实的 Kotlin 编译过程中，内联函数会被直接展开（类似上面展示的），特别是对于高阶函数的情况。下面我们看一个更复杂的情况。

Kotlin 代码 - 高阶函数场景

```kotlin
inline fun performWithResult(action: () -> Int): Int {
    println("Before action")
    val result = action()
    println("After action")
    return result
}

fun main() {
    val result = performWithResult {
        println("Calculating result")
        42  // 返回结果
    }
    println("Result: $result")
}
```

内联后的等效 Java 代码

```kotlin
public class InlineExample {
    public static void main(String[] args) {
        // 相当于 performWithResult 被内联展开
        System.out.println("Before action");
        System.out.println("Calculating result");
        int result = 42;  // 这里是 action 的函数体被内联了
        System.out.println("After action");

        System.out.println("Result: " + result);
    }
}
```

解释

1. 内联展开：performWithResult 函数被展开，action 的内容直接替换进函数调用的位置。因此，println("Calculating result") 和 return 42 直接插入了调用处。
2. 性能优化：由于没有生成额外的函数调用，编译后的代码运行时不需要创建闭包对象，也不需要进行函数栈帧的切换，从而提高了性能。

这就是 Kotlin 内联函数的一个重要特点：在编译时直接展开函数内容，从而避免了高阶函数调用带来的性能开销。

## 编译后的内链函数内定义的变量是否会和外界冲突

Kotlin 编译器在处理内联函数时，会确保内联函数内定义的局部变量不会与外界的变量冲突。尽管内联函数的代码在编译时被展开到调用处，但变量名冲突的问题是通过在编译过程中对局部变量进行“作用域隔离”来解决的。

变量名冲突避免机制

为了防止内联函数中的局部变量与外部作用域的变量发生冲突，Kotlin 编译器会为内联函数中的局部变量生成唯一的名字。这是通过“名称重写”的方式完成的。换句话说，编译器在内联展开时，会为每个局部变量生成唯一的名称，从而避免与调用者代码中的局部变量重名。

例子

我们来看一个例子，展示内联函数中的局部变量如何避免与外部变量冲突。

Kotlin 代码

```kotlin
inline fun inlineFunction() {
    val number = 100  // 内联函数中的局部变量
    println("Inside inline function: $number")
}

fun main() {
    val number = 42  // 外部作用域中的变量
    inlineFunction()
    println("Outside inline function: $number")
}
```

在这个例子中，内联函数 inlineFunction 定义了一个局部变量 number，并且在 main 函数中也定义了一个同名的 number 变量。在内联函数展开之后，编译器会生成唯一的变量名，避免冲突。

编译后的等效 Java 代码

```Java
public class InlineExample {
    public static void main(String[] args) {
        int number = 42;  // 外部作用域中的变量

        // 内联了 inlineFunction 的代码
        int number_inlineFunction = 100;  // 编译器对内联函数中的变量重命名，避免冲突
        System.out.println("Inside inline function: " + number_inlineFunction);

        System.out.println("Outside inline function: " + number);
    }
}
```

解释

• 在 main 函数中，number 变量被定义为 42，这个变量属于 main 的作用域。
• 内联函数 inlineFunction 中的 number 变量在编译时被重命名为 number_inlineFunction，从而避免了与外部的 number 变量冲突。

注意事项

1. 作用域隔离：内联函数中的局部变量仅在该函数内可见，即使内联展开到调用处，编译器也会确保这些变量的作用域被隔离开。
2. 变量重命名：如果内联函数与外部作用域的变量有同名变量，编译器会自动对内联函数中的局部变量进行重命名，防止冲突。

总结

Kotlin 内联函数编译后的代码中，局部变量不会与外界变量发生冲突。Kotlin 编译器通过对局部变量进行名称重写，确保了变量的作用域隔离。

## 应用场景

Kotlin 的内联函数（inline functions）主要应用于高阶函数的场景，尤其是在需要频繁调用的函数中，它能够优化性能，减少函数调用和闭包的创建开销。以下是内联函数的一些常见应用场景：

1. 高阶函数

Kotlin 允许将函数作为参数传递给其他函数，但每次传递高阶函数时，都会涉及函数对象的创建和闭包的生成。这些操作会带来一定的性能开销。使用 inline 关键字，Kotlin 编译器可以将函数展开为具体的代码，从而避免函数调用的开销。

应用场景：

    •	需要高性能的高阶函数场景（如框架设计）。
    •	在 Android 开发中避免过多的 Lambda 函数对象创建，减少内存分配。

例子：

```kotlin
inline fun measureTime(action: () -> Unit) {
    val startTime = System.currentTimeMillis()
    action()
    val endTime = System.currentTimeMillis()
    println("Time taken: ${endTime - startTime}ms")
}

fun main() {
    measureTime {
        println("Executing some code")
    }
}
```

在这个例子中，measureTime 是一个高阶函数，接受另一个函数作为参数。通过内联展开，避免了对 action 的函数调用和闭包创建。

2. Lambda 表达式性能优化

在 Kotlin 中，使用 Lambda 表达式非常常见，比如在集合操作、事件监听中，但 Lambda 每次都会创建一个匿名类或闭包对象，从而带来性能损耗。通过使用内联函数，Lambda 的开销可以被消除。

应用场景：

• 大量使用 Lambda 表达式的场景（如集合操作、回调函数）。
• Android 中常见的点击事件、监听器等回调函数。

例子：

```kotlin
inline fun forEachElement(elements: List<Int>, action: (Int) -> Unit) {
    for (element in elements) {
        action(element)  // 不再有额外的 Lambda 对象创建开销
    }
}

fun main() {
    val numbers = listOf(1, 2, 3, 4, 5)
    forEachElement(numbers) { println(it) }
}
```

通过 inline 关键字，避免了在 forEachElement 函数内部重复创建 Lambda 对象。

3. 控制流中的非局部返回

内联函数的另一个常见应用场景是支持 非局部返回。在 Kotlin 中，Lambda 表达式通常不能使用 return 从外层函数返回，除非函数是内联的。通过内联函数，Lambda 可以实现非局部返回。

应用场景：

• 当希望从嵌套的 Lambda 中提前返回到外层函数时。
• 用于简化代码的控制流。

例子：

```kotlin
inline fun performAction(action: () -> Unit) {
    println("Before action")
    action() // 可以使用 return 直接返回
    println("After action")
}

fun main() {
    performAction {
        println("Action started")
        return  // 非局部返回，直接从 main 函数返回
    }
    println("This will never be printed")
}
```

如果 performAction 不是内联函数，那么 return 将仅返回到 Lambda，而不会返回到 main 函数中。通过内联，return 直接影响到外层调用。

4. 避免重复代码的抽象封装

内联函数可以用于封装常见的逻辑模式，同时保持性能优化。例如，许多框架中的 DSL（领域特定语言）利用内联函数实现流畅的接口，同时避免性能损耗。

应用场景：

• 封装逻辑，提供简洁的 API。
• 编写 DSL，如构建者模式、配置对象的初始化。

例子：

```kotlin
inline fun <T> buildObject(builderAction: T.() -> Unit): T where T : Any, T : Cloneable {
    val obj: T = T::class.java.newInstance()
    obj.builderAction()  // 执行构建逻辑
    return obj
}

fun main() {
    val person = buildObject<Person> {
        name = "John"
        age = 30
    }
    println(person)
}
```

在这个例子中，内联函数 buildObject 允许我们以简洁的方式构建对象，同时保持良好的性能。

5. Android 开发中的性能优化

Android 开发中，内存和性能优化非常重要。由于 Android 中常常使用匿名内部类作为回调（如按钮点击事件、网络请求的回调），每次创建匿名类对象都会占用额外的内存。通过内联函数，回调函数可以被展开，避免这些对象的创建。

应用场景：

• 在 Android 中的事件监听器、回调函数、异步任务等。
• 例如 View.setOnClickListener 等事件处理的优化。

例子：

```kotlin
inline fun setOnClickListener(view: View, crossinline onClick: () -> Unit) {
    view.setOnClickListener {
        onClick()  // 避免创建多余的匿名对象
    }
}

fun main() {
    val button = Button(context)
    setOnClickListener(button) {
        println("Button clicked")
    }
}
```

在这个例子中，内联函数 setOnClickListener 避免了额外的闭包对象创建，提升了性能。

6. 资源管理（RAII 模式）

在一些需要确保资源被正确释放的场景中，内联函数可以帮助封装资源管理逻辑，比如文件、网络连接的管理等。这可以避免开发者忘记释放资源，提高代码的健壮性。

应用场景：

• 文件操作、网络连接等需要确保正确关闭资源的场景。
• Kotlin 标准库中的 use 函数就是一个内联函数的典型例子。

例子：

```kotlin
inline fun <T: AutoCloseable, R> T.use(block: (T) -> R): R {
    try {
        return block(this)
    } finally {
        this.close()  // 确保资源被关闭
    }
}

fun main() {
    FileInputStream("file.txt").use { inputStream ->
        // 使用 inputStream 进行操作，确保在结束后关闭
    }
}
```

这个模式类似于 C++ 的 RAII（资源获取即初始化），在 Kotlin 中通过内联函数封装可以简化资源管理逻辑。

总结

Kotlin 的内联函数在以下场景中特别有用：

• 高阶函数：频繁调用时避免函数调用开销。
• Lambda 表达式优化：减少闭包和匿名类的创建。
• 非局部返回：简化控制流。
• 代码复用与封装：提供简洁高效的 API。
• 性能敏感的场景：如 Android 开发中的事件监听和资源管理。

使用内联函数不仅可以提高代码性能，还可以让代码更加简洁易读。
