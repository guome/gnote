# Retrofit

基于okhttp的网络请求库

## 基本使用

```kotlin
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET

fun main() {
    getData()
}

/**
 * 完整的 retrofit 接口请求，但是过于复杂
 */
fun getData() {
    val retrofit = Retrofit.Builder()
        .baseUrl("http://192.168.26.210:7001")
        .addConverterFactory(GsonConverterFactory.create())
        .build()

    val appService = retrofit.create(AppService::class.java)

    appService.getAppData().enqueue(object : Callback<List<App>> {
        override fun onResponse(call: Call<List<App>>, response: Response<List<App>>) {
            val list = response.body()
            if (list != null) {
                println("id | name | version")
                for (app in list) {
                    println(" ${app.id} | ${app.name} | ${app.version}")
                }
            }
        }

        override fun onFailure(call: Call<List<App>>, t: Throwable) {
            t.printStackTrace()
        }
    })
}

class App(val id: String, val name: String, val version: String)

// App 模块的接口
interface AppService {
    @GET("public/json/get_data.json")
    fun getAppData(): Call<List<App>>
}
```

运行结果

![image-20211104144630187](./Retrofit.assets/image-20211104144630187.png)

封装

Retrofit 内部已实现了，封装完全没有必要了，而且内部实现的代码一模一样，这里只是做一个分析

```kotlin
object ServiceCreator {
    private const val BASE_URL = "xxx"

    private val retrofit = Retrofit.Builder()
        .baseUrl(BASE_URL)
        .addConverterFactory(GsonConverterFactory.create())
        .build()

    fun <T> create(serviceClass: Class<T>): T = retrofit.create(serviceClass)

    inline fun <reified T> create(): T = create(T::class.java)
}


object NetWorkApi {
    private val appService = ServiceCreator.create<AppService>()

    suspend fun getAppData() = appService.getAppData().await()

      // 这里的泛型 T 有点难以理解
      // 首先 Call<T>.await() 是Call<T>的扩展函数
      // 来看看 appService.getAppData() 返回的 Call<List<App>> 然后它再调用了 await()
      // 所以这里的 T 对应的就是 List<App>，也就是请求接口的数据格式了；后面的一切都容易解释了
    suspend fun <T> Call<T>.await(): T {
          // 注意 Lambda 表达式里的代码其实是在普通线程中执行的 
        return suspendCoroutine { continuation ->
            // this.enqueue 由于是 Call 的扩展函数所有这里的this就是 Call,当然这里的 this 其实是可以省略的，只是为了代码更清楚
            this.enqueue(object : Callback<T> {
                override fun onResponse(call: Call<T>, response: Response<T>) {
                    val body = response.body()

                      // 这里倒是可以做响应拦截
                    if (body != null) {
                          // 什么时候恢复线程，这个看自己的逻辑
                        continuation.resume(body)
                    } else {
                        RuntimeException("response body is null")
                    }
                }

                override fun onFailure(call: Call<T>, t: Throwable) {
                    continuation.resumeWithException(t)
                }
            })
        }
    }
}

class App(val id: String, val name: String, val version: String)

// App 模块的接口
interface AppService {
    @GET("public/json/get_data.json")
    fun getAppData(): Call<List<App>>
}
```

## 调用

本质上就是必须在协程作用域中调用

```kotlin
fun main() {
      // 实际开发其实不能这么写
    runBlocking {
        try {
            val list = NetWorkApi.getAppData()

            for (app in list) {
                println(app.name)
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }
}
```

开发中常用的写法【安卓示例代码】

```kotlin
class MainActivity : AppCompatActivity() {
    // Android中要求网络请求必须在子线程中进行
    // 其实这里应该使用 Dispatchers.Main, 在封装接口调用的时候使用 Dispatchers.IO,那么在接口返回的时候自动切回主线程就ok了
    private val ioScope = CoroutineScope(Dispatchers.IO)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

          // 不会阻塞线程,非主线程不能修改UI
        ioScope.launch {
            try {
                // 假如这里使用 withContext(Dispatchers.Main) 封装调用接口，
                //那么就可以直接在主线程中调用接口；后面也不需要再切线程了；但是依旧需要取消
                val res = NetWorkApi.getAppData()
                withContext(Dispatchers.Main) {
                    // 切回主线程修改ui
                }
            } catch (e: Exception) {
              e.printStackTrace()
            }
        }

        // 可以调用多次
        ioScope.launch {  }

          // 直接怎么写会报错, runBlocking 是运行在当前线程下的
        runBlocking {
          // 网络请求
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        // 取消一次就可以
        ioScope.cancel()
    }
}
```

安卓中，另一种更方便的写法，利用 lifecycle 这样就不需要写取消了，但是需要引用第三方库

```groovy
implementation("androidx.lifecycle:lifecycle-runtime-ktx:2.2.0")
```

```kotlin
class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        // 属于主线程。可以修改UI
        lifecycleScope.launch{
            try {
                // 接口调用会在其他线程中，调用完毕后，自动切回主线程
                NetWorkApi.getAppData()
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }
}
```