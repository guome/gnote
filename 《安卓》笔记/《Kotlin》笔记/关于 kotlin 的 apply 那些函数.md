# 关于 kotlin 的 apply 那些函数

Kotlin 提供了一些非常实用的作用域函数（scope functions），包括 apply、also、run、let、和 with。这些函数能够在不同的上下文中简化代码逻辑，增强可读性。本文将重点介绍 apply 函数，并对它的用法和适用场景进行详细解释。

1. apply 函数概述

apply 是 Kotlin 提供的一个作用域函数，它可以用于对象的配置和初始化。apply 函数的特点是，它会返回调用它的对象本身（即 this），同时在 apply 块中可以使用 this 来访问对象的属性和方法。这使得 apply 特别适合用于链式调用和对象的初始化操作。

2. apply 的使用方式

apply 函数通常用于初始化对象或对对象进行配置，它的基本语法如下：

```kotlin
fun <T> T.apply(block: T.() -> Unit): T
```

• 接收者对象 T：是调用 apply 的对象。
• 函数块 block: T.() -> Unit：在 apply 函数块中，this 指代接收者对象，函数块中的代码可以对对象进行操作。
• 返回值 T：apply 函数返回调用它的对象本身。

3. 常见用法

（1）对象初始化

apply 通常用于创建对象时同时进行配置，这样可以避免显式多次调用对象的 setter 方法。

```kotlin
val person = Person().apply {
    name = "John"
    age = 30
    address = "New York"
}
```

在这个例子中，apply 用于对象初始化，并对对象的多个属性进行赋值。最后返回的是 person 对象本身。

（2）简化代码

通过 apply 可以将多个对同一对象的操作放入一个作用域中，减少重复引用对象的代码。

```kotlin
val person = Person()
    person.name = "John"
    person.age = 30
    person.address = "New York"
```

上面的代码可以简化为：

```kotlin
val person = Person().apply {
    name = "John"
    age = 30
    address = "New York"
}
```

这样就避免了重复写 person.，让代码更加简洁。

（3）Android 开发中的使用

在 Android 开发中，apply 函数经常用于配置 View 对象，比如设置属性、监听器等。

```kotlin
val button = Button(context).apply {
        text = "Click Me"
        textSize = 20f
        setOnClickListener {
        println("Button clicked")
    }
}
```

（4）链式调用

因为 apply 返回对象本身，它可以轻松实现链式调用。你可以在一个对象上调用多个方法，并返回最终的对象。

```kotlin
val list = mutableListOf<String>().apply {
    add("Item 1")
    add("Item 2")
    add("Item 3")
}.also {
    println("List has been initialized")
}
```

4. apply vs 其他作用域函数

apply vs also

• apply 和 also 都返回调用它的对象本身。
• 区别：在 apply 中，函数块内使用 this 访问对象；而在 also 中，函数块的参数是对象本身，通常命名为 it。

```kotlin
val person = Person().apply {
    name = "John"
    age = 30
}

val person2 = Person().also {
    it.name = "Jane"
    it.age = 25
}
```

apply vs run

• apply 返回对象本身，适用于对象初始化。
• 区别：run 返回的是函数块的执行结果，而不是对象本身。

```kotlin
val person = Person().apply {
    name = "John"
    age = 30
}

val greeting = person.run {
    "Hello, my name is $name and I'm $age years old."
}
```

在这里，run 返回的是字符串，而 apply 返回的是对象。

apply vs let

• 区别：apply 在函数块中使用 this，而 let 使用 it 作为对象的引用，let 返回的是函数块的结果，而不是对象本身。

```kotlin
val person = Person().apply {
    name = "John"
    age = 30
}

val personNameLength = person.let {
    it.name.length
}
```

5. apply 的应用场景

（1）对象的初始化和配置

apply 非常适合在创建对象时对其进行属性设置、方法调用。特别是在需要多个初始化步骤时，apply 可以让代码更加简洁和易读。

```kotlin
val car = Car().apply {
    model = "Toyota"
    year = 2020
    color = "Red"
}
```

（2）Android 中的 UI 配置

在 Android 开发中，apply 经常用于配置 View 对象，简化代码。

```kotlin
val editText = EditText(context).apply {
    hint = "Enter your name"
    textSize = 18f
    setPadding(16, 16, 16, 16)
}
```

（3）对象的批量操作

当你需要对一个对象进行多步操作时，apply 可以将所有的操作放入一个代码块中，避免重复引用该对象。

```kotlin
val person = Person().apply {
    firstName = "John"
    lastName = "Doe"
    age = 25
    println("Person initialized")
}
```

6. 总结

apply 是 Kotlin 中非常强大且常用的作用域函数，适合在以下场景中使用：

• 对象的初始化和配置。
• 避免重复引用对象。
• 在链式调用中对对象进行配置。
• Android 开发中用于配置 UI 元素。

它的特点是返回调用者对象本身，并且可以在作用域中使用 this 来引用对象，使得代码简洁、直观。
