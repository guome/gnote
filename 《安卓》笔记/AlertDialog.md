基本使用

```kotlin
val builder = AlertDialog.Builder(this)
val alert = builder.setIcon(R.mipmap.ic_launcher)
    .setTitle("系统提示：")
    .setMessage("这是一个最普通的 AlertDialog,\n带有三个按钮，分别是取消，中立和确定")
    .setNegativeButton(
        "取消"
    ) { _, which ->
        Toast.makeText(this, "你点击了取消按钮, which = $which", Toast.LENGTH_SHORT).show()
    }.setPositiveButton("确定") { _, which ->
        Toast.makeText(this, "你点击了确定按钮, which = $which", Toast.LENGTH_SHORT).show()
    }.setNeutralButton("中立") { _, which ->
        Toast.makeText(this, "你点击了确定按钮, which = $which", Toast.LENGTH_SHORT).show()
    }.setCancelable(false).create()

alert.show()
```

自定义布局

```kotlin
val builder = AlertDialog.Builder(this)
val viewCustom = layoutInflater.inflate(R.layout.view_dialog_custom, null, false)

val alert = builder
    .setCancelable(false)
    // setView(v)，自定义布局只会覆盖title和button之间的那部分
    // .setView(viewCustom)
    .setTitle("系统提示：")
    .create()
alert.show()

// 覆盖全部: 要在dialog.show()的后面，
alert.window?.setContentView(viewCustom)

viewCustom.findViewById<Button>(R.id.cancel_btn).setOnClickListener {
    Toast.makeText(this, "取消", Toast.LENGTH_SHORT).show()
    alert.dismiss()
}

viewCustom.findViewById<Button>(R.id.confirm_btn).setOnClickListener {
    Toast.makeText(this, "确认", Toast.LENGTH_SHORT).show()
    alert.dismiss()
}
```

> 注：自定义布局,不能指定大小,不能全屏