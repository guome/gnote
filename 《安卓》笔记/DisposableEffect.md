# DisposableEffect 基础使用

DisposableEffect 是 Jetpack Compose 中用于处理副作用的一个 API。它允许你在 Composable 的生命周期内执行一些一次性的操作，并在 Composable 被移除或重组（recomposition）时进行必要的清理工作。它与传统的 onCreate、onDestroy 等生命周期方法有点类似，但设计上更符合声明式 UI 框架的需求。

主要用途

    •	执行副作用操作：当 Composable 被第一次加载（或者特定的依赖项发生变化时）执行一些操作，比如启动网络请求、注册监听器等。
    •	清理资源：当 Composable 不再需要时（例如它被移除或者重新组合），执行清理工作，比如移除监听器、停止后台任务等。

使用场景

    •	当你想要在 Composable 被显示时执行某个操作，并在它被移除或重组时做清理工作。
    •	常见的副作用场景包括：
    •	注册和解除注册某些系统事件监听器。
    •	处理特定的生命周期事件，例如 View 初始化或者销毁操作。
    •	在组件销毁时释放资源。

DisposableEffect 的基本语法

```kotlin

@Composable
fun MyComposable() {
    // 使用 DisposableEffect
    DisposableEffect(Unit) {
        // 这个代码块会在 Composable 第一次进入组合时执行
        println("Composable is active")

        // `onDispose` 会在 Composable 被移除或重组时执行
        onDispose {
            println("Composable is disposed")
        }
    }

    // 其他 UI 组件
    Text(text = "Hello, DisposableEffect!")
}
```

参数说明

    •	DisposableEffect 的输入参数：用于决定 DisposableEffect 何时重新执行。当传递的依赖项发生变化时，DisposableEffect 会重新执行，同时会执行上一次的清理操作。常见的输入参数可以是 Unit（不依赖任何外部变化），也可以是某些具体的状态变量。
    •	onDispose：这个 lambda 是一个清理函数，在 Composable 离开屏幕、依赖项改变或者组件销毁时被调用。它类似于传统 Android 中 onDestroy 的作用。

实际操作流程

    1.	当 Composable 第一次进入组合时（比如首次加载到界面），DisposableEffect 的代码块会执行，通常用于初始化或注册某些资源。
    2.	当 Composable 离开组合或者 DisposableEffect 的依赖项发生变化时，onDispose 会被调用，用于清理这些资源。
    3.	如果 DisposableEffect 的依赖项发生了变化，旧的 onDispose 会先被调用清理之前的操作，然后新的 DisposableEffect 代码块会重新执行。

示例 1：注册监听器并清理

```kotlin

@Composable
fun SensorListener(sensorManager: SensorManager) {
    // 使用 DisposableEffect 注册和移除传感器监听器
    DisposableEffect(sensorManager) {
        val sensor = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER)
        val listener = object : SensorEventListener {
            override fun onSensorChanged(event: SensorEvent?) {
                // 处理传感器变化
            }

            override fun onAccuracyChanged(sensor: Sensor?, accuracy: Int) {
                // 处理精度变化
            }
        }

        // 注册传感器监听器
        sensorManager.registerListener(listener, sensor, SensorManager.SENSOR_DELAY_NORMAL)

        // 当 Composable 离开组合时，解除注册
        onDispose {
            sensorManager.unregisterListener(listener)
        }
    }
}
```

示例 2：监听 Lifecycle 事件并清理

```kotlin

@Composable
fun LifecycleObserverExample() {
    val lifecycleOwner = LocalLifecycleOwner.current

    // 使用 DisposableEffect 来监听生命周期
    DisposableEffect(lifecycleOwner) {
        val observer = LifecycleEventObserver { _, event ->
            when (event) {
                Lifecycle.Event.ON_START -> println("ON_START")
                Lifecycle.Event.ON_STOP -> println("ON_STOP")
                else -> Unit
            }
        }

        // 将观察者添加到当前的生命周期
        lifecycleOwner.lifecycle.addObserver(observer)

        // 在 Composable 被移除时，移除观察者
        onDispose {
            lifecycleOwner.lifecycle.removeObserver(observer)
        }
    }
}
```

依赖项与重组

DisposableEffect 的依赖项是控制其执行的关键。当依赖项发生变化时，DisposableEffect 会执行一次清理操作 (onDispose)，然后重新执行其主体逻辑。常见的依赖项可以是传入的 Activity、Context 或其他状态变量。

例如：

```kotlin

@Composable
fun MyComposableWithDynamicState(data: SomeData) {
    DisposableEffect(data) {
        // 使用 data 初始化某些资源
        println("Initializing with new data: $data")

        // 清理操作
        onDispose {
            println("Cleaning up for data: $data")
        }
    }
}
```

在这个例子中，当 data 发生变化时，DisposableEffect 会先调用 onDispose 进行清理，然后使用新的 data 重新执行逻辑。

总结

    •	DisposableEffect 是 Jetpack Compose 中管理副作用的工具，用于在 Composable 的生命周期内进行初始化和清理操作。
    •	适合处理像监听器注册、生命周期观察者、资源分配和释放等任务。
    •	使用 onDispose 来处理 Composable 离开屏幕或依赖项改变时的清理操作。

DisposableEffect 提供了一种声明式的方式来处理传统 UI 框架中常见的生命周期相关任务，非常适合用于 Compose 中的副作用管理。

## 在 Composable 离开屏幕、依赖项改变 这句话如何理解

在 Jetpack Compose 中，Composable 的生命周期是与其在界面上的可见性和状态管理紧密相关的。当你看到「Composable 离开屏幕」和「依赖项改变」这类描述时，它们实际上是指 Jetpack Compose 在重新组合（recomposition）期间对 UI 元素的处理方式。接下来我详细解释这些概念。

1. Composable 离开屏幕

“离开屏幕” 意味着某个 Composable 不再需要显示在界面上了，通常发生在以下场景：

    •	UI 结构变化：当用户切换到其他页面或改变导航时，某些 Composable 组件会被移除。例如，如果你在一个 List 中显示多个项目，滚动时超出屏幕范围的项目会离开屏幕，Compose 会停止显示这些组件。
    •	条件渲染：如果使用条件语句控制 Composable 的显示状态，当条件变为 false 时，Compose 会将该 Composable 从视图树中移除。例如：

```kotlin
@Composable
fun MyScreen(showContent: Boolean) {
if (showContent) {
Text("This content is visible")
}
// 如果 showContent 变为 false，"This content is visible" 的 Text 将不再显示
}
```

在这种情况下，当 showContent 变为 false 时，Text 组件会 “离开屏幕”，即被移除，所有与这个 Text 相关的资源（如事件监听器、数据流等）也应及时释放。

示例：在 Composable 离开屏幕时停止操作

```kotlin

@Composable
fun ShowOrHideComposable(showContent: Boolean) {
if (showContent) {
DisposableEffect(Unit) {
// 这个代码块在组件被加载时执行
println("Composable is active")

            onDispose {
                // 当 Composable 被移除时执行
                println("Composable is disposed")
            }
        }
        Text("Hello, World!")
    }

}
```

在这个例子中，当 showContent 为 false 时，Text 会 “离开屏幕”，这意味着与它相关的 DisposableEffect 会调用 onDispose 来执行清理操作。

2. 依赖项改变

依赖项改变指的是，DisposableEffect 中传入的参数（称为依赖项）发生变化，触发 重组（recomposition）。Compose 是声明式的框架，依赖项一旦发生变化，相关的 Composable 会重新执行，UI 会根据新的数据进行更新。在 DisposableEffect 中，这意味着每次依赖项变化时，都会调用 onDispose 清理旧资源，然后执行新的副作用逻辑。

例如，假设你有一个 DisposableEffect 依赖于某个状态变量：

```kotlin

@Composable
fun MyComposableWithState(data: String) {
DisposableEffect(data) {
println("Using data: $data")

        // 当 data 改变时，先执行清理
        onDispose {
            println("Cleaning up for data: $data")
        }
    }

}

```

如果 data 的值发生变化，DisposableEffect 会触发以下行为：

    1.	调用 onDispose：清理当前的副作用逻辑，释放与旧的 data 相关的资源。
    2.	重新执行主体逻辑：使用新的 data 重新执行 DisposableEffect，并初始化新的副作用逻辑。

示例：依赖项变化时的效果

```kotlin

@Composable
fun MyComposableWithDynamicState(data: String) {
DisposableEffect(data) {
// 执行某些操作，比如启动某个任务
println("Initializing with data: $data")

        // 当依赖项 data 发生变化或 Composable 被移除时执行清理
        onDispose {
            println("Cleaning up for data: $data")
        }
    }

}
```

在这个例子中，data 是 DisposableEffect 的依赖项。每次 data 改变时：

    •	先执行 onDispose，清理旧的副作用逻辑。
    •	然后执行新的初始化操作，使用新的 data 重新执行。

3. 组合与重组（Composition & Recomposition）

理解上面的概念，需要对 组合（composition） 和 重组（recomposition） 有一些认识：

    •	组合：当 Compose 第一次渲染 Composable 时，称之为组合。Compose 会根据 Composable 函数的代码，构建 UI 树并绘制界面。
    •	重组：当某些状态或依赖发生变化时，Compose 不会重新构建整个 UI，而是通过部分更新来重新计算和渲染受影响的 Composable。这就是重组。重组只会更新必要的部分，而不是整个页面。

因此，在 DisposableEffect 中，如果依赖项变化，Compose 通过 重组 来确保 UI 的更新，同时调用 onDispose 清理旧的资源并重新初始化新资源。

总结

    •	Composable 离开屏幕：意味着该 Composable 被移除，不再显示，通常是由于 UI 结构变化、滚动或者条件控制。
    •	依赖项改变：指的是在 DisposableEffect 中，传入的某个依赖（比如状态或参数）发生变化，Compose 重新计算并更新 UI，触发资源的清理和重新初始化。

DisposableEffect 正是为处理这些情况下的资源管理而设计的，使得开发者可以在 Compose 中轻松管理副作用，确保资源在 Composable 生命周期中的正确初始化和清理。

## DisposableEffect 页面隐藏是否也会触发

不会

## 和 LaunchedEffect 有什么不一样

DisposableEffect 和 LaunchedEffect 都是 Jetpack Compose 中用于处理副作用的 API，但它们有一些重要的区别，尤其是在触发条件、执行时机以及它们如何处理清理操作方面。

区别总结

| 特性               | DisposableEffect                                                                      | LaunchedEffect                                                                             |
| ------------------ | ------------------------------------------------------------------------------------- | ------------------------------------------------------------------------------------------ |
| 用途               | 用于处理需要清理的副作用（如注册/取消监听器、打开/关闭资源）。                        | 用于启动协程，执行一次性操作或持续操作（如网络请求、动画、定时任务等）。                   |
| 触发时机           | 第一次组合时执行，依赖项发生变化或 Composable 被移除时，触发 onDispose 来清理资源。   | 第一次组合时执行，如果依赖项发生变化会重新启动协程。                                       |
| 清理操作           | 通过 onDispose 清理资源或取消操作（例如注销监听器）。                                 | 不需要手动清理操作，Compose 自动管理协程的生命周期（协程会在 Composable 离开时自动取消）。 |
| 用于协程           | 不用于协程。                                                                          | 专门用于在 Composable 中启动协程，处理异步操作。                                           |
| 重组时的行为       | 如果依赖项变化，会先调用 onDispose 清理旧的副作用，再执行新的副作用逻辑。             | 如果依赖项变化，旧的协程会自动取消，并启动新的协程。                                       |
| 依赖于组件生命周期 | 是的，常用于与 Composable 生命周期挂钩的操作（如监听生命周期事件、注册/注销广播等）。 | 是的，协程的生命周期与 Composable 关联，当 Composable 重新组合或离开屏幕时，协程自动处理。 |
| 典型用例           | 注册和移除监听器、资源分配和清理操作等，如传感器监听器、位置监听器等。                | 启动异步任务、网络请求、动画处理、数据加载等需要异步或长时间运行的操作。                   |

1. DisposableEffect

用途与特点

    •	DisposableEffect 用于处理那些需要在 Composable 销毁或依赖项改变时清理的副作用，例如注册/取消事件监听器、资源分配与释放。
    •	你需要手动处理资源的清理工作。通过 onDispose 回调来确保在 Composable 不再显示时，能够正确地释放资源或取消事件监听器。

示例：使用 DisposableEffect 注册和注销监听器

```kotlin

@Composable
fun MySensorListener(sensorManager: SensorManager) {
    DisposableEffect(sensorManager) {
        // 注册传感器监听器
        val sensor = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER)
        val listener = object : SensorEventListener {
            override fun onSensorChanged(event: SensorEvent?) {
                // 处理传感器变化
            }

            override fun onAccuracyChanged(sensor: Sensor?, accuracy: Int) {
                // 处理精度变化
            }
        }
        sensorManager.registerListener(listener, sensor, SensorManager.SENSOR_DELAY_NORMAL)

        // 清理操作：当 Composable 销毁或依赖项变化时，取消监听器
        onDispose {
            sensorManager.unregisterListener(listener)
        }
    }
}
```

    •	在这里，DisposableEffect 用来注册和取消传感器监听器。onDispose 确保在 Composable 被移除或 sensorManager 发生变化时，能够注销监听器，防止内存泄漏。

2. LaunchedEffect

用途与特点

    •	LaunchedEffect 用于启动协程，处理异步任务，如网络请求、动画、定时器等。
    •	它会在 Composable 进入组合时启动协程，并根据依赖项的变化重新启动协程。LaunchedEffect 的协程会在 Composable 离开视图树时自动取消，因此你不需要手动处理清理工作。

示例：使用 LaunchedEffect 执行网络请求

```kotlin

@Composable
fun MyComposable(data: String) {
    LaunchedEffect(data) {
        // 这里可以进行异步操作，如网络请求
        val result = fetchDataFromNetwork(data)
        // 处理结果
        println("Result: $result")
    }
}
```

    •	LaunchedEffect 会在 Composable 第一次加载时启动，并根据 data 的变化重新执行。如果 Composable 被移除，协程会自动取消。

依赖项变化时的行为

    •	DisposableEffect: 依赖项变化时，先执行 onDispose 来清理现有的副作用（如移除监听器），然后重新执行新的副作用（如注册新的监听器）。
    •	LaunchedEffect: 依赖项变化时，Compose 会自动取消当前协程并启动新的协程来处理更新后的逻辑。

3.  具体区别分析

1.  资源清理与协程管理

    • DisposableEffect 主要用于清理资源，你必须显式地调用 onDispose 来释放资源（例如注销监听器、关闭数据库等）。
    • LaunchedEffect 不涉及资源的手动清理，它自动管理协程的生命周期。当 Composable 被移除或依赖项变化时，LaunchedEffect 会取消协程。

1.  副作用类型

        •	DisposableEffect：用于管理与外部系统交互的副作用，典型场景包括注册和移除监听器，或是处理需要清理的资源。

    使用场景：
    • 注册传感器或位置监听器等。
    • 资源分配与释放。
    • LaunchedEffect：用于启动异步任务或协程，通常用于处理如网络请求、动画或延时任务等长时间运行的任务。
    使用场景：
    • 执行网络请求。
    • 延时任务或定时器。
    • 动画或其他异步任务。

示例比较：DisposableEffect 与 LaunchedEffect 的使用

DisposableEffect 示例：注册和注销监听器

```kotlin

@Composable
fun MyComposableWithListener() {
    val context = LocalContext.current

    DisposableEffect(Unit) {
        // 注册广播接收器
        val receiver = object : BroadcastReceiver() {
            override fun onReceive(context: Context?, intent: Intent?) {
                // 处理广播
            }
        }
        context.registerReceiver(receiver, IntentFilter("MY_ACTION"))

        // 清理操作：移除广播接收器
        onDispose {
            context.unregisterReceiver(receiver)
        }
    }
}

```

LaunchedEffect 示例：执行网络请求

```kotlin

@Composable
fun MyComposableWithNetworkRequest() {
    LaunchedEffect(Unit) {
        // 启动协程执行网络请求
        val response = fetchFromNetwork()
        println("Network response: $response")
    }
}
```

在这两个示例中，DisposableEffect 用于处理需要注册和注销的系统资源，而 LaunchedEffect 用于处理异步任务，例如网络请求。

4. 总结

   • DisposableEffect：适合用于需要手动清理资源的场景，如注册/注销监听器、释放外部资源等。通过 onDispose 来进行资源的清理。
   • LaunchedEffect：适合用于异步操作，如网络请求、启动动画、协程等。它会自动管理协程的生命周期，确保在 Composable 被移除或依赖项发生变化时，协程能够正确取消，不需要手动清理。

选择哪个 API 取决于你的副作用是否需要清理（DisposableEffect），或者你是否在处理异步任务或需要协程支持（LaunchedEffect）。
