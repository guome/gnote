# 关于安卓的 api 版本和 sdk 等各种版本的关系

在 Android 开发中，API 版本、SDK 版本 和 Android 系统版本 是三个重要概念，它们紧密相关，但各自有不同的意义。了解它们之间的关系有助于更好地理解应用开发的兼容性要求以及系统功能的适配。下面我们来详细解释这些术语及其关系。

1. API 版本（API Level）

API Level 是 Android 为开发者提供的一种编程接口，它代表 Android 操作系统的功能集。每一个 Android 系统版本都对应一个唯一的 API 版本。例如：

    •	Android 14 对应 API Level 34
    •	Android 13 对应 API Level 33
    •	Android 12 对应 API Level 31
    •	Android 10 对应 API Level 29

每个 API Level 引入了特定的功能和限制。开发者在编写应用时，可以通过设置应用的 minSdkVersion、targetSdkVersion 等来控制应用的兼容性，以便在不同的 Android 版本上运行。

2. SDK 版本（SDK Version）

SDK（Software Development Kit）版本 代表 Android 开发工具集，包含了构建 Android 应用所需的库、工具和文档。每个 SDK 版本与 Android 系统版本相对应。

SDK 相关的配置：

    •	compileSdkVersion: 这个配置指定了编译应用程序时使用的 API Level，也就是应用程序编译时能使用的 API 功能集。例如，如果 compileSdkVersion 设置为 34，表示应用在编译时可以使用 Android 14 引入的 API，但并不意味着应用无法在更低的 Android 版本上运行。
    •	minSdkVersion: 指定应用支持的最低 API Level。即使 compileSdkVersion 高于 minSdkVersion，应用依然能够在较低的 API 版本上运行，但开发者必须在代码中处理低版本设备上不支持的新 API。
    •	targetSdkVersion: 这个配置决定了应用以哪个 API Level 为目标，也就是说，应用将针对哪个 Android 版本进行优化。设置了 targetSdkVersion 后，系统将为应用启用新的行为规范。

3. Android 系统版本（Android Version）

Android 系统版本 指的是用户设备上运行的操作系统版本。Android 系统版本每隔一段时间发布一次新版本，每个新版本引入新的功能、用户体验改进以及安全更新等。

Android 系统版本的命名：

    •	Android 14（API Level 34）
    •	Android 13（API Level 33）
    •	Android 12（API Level 31）
    •	Android 11（API Level 30）
    •	Android 10（API Level 29）

每个 Android 系统版本与一个 API Level 直接对应，这些版本之间有向后兼容的关系。

4. 它们之间的关系

在 Android 开发中，API 版本、SDK 版本和 Android 系统版本密切相关。它们通过 minSdkVersion、targetSdkVersion 和 compileSdkVersion 三个配置参数共同定义了应用程序的行为和兼容性。

    •	compileSdkVersion：决定应用编译时所使用的 API 版本，但不影响应用的运行时表现。你可以使用较高版本的 API，但如果目标设备的 Android 系统版本较低（低于 minSdkVersion），应用可能会崩溃。因此你需要通过 @RequiresApi 注解或版本检查来确保兼容性。
    •	minSdkVersion：表示应用能运行的最低 Android 系统版本。如果设备的系统版本低于 minSdkVersion，该应用将无法安装或运行。
    •	targetSdkVersion：表示应用程序运行时应该针对哪个 Android 版本进行优化。如果应用在更高的系统版本上运行，系统可能会应用一些向后兼容的行为，但如果 targetSdkVersion 与系统版本匹配，系统将应用新的行为规范。

5. 示例理解

假设你在 build.gradle 中有如下配置：

```kotlin
android {
compileSdkVersion 34 // 编译时使用的 Android 14 (API Level 34)

    defaultConfig {
        minSdkVersion 21   // 最低支持 Android 5.0 (API Level 21)
        targetSdkVersion 33 // 针对 Android 13 (API Level 33) 优化
    }

}
```

    •	compileSdkVersion 34：应用程序编译时使用 Android 14 的 API。你可以在代码中使用 Android 14 的 API 特性，但是需要检查兼容性，以确保它在较低版本上不崩溃。
    •	minSdkVersion 21：应用最早支持的 Android 版本是 Android 5.0（Lollipop，API Level 21）。运行在 API 21 及以上版本的设备上。
    •	targetSdkVersion 33：应用程序会针对 Android 13（API Level 33）进行优化。如果设备的系统版本高于 33，系统将使用较新版本的行为特性，但仍保留一定的向后兼容行为。

6. API 版本和 Android 功能的演变

每个 API 版本通常会引入新的功能和行为，而旧版本设备则不支持这些特性。为了在不影响旧设备运行的情况下使用新特性，开发者需要做版本检查，比如：

```kotlin
if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
// 使用 Android 10 及以上版本的功能
} else {
// 使用较低版本的替代方案
}
```

这种方式确保应用可以利用较新的系统特性，而不会在低版本设备上崩溃。

7. 升级 compileSdkVersion 和 targetSdkVersion 的重要性

当 Google 发布新的 Android 系统版本时，开发者通常会被鼓励将 compileSdkVersion 和 targetSdkVersion 升级到最新版本。这样可以：

    •	使应用能够使用最新的系统 API 和功能。
    •	保证应用在最新的 Android 系统上能够正常运行并符合新的系统行为规范。

总结

    •	API Level：表示 Android 系统功能集，每个系统版本对应一个唯一的 API Level。
    •	SDK 版本：表示开发工具集，用于编译和构建应用。
    •	Android 系统版本：用户设备上的操作系统版本，对应一个特定的 API Level。

compileSdkVersion 决定了应用可以使用的 API，而 minSdkVersion 和 targetSdkVersion 控制了应用的兼容性和运行时行为。
