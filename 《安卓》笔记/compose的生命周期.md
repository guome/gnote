# compose 的生命周期

Jetpack Compose 中的生命周期与传统的 Android UI 生命周期有所不同，因为它基于一种声明式的编程范式。尽管如此，Jetpack Compose 仍然依赖于 LifecycleOwner（通常是 Activity 或 Fragment）来管理 UI 的生命周期。为了理解 Compose 中的生命周期，重要的是理解它如何结合 Activity 或 Fragment 的生命周期以及如何使用 Compose 特定的生命周期控制函数来管理资源和状态。

## 1. Jetpack Compose 与 Activity/Fragment 的生命周期

在 Compose 中，UI 是基于声明的，并且每次状态发生变化时都会重新组合（recomposition）。通常情况下，Jetpack Compose 的生命周期与 Activity 或 Fragment 的生命周期结合在一起，Composable 函数的生命周期跟随宿主组件的生命周期变化。

### Activity 或 Fragment 的典型生命周期流程：

• onCreate: 初始化 UI 以及初始状态，调用 `setContent {}` 来设置 Composable UI。
• onStart: UI 变为可见，可能会触发第一次重组。
• onResume: UI 与用户交互，可能会频繁触发重组。
• onPause: UI 不再与用户交互，可能需要停止动画或其他耗电操作。
• onStop: UI 不可见，通常可以释放不再需要的资源。
• onDestroy: 销毁 UI 及相关资源。

```kotlin
class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            MyComposableScreen()
        }
    }
}
```

## 2. Jetpack Compose 的重组（Recomposition）

在 Compose 中，每当可观察的状态发生变化时，会触发重组（Recomposition），这意味着 Compose 会重新执行与这些状态相关的 Composable 函数。以下是 Compose 重组与生命周期的几个关键点：

• 初次组合：当 Composable 函数第一次进入 Composition 时（即，setContent 调用后），会进行初次组合。此时初始化所有 Composable UI。
• 重组：当 Composable 的状态发生变化时，Compose 会触发重组，只会重新计算那些状态发生变化的部分，不会刷新整个 UI。
• 移除（Decomposition）：当某个 Composable 不再需要（例如，它被 UI 树移除），Compose 会移除它并清理与之相关的资源。

## 3. Jetpack Compose 中的生命周期相关 API

为了更好地管理 Compose 中的生命周期，Jetpack Compose 提供了一些工具和 API 来控制状态和资源的生命周期。

### DisposableEffect

DisposableEffect 是一个用于在生命周期中进行副作用管理的 API。它允许你在 Composable 进入或离开 Composition 时执行一些资源初始化或清理操作。DisposableEffect 通常用于在 Composable 生命周期内管理资源（如传感器、位置服务等），并在 Composable 移出 Composition 时释放资源。

```kotlin
@Composable
fun MyComposableScreen() {
    DisposableEffect(Unit) {
        // 开始某个操作，比如监听事件或启动动画
        println("Composable entered the composition")

        onDispose {
            // 当这个 Composable 离开 Composition 时执行清理操作
            println("Composable left the composition")
        }
    }

    // 其他 UI 内容
    Text("Hello, World!")
}
```

• onDispose 是在 Composable 移出 Composition 时调用的，用于清理资源。

### LaunchedEffect

LaunchedEffect 是 Compose 中用于在 Composable 中启动协程的工具。它会在 Composable 进入 Composition 时启动，并确保该操作在 Composable 的整个生命周期中保持活跃。如果任何传递给 LaunchedEffect 的键（key1, key2）发生变化，LaunchedEffect 会重新启动，并取消之前的操作。

```kotlin
@Composable
fun MyScreen() {
    LaunchedEffect(Unit) {
        // 协程中的逻辑将在 Composable 进入 Composition 时启动
        // 例如，发起网络请求
    }

    // 其他 UI 内容
    Text("Hello, Compose!")
}
```

• LaunchedEffect 可以与 Coroutine 协作，用于处理异步操作，并且会随着 Composable 生命周期变化而自动取消和启动任务。

### remember 和 rememberSaveable

• remember：用于在重组期间保持状态。你可以使用 remember 将某个值存储在内存中，确保在重组时状态不会丢失。
• rememberSaveable：与 remember 类似，但它不仅在重组中保持状态，还可以在配置更改（如设备旋转）时保存状态。

```kotlin
@Composable
fun Counter() {
    var count by remember { mutableStateOf(0) } // 状态保持在重组期间

    Button(onClick = { count++ }) {
        Text("Count: $count")
    }
}
```

## 4. 与传统 Android 生命周期的整合

Jetpack Compose 中的 UI 是声明式的，状态驱动界面更新，因此与传统 Activity 和 Fragment 的生命周期有着很好的兼容性。你可以结合 Android 的生命周期方法和 Compose 提供的 API 来更精细地控制 UI 的生命周期行为。

## 小结

• 重组（Recomposition） 是 Compose 中生命周期的核心概念，它是随着状态变化重新计算 UI 的过程。
• DisposableEffect 和 LaunchedEffect 可以用于处理需要响应 Compose 生命周期的副作用和异步任务。
• 状态管理 是通过 remember 和 rememberSaveable 这样的工具来完成的。

这些工具和概念帮助管理 Composable 的生命周期，使其能够与 Android 传统的 Activity 和 Fragment 生命周期有机结合。

# DisposableEffect 详解

DisposableEffect 是 Jetpack Compose 中用于处理副作用（Side Effects）并管理其生命周期的 API。它允许在 Composable 函数进入或离开 Composition 时执行特定的代码，适用于需要管理资源、注册/取消监听器、启动/停止操作等场景。

## DisposableEffect 的作用

### DisposableEffect 主要用于以下场景：

• 当 Composable 进入 Composition 时执行某些副作用操作（例如，启动某些服务、注册监听器）。
• 当 Composable 离开 Composition 时执行清理操作（例如，注销监听器、释放资源）。

这是一个常用的结构来管理生命周期和资源，在 Compose 中与传统的 onStart、onStop 或 onDestroy 类似。

## DisposableEffect 的基本语法

```kotlin
@Composable
fun MyComposable() {
    DisposableEffect(Unit) {
        // 在 Composable 进入 Composition 时执行的代码
        println("Composable entered the composition")

        onDispose {
            // 在 Composable 离开 Composition 时执行的清理代码
            println("Composable left the composition")
        }
    }

    // 其他 UI 代码
    Text("Hello, World!")
}
```

## DisposableEffect 的参数

DisposableEffect 接收一个或多个键（keys）作为参数。当这些键发生变化时，Compose 会重新启动 DisposableEffect，并自动调用 onDispose 来清理之前的副作用。

• Keys：当 DisposableEffect 中依赖的 key 发生变化时，Compose 会先触发之前的 onDispose，然后重新运行新的副作用逻辑。

例如：

```kotlin
@Composable
fun MyComposable(name: String) {
    DisposableEffect(name) {
        println("Starting effect for $name")

        onDispose {
            println("Cleaning up effect for $name")
        }
    }
}
```

在这个例子中，每当 name 参数发生变化时，旧的 DisposableEffect 会清理（调用 onDispose），然后重新创建一个新的副作用。

## 详细解释 DisposableEffect 的工作流程

1. 初次组合（Entering Composition）：当 DisposableEffect 第一次进入 Composition 时，会执行 lambda 表达式的前半部分（即没有 onDispose 的部分）。这个部分用于初始化副作用，执行诸如监听事件、启动动画、订阅服务等操作。
2. onDispose 调用（Leaving Composition）：如果 Composable 被移除（例如，由于 UI 结构的变化），onDispose 函数会被调用，用于执行清理操作。onDispose 的主要目的是释放或取消在副作用中启动的操作，防止资源泄漏或不必要的操作继续执行。
3. 键值变化（Key Changes）：如果 DisposableEffect 的键（keys）发生变化，那么之前的 onDispose 会被触发清理操作，然后 DisposableEffect 会重新执行，这样可以确保新的一组副作用操作基于新的输入运行。

## 典型使用场景

DisposableEffect 主要用于那些需要显式管理生命周期的场景，常见的例子包括：

• 注册/注销监听器（如位置服务、传感器）
• 启动/停止长时间运行的后台任务
• 管理系统资源（如相机、蓝牙、网络连接）
• UI 动画的启动和停止

### 示例 1：监听系统广播

假设我们需要监听系统的广播事件，当 Composable 进入或离开 Composition 时分别注册和注销广播接收器。

```kotlin
@Composable
fun BroadcastListener(context: Context) {
    DisposableEffect(Unit) {
        val intentFilter = IntentFilter(Intent.ACTION_BATTERY_CHANGED)
        val receiver = object : BroadcastReceiver() {
            override fun onReceive(context: Context?, intent: Intent?) {
                // 处理电池状态变化
                println("Battery level changed")
            }
        }

        // 注册广播接收器
        context.registerReceiver(receiver, intentFilter)

        // 离开 Composition 时注销广播接收器
        onDispose {
            context.unregisterReceiver(receiver)
        }
    }

    // 其他 UI 代码
    Text("Listening to battery changes...")
}
```

在这个例子中，DisposableEffect 被用于注册和注销广播接收器。进入 Composition 时，我们注册广播接收器；当 Composable 被移除或重新组合时，onDispose 函数会被调用，以保证广播接收器被正确注销，避免内存泄漏。

### 示例 2：传感器管理

假设我们有一个 Composable 需要使用设备的加速度传感器，并且在离开 Composition 时停止传感器的读取。

```kotlin
@Composable
fun AccelerometerReader(context: Context) {
    DisposableEffect(Unit) {
        val sensorManager = context.getSystemService(Context.SENSOR_SERVICE) as SensorManager
        val accelerometer = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER)
        val sensorListener = object : SensorEventListener {
            override fun onAccuracyChanged(sensor: Sensor?, accuracy: Int) {
                // 精度变化时执行
            }

            override fun onSensorChanged(event: SensorEvent?) {
                // 传感器数据变化时执行
                event?.let {
                    println("Accelerometer X: ${it.values[0]}, Y: ${it.values[1]}, Z: ${it.values[2]}")
                }
            }
        }

        // 注册传感器监听器
        sensorManager.registerListener(sensorListener, accelerometer, SensorManager.SENSOR_DELAY_NORMAL)

        // 在 Composable 离开 Composition 时注销传感器监听器
        onDispose {
            sensorManager.unregisterListener(sensorListener)
        }
    }

    Text("Reading accelerometer data...")
}
```

在这个例子中，DisposableEffect 用于管理加速度传感器的监听。在进入 Composition 时，传感器监听器被注册，而在离开 Composition 时通过 onDispose 注销监听器。

## LaunchedEffect 与 DisposableEffect 的区别

• LaunchedEffect：主要用于启动协程并处理异步任务，当依赖的 key 变化时会重启协程。如果不涉及资源清理，可以使用 LaunchedEffect。
• DisposableEffect：专门用于处理资源初始化和清理操作。当涉及到需要在 Composable 生命周期内管理外部资源时，如监听器、服务等，应使用 DisposableEffect。

## 总结

DisposableEffect 是 Jetpack Compose 中用于处理副作用的工具，尤其适用于需要管理生命周期的资源。当 Composable 进入 Composition 时，它允许你启动一些副作用操作，并在离开时通过 onDispose 进行清理，确保资源得到正确管理，避免内存泄漏。
