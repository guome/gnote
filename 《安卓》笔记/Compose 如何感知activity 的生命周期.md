在 Jetpack Compose 中，虽然主要使用的是声明式编程模型，但仍然可以感知和处理 Activity 的生命周期。为了感知和响应 Activity 的生命周期事件，你可以使用 LifecycleOwner 和 Lifecycle 来监听生命周期的变化。结合这些 API，可以在 Jetpack Compose 中实现类似于传统视图中的生命周期管理。

以下是一些在 Compose 中感知和处理 Activity 生命周期的常见方法。

方法一：使用 LifecycleObserver

可以通过 LifecycleObserver 来监听 Activity 的生命周期事件，并在 Compose 代码中处理这些事件。

1. 实现 LifecycleEventObserver

你可以通过 LifecycleEventObserver 来监听生命周期事件，然后在 Compose 中注册监听器。

```kotlin

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.compose.runtime._
import androidx.lifecycle._
import androidx.compose.material3.\*
import androidx.compose.ui.tooling.preview.Preview

class MainActivity : ComponentActivity() {
override fun onCreate(savedInstanceState: Bundle?) {
super.onCreate(savedInstanceState)
setContent {
MyComposableWithLifecycle()
}
}
}

@Composable
fun MyComposableWithLifecycle() {
// 获取当前的 LifecycleOwner（通常是 Activity 或 Fragment）
val lifecycleOwner = LocalLifecycleOwner.current

    // 创建一个可变的状态来保存当前的生命周期事件
    val lifecycleEventState = remember { mutableStateOf<Lifecycle.Event?>(null) }

    // 创建一个 LifecycleEventObserver
    DisposableEffect(lifecycleOwner) {
        val observer = LifecycleEventObserver { _, event ->
            lifecycleEventState.value = event
            when (event) {
                Lifecycle.Event.ON_CREATE -> {
                    // Handle onCreate event
                }
                Lifecycle.Event.ON_START -> {
                    // Handle onStart event
                }
                Lifecycle.Event.ON_RESUME -> {
                    // Handle onResume event
                }
                Lifecycle.Event.ON_PAUSE -> {
                    // Handle onPause event
                }
                Lifecycle.Event.ON_STOP -> {
                    // Handle onStop event
                }
                Lifecycle.Event.ON_DESTROY -> {
                    // Handle onDestroy event
                }
                else -> Unit
            }
        }

        // 将 observer 添加到当前 Lifecycle
        lifecycleOwner.lifecycle.addObserver(observer)

        // 在 Composable 被销毁时移除 observer
        onDispose {
            lifecycleOwner.lifecycle.removeObserver(observer)
        }
    }

    // 显示当前的生命周期事件（例如可以在 UI 中展示）
    Text(text = "Current lifecycle event: ${lifecycleEventState.value}")

}

@Preview
@Composable
fun PreviewMyComposableWithLifecycle() {
MyComposableWithLifecycle()
}
```

代码解析：

    •	LifecycleOwner：通过 LocalLifecycleOwner 获取当前的生命周期拥有者，通常是 Activity 或 Fragment。
    •	LifecycleEventObserver：通过 LifecycleEventObserver 监听生命周期变化，比如 ON_CREATE、ON_START、ON_RESUME 等。
    •	DisposableEffect：确保在组件离开屏幕时进行清理操作，比如移除生命周期观察者。

方法二：使用 rememberCoroutineScope 结合 LaunchedEffect

你也可以使用 rememberCoroutineScope 和 LaunchedEffect 来监听生命周期的特定事件。例如，可以结合 Lifecycle 和协程来感知 Activity 的生命周期。

示例：在 ON_START 和 ON_STOP 之间执行某个逻辑

```kotlin

@Composable
fun MyComposableWithLifecycleAwareLogic() {
val lifecycleOwner = LocalLifecycleOwner.current
val lifecycle = lifecycleOwner.lifecycle
val coroutineScope = rememberCoroutineScope()

    LaunchedEffect(lifecycle) {
        // 启动协程，感知生命周期事件
        lifecycle.repeatOnLifecycle(Lifecycle.State.STARTED) {
            // 这个代码块只在生命周期处于 STARTED 或更高状态时运行
            // 当生命周期进入 STOPPED 时会挂起
            coroutineScope.launch {
                // 在 STARTED 和 STOPPED 之间执行的逻辑
                println("Lifecycle in STARTED state")
            }
        }
    }

    Text(text = "Lifecycle-aware content")

}
```

代码解析：

    •	LaunchedEffect：监听 lifecycle 变化，启动协程来处理生命周期事件。
    •	repeatOnLifecycle：该 API 会确保只有在特定的生命周期状态（例如 STARTED）时，才会执行某段逻辑。当 Activity 进入 STOPPED 状态时，协程会自动挂起，直到再次进入 STARTED。

方法三：使用 DisposableEffect

DisposableEffect 是一种处理副作用的方法，它可以用于监听生命周期事件并在不再需要时进行清理。例如，当 Activity 销毁时停止某些逻辑。

```kotlin

@Composable
fun MyComposableWithDisposableEffect() {
val lifecycleOwner = LocalLifecycleOwner.current

    DisposableEffect(lifecycleOwner) {
        // 在 Composable 被加载时执行
        println("Composable is active")

        // 在 Composable 被销毁时执行
        onDispose {
            println("Composable is disposed")
        }
    }

    Text(text = "Composable with DisposableEffect")

}
```

代码解析：

    •	DisposableEffect：用于执行副作用和清理操作，例如当 Composable 进入屏幕或离开屏幕时执行一些逻辑。

方法四：使用 SideEffect

SideEffect 通常用于与不可组合的 UI 组件（如系统组件或外部库）进行交互。它可以确保在每次组合完成后执行代码。

```kotlin
@Composable
fun MyComposableWithSideEffect() {
SideEffect {
println("This is a side effect that runs after every recomposition.")
}

    Text(text = "Composable with SideEffect")

}

```

总结

在 Jetpack Compose 中感知 Activity 的生命周期有多种方法：

    •	使用 LifecycleObserver 或 LifecycleEventObserver 监听具体的生命周期事件。
    •	使用 rememberCoroutineScope 和 repeatOnLifecycle 来启动和暂停与生命周期状态相关的协程任务。
    •	使用 DisposableEffect 和 SideEffect 处理副作用和资源清理。

这些方法结合了 Compose 声明式的特性，使得你可以轻松地处理和感知生命周期事件，同时保持代码简洁高效。
