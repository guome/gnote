# Row 的滚动以及，滚动到特定位置

在 Compose 中，如果要让 Row 水平滚动，正确的做法是使用 Modifier.horizontalScroll，结合 rememberScrollState 来管理滚动状态。

正确的实现方式

以下是使用 Modifier.horizontalScroll 实现 Row 水平滚动的正确示例：

```kotlin
import androidx.compose.foundation.horizontalScroll
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.text.BasicText
import androidx.compose.material.Button
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import kotlinx.coroutines.launch
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.Column

@Composable
fun ScrollableRow() {
    // 创建 ScrollState 用于控制滚动状态
    val scrollState = rememberScrollState()
    // 创建协程作用域，用于异步滚动
    val coroutineScope = rememberCoroutineScope()

    // 布局
    Column {
        // 按钮用于触发滚动到特定位置
        Button(onClick = {
            coroutineScope.launch {
                // 滚动到 300px 位置
                scrollState.animateScrollTo(300)
            }
        }) {
            Text("Scroll to 300px")
        }

        // 使用 Modifier.horizontalScroll 来实现水平滚动
        Row(
            modifier = Modifier
                .horizontalScroll(scrollState) // 启用水平滚动
                .padding(16.dp) // 添加内边距
        ) {
            // Row 中的每个项目
            repeat(20) {
                BasicText(text = "Item $it", modifier = Modifier.padding(8.dp))
            }
        }
    }
}
```

关键点：

- Modifier.horizontalScroll()：这是为水平滚动设置的修饰符。通过它，Row 组件就能够在水平轴上滚动。
- rememberScrollState()：这个状态对象管理滚动的当前位置。它允许你手动控制滚动行为。
- scrollTo() 和 animateScrollTo()：这两个方法可以用来控制滚动到指定位置，animateScrollTo() 会带有动画效果。

通过这种方式，你可以轻松实现 Row 组件的水平滚动。

## 点击滚动到特定位置

如果 Row 中的每一项的大小不确定，并且你希望在点击某一项后滚动到该项的位置，关键是要在布局中动态计算每个项目的位置。Jetpack Compose 提供了 Modifier.onGloballyPositioned 来帮助我们获取每个项目的布局位置信息。

实现步骤：

- 动态测量项目的位置：使用 Modifier.onGloballyPositioned 获取每个项目的绝对位置。
- 滚动到指定位置：在用户点击项目时，通过 scrollState 滚动到对应的目标位置。

完整示例代码

```kotlin
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.text.BasicText
import androidx.compose.foundation.horizontalScroll
import androidx.compose.material.Button
import androidx.compose.material.Text
import androidx.compose.runtime.*
import androidx.compose.ui.Modifier
import androidx.compose.ui.layout.onGloballyPositioned
import androidx.compose.ui.unit.IntOffset
import androidx.compose.ui.unit.dp
import kotlinx.coroutines.launch
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.Column
import androidx.compose.ui.geometry.Offset
import androidx.compose.ui.layout.boundsInParent

@Composable
fun ScrollableRowWithDynamicItems() {
    val scrollState = rememberScrollState()  // 用于控制滚动状态
    val coroutineScope = rememberCoroutineScope()

    // 用于保存每个项目的位置信息
    val itemPositions = remember { mutableStateMapOf<Int, IntOffset>() }

    Column {
        // 每个按钮点击后滚动到对应项
        Button(onClick = {
            coroutineScope.launch {
                // 滚动到第 10 个项目的位置
                itemPositions[9]?.let { offset ->
                    scrollState.animateScrollTo(offset.x)
                }
            }
        }) {
            Text("Scroll to Item 10")
        }

        // 实现水平滚动
        Row(
            modifier = Modifier
                .horizontalScroll(scrollState)
                .padding(16.dp)
        ) {
            // 渲染多个项目
            repeat(20) { index ->
                BasicText(
                    text = "Item $index",
                    modifier = Modifier
                        .padding(8.dp)
                        .onGloballyPositioned { layoutCoordinates ->
                            // 获取每个项目在父容器中的位置
                            val bounds = layoutCoordinates.boundsInParent()
                            itemPositions[index] = IntOffset(bounds.left.toInt(), bounds.top.toInt())
                        }
                )
            }
        }
    }
}
```

代码说明：

- itemPositions：使用 mutableStateMapOf 来存储每个项目的位置。键是项目的索引，值是项目的 IntOffset，表示该项目在 Row 容器中的位置。
- onGloballyPositioned：这是 Jetpack Compose 提供的一个修饰符，允许你在布局完成后获取组件的全局位置。通过 boundsInParent()，我们获取项目在父容器中的位置，存储到 itemPositions 中。
- scrollState.animateScrollTo()：当点击按钮时，根据项目的存储位置，滚动到目标项目的水平位置。

动态项目大小的处理：

因为每个项目的大小不确定，我们通过 onGloballyPositioned 动态地获取每个项目的位置。这样可以确保无论项目的大小如何变化，点击时都可以滚动到相应的位置。

这种方法非常灵活，适用于处理布局中每个项目大小不一致的情况。

# 点击项目，使其(项目)滚动到 row 的中间位置

```kotlin
@Composable
fun ItemType(modifier: Modifier = Modifier) {
    val scrollState = rememberScrollState()
    // 创建协程作用域，用于异步调用滚动方法
    val coroutineScope = rememberCoroutineScope()
    val itemPositions = remember { mutableStateMapOf<Int, LayoutCoordinates>() }
    var rowSize by remember { mutableStateOf(IntSize.Zero) } // 保存 Row 的尺寸
    val currentIndex = remember { mutableIntStateOf(-1) }

    // 外层需要套一层 Box
    Box(
        modifier = Modifier
            .fillMaxWidth()
            .onGloballyPositioned { coordinates ->
                // 获取 Row 的可见尺寸
                // 如果是放在 Row 组件中使用的话，就会得到内容宽度
                rowSize = coordinates.size
            }
    ) {
        Row(
            modifier = modifier.horizontalScroll(scrollState)
        ) {
            repeat(10) { index ->
                Text(
                    "推荐 $index",
                    color = if (index == currentIndex.intValue) {
                        Color.Red
                    } else {
                        Color.Gray
                    },
                    modifier = Modifier
                        // .background(Color.Cyan)
                        .padding(end = 10.dp)
                        .clickable {
                            currentIndex.intValue = index
                            coroutineScope.launch {
                                itemPositions[index]?.let { itemCoordinates ->
                                    val itemBounds = itemCoordinates.boundsInParent()
                                    val itemWidth = itemBounds.width
                                    val itemCenter = itemBounds.left + itemWidth / 2

                                    // 计算需要滚动的目标位置（让项目居中）
                                    val scrollTo = itemCenter - (rowSize.width / 2)

                                    // 触发滚动
                                    scrollState.animateScrollTo(scrollTo.toInt())
                                }
                            }
                        }
                        .onGloballyPositioned { coordinates ->
                            itemPositions[index] = coordinates
                        })
            }
        }
    }

}
```
