# 手势处理

Jetpack Compose 中的手势处理通过声明式的 Modifier 以及一系列预定义的手势检测函数来实现。你可以轻松地为 UI 元素添加响应用户输入的手势，比如点击、滑动、缩放等。

1. 基础手势处理

Jetpack Compose 提供了用于处理常见手势的修饰符（modifier），如点击、拖动等。常用的手势处理包括：

• clickable: 处理点击事件。
• pointerInput: 捕捉和处理更复杂的手势，比如滑动、缩放等。

示例：clickable 和 combinedClickable

```kotlin
Box(
    modifier = Modifier
        .size(100.dp)
        .background(Color.Blue)
        .clickable {
            // 点击事件处理
        }
)
```

对于更复杂的点击，比如长按和双击，使用 combinedClickable：

```kotlin
Box(
    modifier = Modifier
        .size(100.dp)
        .background(Color.Blue)
        .combinedClickable(
        onClick = { /* 单击处理 */ },
        onLongClick = { /*  长按处理 */  }
    )
)
```

2. 拖动 (Drag)

拖动手势允许用户通过触摸屏幕移动元素。使用 pointerInput 和 detectDragGestures 可以轻松实现拖动功能。

示例：检测拖动手势

```kotlin
val offsetX = remember { mutableStateOf(0f) }
val offsetY = remember { mutableStateOf(0f) }

Box(
    modifier = Modifier
        .offset { IntOffset(offsetX.value.roundToInt(), offsetY.value.roundToInt()) }
        .size(100.dp)
        .background(Color.Green)
        .pointerInput(Unit) {
            detectDragGestures { change, dragAmount ->
                change.consume()  // 表示消费事件
                offsetX.value += dragAmount.x
                offsetY.value += dragAmount.y
            }
        }
)
```

3. 滚动 (Scroll)

Compose 支持垂直或水平滚动，通过 scrollable 修饰符来实现。这可以与手势结合使用，允许用户通过滑动滚动视图内容。

示例：使用 scrollable 实现水平滚动

```kotlin
val scrollState = rememberScrollState()

Row(
    modifier = Modifier
        .scrollable(scrollState, Orientation.Horizontal)
) {
    repeat(10) {
        Box(
            modifier = Modifier
                .size(100.dp)
                .background(Color.Red)
                .padding(10.dp)
        )
    }
}
```

4. 惯性滑动 (Fling)

惯性滑动是手势系统中比较自然的一部分，用户完成拖动后，元素可以继续按照惯性滑动。Compose 使用 Animatable 和 animateDecay 来实现这一效果。

示例：实现惯性滑动

```kotlin
val offsetX = remember { Animatable(0f) }

Box(
    modifier = Modifier
        .offset { IntOffset(offsetX.value.roundToInt(), 0) }
        .size(100.dp)
        .background(Color.Blue)
        .pointerInput(Unit) {
            detectDragGestures(
                onDragEnd = {
                    launch {
                        offsetX.animateDecay(1000f)  // 惯性滑动效果
                    }
                }
            ) { change, dragAmount ->
                change.consume()
                offsetX.snapTo(offsetX.value + dragAmount.x)
            }
        }
)
```

5. 缩放和旋转 (Pinch and Rotation)

Compose 支持通过多点触控来实现缩放和旋转手势。可以使用 detectTransformGestures 来处理缩放、旋转以及拖动的组合手势。

示例：缩放和旋转手势处理

```kotlin
var scale by remember { mutableStateOf(1f) }
var rotationState by remember { mutableStateOf(1f) }

Box(
    modifier = Modifier
        .graphicsLayer(
            scaleX = scale,
            scaleY = scale,
            rotationZ = rotationState
        )
        .size(200.dp)
        .background(Color.Magenta)
        .pointerInput(Unit) {
            detectTransformGestures { _, pan, zoom, rotation ->
                scale *= zoom  // 处理缩放
                rotationState += rotation  // 处理旋转
            }
        }
)
```

6. 手势的优先级与冲突

在处理多个手势时，有时可能会发生手势冲突。Jetpack Compose 提供了 awaitFirstDown、awaitTouchSlopOrCancellation 等低级别 API 来处理手势冲突。例如，你可以控制某个手势在何时被消费，或者如何与其他手势组合使用。

示例：处理滑动和点击冲突

```kotlin
Box(
    modifier = Modifier
        .size(200.dp)
        .background(Color.Cyan)
        .pointerInput(Unit) {
            detectTapGestures(onTap = { /* 点击事件处理 */ })
        }
        .pointerInput(Unit) {
            detectDragGestures { change, dragAmount ->
                // 拖动事件处理
            }
        }
)
```

总结

Jetpack Compose 提供了灵活且强大的手势处理系统，从简单的点击手势到复杂的多点触控，都可以通过内置的修饰符和手势检测函数来实现。开发者可以利用这些工具轻松创建响应用户交互的动态 UI。

# 手势的不同的抽象级别

在 Jetpack Compose 中，手势操作的处理是通过不同的抽象级别来实现的。这些抽象级别允许开发者在不同的复杂度和控制需求下，灵活地处理用户交互。我们可以将这些抽象级别大致分为以下三类，从简单到复杂依次递进：

• 高层级抽象 (High-level Abstraction)：简单的手势处理
• 中层级抽象 (Mid-level Abstraction)：自定义手势处理
• 低层级抽象 (Low-level Abstraction)：完全控制的手势处理

1. 高层级抽象 (High-level Abstraction)

高层级抽象是指 Jetpack Compose 提供的预定义手势修饰符，这些修饰符封装了常见的手势交互，如点击、拖动、滚动等。这些修饰符大多是直接可用的，适合处理简单、常见的手势需求。开发者不需要深入理解手势的细节，只需绑定相应的修饰符即可。

常见的高层级手势修饰符：

• Modifier.clickable()：处理点击事件
• Modifier.swipeable()：处理滑动手势
• Modifier.toggleable()：用于可切换的 UI 元素（类似于复选框）
• Modifier.scrollable()：处理滚动手势
• Modifier.draggable()：处理拖动手势

示例：使用 Modifier.clickable

```kotlin
Box(
    modifier = Modifier
        .size(100.dp)
        .clickable {
            // 处理点击事件
        }
)
```

这些高层级修饰符封装了事件处理和状态管理，适用于简单的手势处理需求，开发者无需深入到手势事件的细节即可实现功能。

2. 中层级抽象 (Mid-level Abstraction)

中层级抽象提供了更多灵活性，允许开发者处理更复杂的手势行为。Jetpack Compose 提供了更细粒度的控制，允许开发者处理多个手势或自定义手势响应。通过 Modifier.pointerInput()，开发者可以在手势事件上实现更多的定制，处理复合手势，如点击、长按、双击等。

常用的中层级 API：

• Modifier.pointerInput()：允许处理自定义手势逻辑
• detectTapGestures()：检测轻击、长按、双击等手势
• detectTransformGestures()：处理缩放、旋转等手势

示例：使用 Modifier.pointerInput 检测点击和长按

```kotlin
Box(
    modifier = Modifier
        .size(100.dp)
        .pointerInput(Unit) {
            detectTapGestures(
                onTap = {
                    // 处理单击事件
                },
                onLongPress = {
                    // 处理长按事件
                }
            )
        }
)
```

中层级抽象允许开发者在简单手势和复杂手势之间找到一个平衡点。开发者可以组合或扩展现有手势来满足特定的需求，而不需要从零开始构建事件处理。

3. 低层级抽象 (Low-level Abstraction)

低层级抽象提供了对手势事件的最大控制，开发者可以直接接触并处理原始的指针事件（Pointer Events）。这种抽象级别非常灵活，适用于需要完全控制手势的场景，例如自定义多点触控手势、复杂的手势识别、手势之间的冲突处理等。

常用的低层级 API：

• awaitPointerEventScope()：提供一个协程作用域，用于监听和处理指针事件
• awaitPointerEvent()：等待并捕获原始的指针事件
• PointerInputChange：包含指针事件的详细信息

示例：使用低层级 API 处理自定义手势

```kotlin
Box(
    modifier = Modifier
        .size(100.dp)
        .pointerInput(Unit) {
            awaitPointerEventScope {
                while (true) {
                    val event = awaitPointerEvent()
                    // 处理指针事件（例如处理多点触控）
                    event.changes.forEach { pointerInputChange ->
                        // 根据事件的变化执行相应的操作
                        if (pointerInputChange.changedToDown()) {
                            // 处理手指按下事件
                        }
                    }
                }
            }
        }
)
```

在低层级抽象中，开发者可以获取到每个指针的详细信息，如按下、抬起、移动等，并根据需求编写复杂的手势识别逻辑。这种方式最为灵活，但也需要更多的代码和手动处理。

三个抽象级别的对比：

| 抽象级别   | 适用场景             | 特点                                                         |
| ---------- | -------------------- | ------------------------------------------------------------ |
| 高层级抽象 | 简单、常见的手势需求 | 封装的手势处理，易于使用，快速实现交互                       |
| 中层级抽象 | 需要自定义或组合手势 | 提供灵活性，允许处理复合手势或扩展手势响应                   |
| 低层级抽象 | 需要对手势完全控制   | 最大程度的灵活性，可以处理复杂的、完全自定义的手势和多指操作 |

结论

Jetpack Compose 提供了不同抽象级别的手势处理方式，开发者可以根据需求选择合适的方式来实现用户交互。从简单的高层级抽象到高度自定义的低层级抽象，Compose 提供了灵活且强大的 API 支持，使得手势处理既能简单实现，也可以根据需求做深入的自定义。
