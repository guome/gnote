## 基本使用

可以左右滑动的组件

效果图：

![image-20211111160539995](./ViewPager2.assets/image-20211111160539995.png)

代码

MainActivity.java文件

```java
public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ViewPager2 viewPager = findViewById(R.id.viewPager);
        ViewPageAdapter viewPageAdapter = new ViewPageAdapter();
        viewPager.setAdapter(viewPageAdapter);
    }
}
```

activity_main.xml文件

```xml
<?xml version="1.0" encoding="utf-8"?>
<LinearLayout xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:tools="http://schemas.android.com/tools"
    android:layout_width="match_parent"
    android:layout_height="match_parent"
    tools:context=".MainActivity">

    <androidx.viewpager2.widget.ViewPager2
        android:id="@+id/viewPager"
        android:layout_width="match_parent"
        android:layout_height="match_parent"
        android:background="@color/design_default_color_primary" />

</LinearLayout>
```

item_page.xml文件【填充ViewPager2组件的布局】

```xml
<?xml version="1.0" encoding="utf-8"?>
<RelativeLayout xmlns:android="http://schemas.android.com/apk/res/android"
    android:id="@+id/container"
    android:layout_width="match_parent"
    android:layout_height="match_parent">

    <TextView
        android:id="@+id/tvTitle"
        android:layout_width="wrap_content"
        android:layout_height="wrap_content"
        android:layout_centerInParent="true"
        android:text="hello"
        android:textColor="#ff4532"
        android:textSize="32sp" />

</RelativeLayout>
```

ViewPageAdapter.java文件

其实主要的就是这个adapter，写法和 RecyclerView 是一样的

```java
public class ViewPageAdapter extends RecyclerView.Adapter<ViewPageAdapter.ViewPagerHolder> {

    private final List<String> titles = new ArrayList<>();

    private final List<Integer> colors = new ArrayList<>();

    public ViewPageAdapter() {
        titles.add("hello");
        titles.add("world");
        titles.add("you");

        colors.add(R.color.purple_200);
        colors.add(R.color.teal_200);
        colors.add(R.color.white);
    }

    @NonNull
    @Override
    public ViewPagerHolder onCreateViewHolder(
        @NonNull ViewGroup parent, int viewType
    ) {
        return new ViewPagerHolder(
            LayoutInflater.from(parent.getContext()).inflate(R.layout.item_page, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewPagerHolder holder, int position) {
        holder.mTv.setText(titles.get(position));
        holder.mContainer.setBackgroundResource(colors.get(position));
    }

    @Override
    public int getItemCount() {
        return titles.size();
    }

    static class ViewPagerHolder extends RecyclerView.ViewHolder {

        TextView mTv;

        RelativeLayout mContainer;

        public ViewPagerHolder(@NonNull View itemView) {
            super(itemView);

            mContainer = itemView.findViewById(R.id.container);
            mTv = itemView.findViewById(R.id.tvTitle);
        }
    }
}
```



## 结合Fragment的使用

实现类似微信的效果，依然是使用 ViewPager2 组件

效果图

![image-20211112142455968](./ViewPager2.assets/image-20211112142455968.png)

查看代码

MainActivity.java文件

```java
public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    ViewPager2 viewPager;

    private LinearLayout llWifi, llTool, llMember;

    private ImageView ivWifi, ivTool, ivMember, ivCurrent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initPager();
        initTabView();
    }

    private void initTabView() {
        llWifi = findViewById(R.id.id_tab_wifi);
        llTool = findViewById(R.id.id_tab_tool);
        llMember = findViewById(R.id.id_tab_member);

        ivWifi = findViewById(R.id.tab_iv_wifi);
        ivTool = findViewById(R.id.tab_iv_tool);
        ivMember = findViewById(R.id.tab_iv_member);

        llWifi.setOnClickListener(this);
        llTool.setOnClickListener(this);
        llMember.setOnClickListener(this);

        ivWifi.setSelected(true);
        ivCurrent = ivWifi;
    }

    private void initPager() {
        viewPager = findViewById(R.id.id_viewpage);

        ArrayList<Fragment> fragments = new ArrayList<>();
      	// 这里偷懒使用了同一个 fragment ，但是实际开发中一般都是不同的 fragment
        fragments.add(BlankFragment.newInstance("WIFI"));
        fragments.add(BlankFragment.newInstance("工具"));
        fragments.add(BlankFragment.newInstance("我的"));

        MyFragmentPagerAdapter pagerAdapter = new MyFragmentPagerAdapter(
            getSupportFragmentManager(),
            getLifecycle(),
            fragments
        );
      	// 这里设置的 adapter 必须继承 FragmentStateAdapter
        viewPager.setAdapter(pagerAdapter);

      	// 页面改变的时候监听函数
        viewPager.registerOnPageChangeCallback(new ViewPager2.OnPageChangeCallback() {
            @Override
            public void onPageScrolled(
                int position, float positionOffset, int positionOffsetPixels
            ) {
                super.onPageScrolled(position, positionOffset, positionOffsetPixels);
            }

            @Override
            public void onPageSelected(int position) {
                super.onPageSelected(position);
                changeTab(position);
            }


            @Override
            public void onPageScrollStateChanged(int state) {
                super.onPageScrollStateChanged(state);
            }
        });
    }

    @Override
    public void onClick(View v) {
        changeTab(v.getId());

    }

    private void changeTab(int id) {
        ivCurrent.setSelected(false);
        switch (id) {
            case 0:
            case R.id.id_tab_wifi:
                ivWifi.setSelected(true);
                ivCurrent = ivWifi;
            		// 比较常用的 api 设置当前项目
                viewPager.setCurrentItem(0);
                break;
            case 1:
            case R.id.id_tab_tool:
                ivTool.setSelected(true);
                ivCurrent = ivTool;
                viewPager.setCurrentItem(1);
                break;
            case 2:
            case R.id.id_tab_member:
                ivMember.setSelected(true);
                ivCurrent = ivMember;
                viewPager.setCurrentItem(2);
                break;
        }
    }
}
```

MyFragmentPagerAdapter.java文件

```java
public class MyFragmentPagerAdapter extends FragmentStateAdapter {

    List<Fragment> fragmentList;

    public MyFragmentPagerAdapter(
        @NonNull FragmentManager fragmentManager,
        @NonNull Lifecycle lifecycle,
        List<Fragment> fragments
    ) {
        super(fragmentManager, lifecycle);
        fragmentList = fragments;
    }

    @NonNull
    @Override
    public Fragment createFragment(int position) {
        return fragmentList.get(position);
    }

    @Override
    public int getItemCount() {
        return fragmentList.size();
    }
}
```

activity_main.xml文件

```xml
<?xml version="1.0" encoding="utf-8"?>
<LinearLayout xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:tools="http://schemas.android.com/tools"
    android:layout_width="match_parent"
    android:layout_height="match_parent"
    android:orientation="vertical"
    tools:context=".MainActivity">

    <androidx.viewpager2.widget.ViewPager2
        android:id="@+id/id_viewpage"
        android:layout_width="match_parent"
        android:layout_height="0dp"
        android:layout_weight="1" />

    <include layout="@layout/bottom_layout" />
</LinearLayout>
```

bottom_layout.xml

```xml
<?xml version="1.0" encoding="utf-8"?>
<LinearLayout xmlns:android="http://schemas.android.com/apk/res/android"
    android:layout_width="match_parent"
    android:layout_height="55dp"
    android:background="@color/gray"
    android:orientation="horizontal"
    android:baselineAligned="false">

    <LinearLayout
        android:id="@+id/id_tab_wifi"
        android:layout_width="0dp"
        android:layout_height="match_parent"
        android:layout_weight="1"
        android:gravity="center"
        android:orientation="vertical">

        <ImageView
            android:id="@+id/tab_iv_wifi"
            android:layout_width="wrap_content"
            android:layout_height="wrap_content"
            android:background="@drawable/tab_wifi" />

        <TextView
            android:id="@+id/text_wifi"
            android:layout_width="wrap_content"
            android:layout_height="wrap_content"
            android:gravity="center_vertical"
            android:text="WIFI" />

    </LinearLayout>

    <LinearLayout
        android:id="@+id/id_tab_tool"
        android:layout_width="0dp"
        android:layout_height="match_parent"
        android:layout_weight="1"
        android:gravity="center"
        android:orientation="vertical">

        <ImageView
            android:id="@+id/tab_iv_tool"
            android:layout_width="wrap_content"
            android:layout_height="wrap_content"
            android:background="@drawable/tab_tool" />

        <TextView
            android:id="@+id/text_tool"
            android:layout_width="wrap_content"
            android:layout_height="wrap_content"
            android:gravity="center_vertical"
            android:text="工具" />

    </LinearLayout>

    <LinearLayout
        android:id="@+id/id_tab_member"
        android:layout_width="0dp"
        android:layout_height="match_parent"
        android:layout_weight="1"
        android:gravity="center"
        android:orientation="vertical">

        <ImageView
            android:id="@+id/tab_iv_member"
            android:layout_width="wrap_content"
            android:layout_height="wrap_content"
            android:background="@drawable/tab_member" />

        <TextView
            android:id="@+id/text_member"
            android:layout_width="wrap_content"
            android:layout_height="wrap_content"
            android:gravity="center_vertical"
            android:text="我的" />

    </LinearLayout>

</LinearLayout>
```

tab_wifi.xml文件

tab_member.xml和tab_tool.xml类似

```xml
<?xml version="1.0" encoding="utf-8"?>
<selector xmlns:android="http://schemas.android.com/apk/res/android">
    <item android:drawable="@mipmap/ic_wifi_activity" android:state_selected="true" />
    <item android:drawable="@mipmap/ic_wifi_normal" android:state_selected="false" />
</selector>
```