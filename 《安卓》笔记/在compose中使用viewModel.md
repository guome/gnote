# 在 compose 中使用 viewModel

在 Jetpack Compose 中使用 ViewModel 非常方便，你可以直接使用 viewModel() 函数，这个函数可以帮助你轻松地获取 ViewModel，并且它会自动管理 ViewModel 的生命周期。下面是如何在 Jetpack Compose 中使用 ViewModel 的完整示例：

1. 创建 ViewModel 类

首先，你需要定义你的 ViewModel，例如：

```kotlin
import androidx.lifecycle.ViewModel
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.LiveData

class MainViewModel : ViewModel() {
private val _text = MutableLiveData("Hello, Compose!")
val text: LiveData<String> = _text

    fun updateText(newText: String) {
        _text.value = newText
    }

}
```

2. 在 Composable 中获取 ViewModel

在 Composable 中，可以使用 viewModel() 函数获取 ViewModel 实例：

```kotlin

import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.lifecycle.viewmodel.compose.viewModel

@Composable
fun MyScreen() {
// 获取 MainViewModel 实例
val viewModel: MainViewModel = viewModel()

    // 从 ViewModel 中读取 LiveData 值
    val text = viewModel.text.observeAsState(initial = "Loading...")

    // 显示 ViewModel 中的文本
    Text(text = text.value ?: "Error loading text")

}

```

3. 响应 ViewModel 的数据更新

通常，ViewModel 中的数据是通过 LiveData 或 StateFlow 来管理的。你可以在 Compose 中使用 observeAsState() 来观察 LiveData，或者使用 collectAsState() 来观察 StateFlow。例如，继续使用上面的 LiveData 示例：

```kotlin

import androidx.compose.material3.Button
import androidx.compose.runtime.getValue
import androidx.compose.runtime.setValue
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.runtime.\*

@Composable
fun MyScreen() {
val viewModel: MainViewModel = viewModel()

    // 观察 LiveData
    val text by viewModel.text.observeAsState(initial = "Loading...")

    // 使用 Compose UI 显示和更新 ViewModel 的数据
    Column {
        Text(text = text)

        Button(onClick = { viewModel.updateText("New Text!") }) {
            Text("Update Text")
        }
    }

}
```

4. 提供自定义的 ViewModel 实例

有时候，你可能需要传递参数给 ViewModel 或者使用一个自定义的 ViewModel 工厂。在这种情况下，你可以使用 viewModel(factory = ...) 来提供自定义工厂。以下是示例：

```kotlin

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider

// ViewModel 接受一个参数
class MainViewModel(initialText: String) : ViewModel() {
private val \_text = MutableLiveData(initialText)
val text: LiveData<String> = \_text
}

// 自定义 ViewModelFactory
class MainViewModelFactory(private val initialText: String) : ViewModelProvider.Factory {
override fun <T : ViewModel?> create(modelClass: Class<T>): T {
if (modelClass.isAssignableFrom(MainViewModel::class.java)) {
return MainViewModel(initialText) as T
}
throw IllegalArgumentException("Unknown ViewModel class")
}
}

@Composable
fun MyScreen(initialText: String) {
// 使用自定义的 ViewModelFactory
val viewModel: MainViewModel = viewModel(factory = MainViewModelFactory(initialText))

    val text by viewModel.text.observeAsState(initial = "Loading...")
    Text(text = text)

}

```

5. 处理生命周期

Jetpack Compose 和 ViewModel 自然地结合在一起，因此不需要手动管理 ViewModel 的生命周期。Compose 会自动为你处理 ViewModel 的创建和销毁，确保它在与 Activity 或 Fragment 的生命周期一致。

这就是在 Jetpack Compose 中使用 ViewModel 的基本方法，利用 viewModel() 函数可以让你轻松地在 Composable 中获取和管理 ViewModel。
