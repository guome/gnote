# WindowManager(窗口管理服务)

# 本节引言：

> 本节给大家带来的 Android 给我们提供的系统服务中的——WindowManager(窗口管理服务)， 它是显示 View 的最底层，Toast，Activity，Dialog 的底层都用到了这个 WindowManager， 他是全局的！该类的核心无非：调用 addView，removeView，updateViewLayout 这几个方法 来显示 View 以及通过 WindowManager.LayoutParams 这个 API 来设置相关的属性！
> 本节我们就来探讨下这个 WindowManager 在实际开发中的一些应用实例吧~

# 1.WindowManager 的一些概念：

## 1）WindowManager 介绍

> Android 为我们提供的用于与窗口管理器进行交互的一个 API！我们都知道 App 的界面都是 由一个个的 Acitivty 组成，而 Activity 又由 View 组成，当我们想显示一个界面的时候， 第一时间想起的是:Activity，对吧？又或者是 Dialog 和 Toast。
> 但是有些情况下，前面这三者可能满足不了我们的需求，比如我们仅仅是一个简单的显示 用 Activity 显得有点多余了，而 Dialog 又需要 Context 对象，Toast 又不可以点击... 对于以上的情况我们可以利用 WindowManager 这个东东添加 View 到屏幕上， 或者从屏幕上移除 View！他就是管理 Android 窗口机制的一个接口，显示 View 的最底层！

## 2）如何获得 WindowManager 实例

**① 获得 WindowManager 对象:**

```java
WindowManager wManager = getApplicationContext().getSystemService(Context. WINDOW_ SERVICE);
```

**② 获得 WindowManager.LayoutParams 对象，为后续操作做准备**

```java
WindowManager.LayoutParams wmParams=new WindowManager.LayoutParams();
```

## 2.WindowManager 使用实例：

### 实例 1：获取屏幕宽高

在 Android 4.2 前我们可以用下述方法获得屏幕宽高：

```java
public static int[] getScreenHW(Context context) {
    WindowManager manager = (WindowManager)context
    .getSystemService(Context.WINDOW_SERVICE);
    Display display = manager.getDefaultDisplay();
    int width = display.getWidth();
    int height = display.getHeight();
    int[] HW = new int[] { width, height };
    return HW;
}
```

而上述的方法在 Android 4.2 以后就过时了，我们可以用另一种方法获得屏幕宽高：

```java
public static int[] getScreenHW2(Context context) {
    WindowManager manager = (WindowManager) context.
    getSystemService(Context.WINDOW_SERVICE);
    DisplayMetrics dm = new DisplayMetrics();
    manager.getDefaultDisplay().getMetrics(dm);
    int width = dm.widthPixels;
    int height = dm.heightPixels;
    int[] HW = new int[] { width, height };
    return HW;
}
```

然后我们可以再另外写两个获取宽以及高的方法，这里以第二种获得屏幕宽高为例：

```java
public static int getScreenW(Context context) {
    return getScreenHW2(context)[0];
}

public static int getScreenH(Context context) {
    return getScreenHW2(context)[1];
}
```

当然，假如你不另外写一个工具类的话，你可以直接直接获取，比如：

```java
public class MainActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        WindowManager wManager = (WindowManager) getSystemService(Context.WINDOW_SERVICE);
        DisplayMetrics dm = new DisplayMetrics();
        wManager.getDefaultDisplay().getMetrics(dm);
        Toast.makeText(MainActivity.this, "当前手机的屏幕宽高：" + dm.widthPixels + "*" +
                dm.heightPixels, Toast.LENGTH_SHORT).show();
    }
}
```

## 实例 2：设置窗口全屏显示

```java
getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        getSupportActionBar().hide();
```

### 实例 3：保持屏幕常亮

```java
public void setKeepScreenOn(Activity activity,boolean keepScreenOn)
{
    if(keepScreenOn)
    {
        activity.getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
    }else{
        activity.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
    }
}
```

### 实例 4: 添加窗口

```kotlin

private const val OVERLAY_PERMISSION_REQ_CODE = 22

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        initCustomWindow()
    }

    private fun initCustomWindow() {
        if (!Settings.canDrawOverlays(this)) {
            Toast.makeText(this, "can not DrawOverlays", Toast.LENGTH_SHORT).show()
            val intent = Intent(
                Settings.ACTION_MANAGE_OVERLAY_PERMISSION,
                Uri.parse("package:$packageName")
            )
            startActivityForResult(intent, OVERLAY_PERMISSION_REQ_CODE)
        } else {
            handleAddWindow()
        }
    }

    private fun handleAddWindow() {
        val button = Button(this)
        button.setBackgroundResource(R.mipmap.ic_launcher)
        button.setOnClickListener {
            Toast.makeText(this, "外部按钮..", Toast.LENGTH_SHORT).show()
        }

        val windowManager = application.getSystemService(Context.WINDOW_SERVICE) as WindowManager
        val layoutParams = WindowManager.LayoutParams()

        layoutParams.apply {
            // 设置 WindowType
            // type = WindowManager.LayoutParams.TYPE_SYSTEM_ALERT
            type = WindowManager.LayoutParams.TYPE_APPLICATION_OVERLAY
            // 设置悬浮框不可触摸
            flags = WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL or WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE
            format = PixelFormat.RGBA_8888
            width = 200
            height = 200
            gravity = Gravity.START
            x = 200
            y = 200
        }

        windowManager.addView(button, layoutParams)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (resultCode == OVERLAY_PERMISSION_REQ_CODE) {
            if (Settings.canDrawOverlays(this)) {
                // 打开了权限
                handleAddWindow();
            } else {
                Toast.makeText(this, "can not DrawOverlays", Toast.LENGTH_SHORT).show();
            }
        }
    }
}
```

需要申请个权限,这玩意在高版本中没什么屌用

```xml
<uses-permission android:name="android.permission.SYSTEM_ALERT_WINDOW" />
```
