# 在 Git 中从某个标签（tag）创建新分支是一项常见任务。下面是具体的步骤和命令，展示如何从标签创建新分支。

## 步骤 1：列出所有标签

首先，你需要查看仓库中所有可用的标签。

```shell
git tag
```

这将列出所有现有的标签。例如，假设你有一个标签 v1.0.0。

## 步骤 2：从标签创建新分支

使用 git checkout 或 git switch 命令从标签 v1.0.0 创建一个新分支 new-branch。

使用 git checkout

```shell
git checkout -b new-branch v1.0.0

```

使用 git switch

git switch 是较新的命令，推荐在较新的 Git 版本中使用。

```shell
git switch -c new-branch v1.0.0
```

## 步骤 3：推送新分支到远程仓库（可选）

如果你需要将新分支推送到远程仓库，可以使用以下命令：

```shell
git push origin new-branch
```
