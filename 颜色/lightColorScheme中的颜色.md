lightColorScheme 是 Jetpack Compose 中定义浅色主题颜色的一个方案。它包含了多种颜色属性，分别控制应用不同部分的视觉效果。下面是 lightColorScheme 中的所有颜色属性：

1. Primary - 主色，应用的主要色调，通常用于顶部栏、按钮等主交互区域。
2. OnPrimary - 主色上的文本和图标颜色，通常为白色或高对比度色，以确保在主色背景上的可读性。
3. PrimaryContainer - 主色容器，用于需要更浅或更深的主色区域，例如大面积填充区域。
4. OnPrimaryContainer - 主色容器上的文本和图标颜色，用于在主色容器背景上显示清晰内容。
5. Secondary - 辅助色，用于次要按钮或强调性区域，补充主色。
6. OnSecondary - 辅助色上的文本和图标颜色，确保在辅助色背景上清晰显示。
7. SecondaryContainer - 辅助色容器，用于较大面积的辅助色区域。
8. OnSecondaryContainer - 辅助色容器上的文本和图标颜色。
9. Tertiary - 三级颜色，用于更次要的操作区域或强调区域。
10. OnTertiary - 三级颜色上的文本和图标颜色。
11. TertiaryContainer - 三级颜色容器，用于大面积的三级颜色背景。
12. OnTertiaryContainer - 三级颜色容器上的文本和图标颜色。
13. Background - 应用的背景色，通常用于整个页面的底色，浅色主题中常为白色或浅灰色。
14. OnBackground - 背景色上的文本和图标颜色，通常为深色以保持良好的可读性。
15. Surface - 表面色，用于卡片、对话框等组件的背景色。
16. OnSurface - 表面色上的文本和图标颜色。
17. SurfaceVariant - 表面色的变体，用于区分不同表面区域。
18. OnSurfaceVariant - 表面色变体上的文本和图标颜色。
19. Error - 错误色，用于错误提示和警告消息，通常为红色。
20. OnError - 错误色上的文本和图标颜色。
21. ErrorContainer - 错误容器色，显示在大面积的错误背景上。
22. OnErrorContainer - 错误容器色上的文本和图标颜色。
23. Outline - 边框和轮廓颜色，用于分隔和描边元素，比如输入框的边框。
24. OutlineVariant - 轮廓色的变体，用于细化轮廓的层次感。
25. InversePrimary - 主色的反转颜色，用于反色主题下的主色区域。
26. InverseSurface - 反色主题下的表面色，用于深色背景上的表面区域。
27. InverseOnSurface - 反色表面色上的文本和图标颜色，确保在反色表面色上保持清晰。
28. SurfaceTint - 表面色的修饰，用于为表面添加柔和的色彩调和效果，例如浅色阴影。

> 这些颜色属性组成了一个完整的 lightColorScheme，帮助设计师和开发者在浅色主题下定义一个功能全面且具有层次感的界面配色方案。
