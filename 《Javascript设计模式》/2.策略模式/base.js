const strategies = {
    isNonEmpty(value, errorMsg) {
        if (value === '') {
            return errorMsg
        }
    },
    minLength(value, minLength, errorMsg) {
        if (value.length < minLength) {
            return errorMsg
        }
    }
}

const Validator = function() {
    this.cache = []
}
/**
 * 添加校验规则
 * @param {HTMLAllCollection} dom dom元素
 * @param {string} rule 规则
 * @param {string} errorMsg 错误信息
 */
Validator.prototype.add = function(dom, rule, errorMsg) {
    // 把 strategy 和参数分开
    let ary = rule.split(":")
    this.cache.push(function() {
        // 用户挑选的 strategy
        let strategy = ary.shift();
        // 把input 的 value 添加进参数列表
        ary.unshift(dom.value)
        // 把 errorMsg 添加进参数列表
        ary.push(errorMsg)
        return strategies[strategy].apply(dom, ary)
    })
}