// 普通类
const CreateDiv = function (html) {
    this.html = html;
    this.init();
}

CreateDiv.prototype.init = function() {
    // .. 初始化
    console.log(this.html);
}

// 代理类，使其转为单例
const ProxySingletonCreateDiv = (() => {
    let instance
    return function(html) {
        if (!instance) {
            instance = new CreateDiv(html)
        }
        return instance
    }
})();

const d1 = new ProxySingletonCreateDiv(1)
const d2 = new ProxySingletonCreateDiv(2)

console.log(
    d1 === d2
)