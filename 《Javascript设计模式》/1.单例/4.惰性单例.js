/**
 * 在 JS 中其实不需要使用 Class,
 * 使用函数足以完成功能
 */

/**
 * 通用单例惰性
 * @param {Function} fn 函数
 * @returns {any}
 */
const getSingle = function(fn) {
    // 这里最好是预定义为 null
    // 因为函数默认返回的 undefined
    let result = null
    return function() {
        if (result === null) {
            result = fn.apply(this, arguments)
        }
        return result
    }
}

const createLogin = function() {
    console.log("createLogin");
}

const createIFrame = function() {
    console.log("createIFrame");
}

const cs = getSingle(createLogin)
const cf = getSingle(createIFrame)

console.log(
    cs(1) === cs(2),
    cf(1) === cf(2)
)