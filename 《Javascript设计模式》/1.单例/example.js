const Singleton = function (name) {
    this.name = name
    this.instance = null
}

Singleton.prototype.getName = function () {
    console.log(this.name)
}

Singleton.getInstance = function (name) {
    if (!this.instance) {
        this.instance = new Singleton(name)
    }
    return this.instance
}

const instance1 = Singleton.getInstance("a");
const instance2 = Singleton.getInstance("a");
