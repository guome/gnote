const CreateDiv = (() => {
    let instance;

    const CreateDiv = function (html) {
        if (instance) {
            return instance
        }
        this.html = html;
        this.init();
        return instance = this;
    }

    CreateDiv.prototype.init = function() {
        // .. 初始化
        console.log(this.html);
    }

    return CreateDiv;
})();

const d1 = new CreateDiv("1");
const d2 = new CreateDiv("2");

console.log(d1 === d2)