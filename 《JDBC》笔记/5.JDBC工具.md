### 封装

JDBCUtils

```java
package utils;

import java.io.IOException;
import java.io.InputStream;
import java.sql.*;
import java.util.Properties;

public class JDBCUtils {
    // 有点像全局变量
    private static final ThreadLocal<Connection> threadLocal = new ThreadLocal<>();

    private static final Properties PROPERTIES = new Properties();

    static String driver = "";
    static String url = "";
    static String user = "";
    static String password = "";

    // 类加载执行一次
    static {
        try {
            InputStream is = JDBCUtils.class.getResourceAsStream("/db.properties");
            try {
                PROPERTIES.load(is);
                driver = PROPERTIES.getProperty("driver");
                url = PROPERTIES.getProperty("url");
                user = PROPERTIES.getProperty("user");
                password = PROPERTIES.getProperty("password");
                // 加载注册驱动
                Class.forName(driver);
            } catch (IOException e) {
                e.printStackTrace();
            }
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    public static Connection getConnect() {
        // 将当前线程中绑定的Connection对象，赋值给connection
        Connection connection = threadLocal.get();

        try {
            if (connection == null) {
                // 获取连接
                connection = DriverManager.getConnection(url, user, password);
                // 把连接存到当前线程共享中
                threadLocal.set(connection);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return connection;
    }

    public static void begin() {
        try {
            Connection connect = getConnect();
            connect.setAutoCommit(false);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    public static void commit() {
        Connection connect = null;
        try {
            connect = getConnect();
            connect.commit();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } finally {
            closeAll(connect, null, null);
        }
    }

    public static void rollback() {
        Connection connection = null;
        try {
            connection = getConnect();
            connection.rollback();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } finally {
            closeAll(connection, null, null);
        }
    }


    public static void closeAll(Connection connection, Statement statement, ResultSet resultSet) {
        try {
            // 释放资源
            // 先开后关
            if (resultSet != null) {
                resultSet.close();
            }

            if (statement != null) {
                statement.close();
            }

            if (connection != null) {
                connection.close();
                // 移除已关闭的connect对象
                threadLocal.remove();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}

```

db.properties

```properties
# 新版 com.mysql.jdbc.Driver 被废弃, 请使用 com.mysql.cj.jdbc.Driver
driver=com.mysql.cj.jdbc.Driver
url=jdbc:mysql://localhost:3306/db01?useSSL=false
user=root
password=123456
```

DaoUtils

```java
package utils;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class DaoUtils<T> {
    /**
     * 公共处理增删改
     *
     * @param sql  sql语句
     * @param args 参数列表
     * @return 受影响的行数
     */
    public int commonUpdate(String sql, Object... args) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;

        try {
            connection = JDBCUtils.getConnect();
            preparedStatement = connection.prepareStatement(sql);

            for (int i = 0; i < args.length; i++) {
                preparedStatement.setObject(i + 1, args[i]);
            }

            return preparedStatement.executeUpdate();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } finally {
            JDBCUtils.closeAll(connection, preparedStatement, null);
        }

        return 0;
    }

    /**
     * 公告查询
     *
     * @param sql       sql语句
     * @param rowMapper 回调
     * @param args      参数列表
     * @return 结果集
     */
    public List<T> commonsSelect(String sql, RowMapper<T> rowMapper, Object... args) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        List<T> list = new ArrayList<T>();

        try {
            connection = JDBCUtils.getConnect();
            preparedStatement = connection.prepareStatement(sql);
            if (args != null) {
                for (int i = 0; i < args.length; i++) {
                    preparedStatement.setObject(i + 1, args[i]);
                }
            }
            resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                T row = rowMapper.getRow(resultSet);
                list.add(row);
            }

            return list;
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } finally {
            JDBCUtils.closeAll(connection, preparedStatement, resultSet);
        }
        return null;
    }
}

```

RowMapper

```java
package utils;

import java.sql.ResultSet;

/**
 * 约束封装对象的ORM
 */
public interface RowMapper<T> {
    public T getRow(ResultSet resultSet);
}

```

DateUtils

```java
package utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;

public class DateUtils {
    static String format = "yyy-MM-dd";

    private static final SimpleDateFormat simpleDateFormat = new SimpleDateFormat(format);

    /**
     * 字符串转为 util.Date
     */
    public static java.util.Date strToUtil(String str) {
        try {
            return simpleDateFormat.parse(str);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return null;
    }

    /**
     * util.Date转为sql.Date
     */
    public static java.sql.Date utilToSql(java.util.Date date) {
        return new java.sql.Date(date.getTime());
    }

    /**
     * util.Date转为字符串
     */
    public static String utilToStr(java.util.Date date) {
        return simpleDateFormat.format(date);
    }
}

```





### 使用

实体类

```java
package admin;

import java.util.Date;

public class Admin {
    private String username;
    private String password;
    private String phone;
    private String address;
    private Date birthday;

    public Admin() {
    }


    public Admin(String username, String password, String phone, String address, Date birthday) {
        this.username = username;
        this.password = password;
        this.phone = phone;
        this.address = address;
        this.birthday = birthday;
    }

    public Date getBirthday() {
        return birthday;
    }

    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    @Override
    public String toString() {
        return "Admin{" +
                "username='" + username + '\'' +
                ", password='" + password + '\'' +
                ", phone='" + phone + '\'' +
                ", address='" + address + '\'' +
                ", birthday=" + birthday +
                '}';
    }
}

```

DAO

```java
package admin;

import utils.DaoUtils;

import java.util.List;

public class AdminDaoImpl {
    public DaoUtils<Admin> daoUtils = new DaoUtils<>();

    public int insert(Admin admin) {
        return daoUtils.commonUpdate(
                "insert into admin(username, password, phone, address, birthday) VALUES (?,?,?,?,?)",
                admin.getUsername(),
                admin.getPassword(),
                admin.getPhone(),
                admin.getAddress(),
                admin.getBirthday()
        );
    }

    public int update(Admin admin) {
        String username = admin.getUsername();
        String sql = "update admin set username=?, password=?,phone=?,address=?,birthday=? where username=?";

        return daoUtils.commonUpdate(
                sql,
                username,
                admin.getPassword(),
                admin.getPhone(),
                admin.getAddress(),
                admin.getBirthday(),
                username
        );
    }

    public int delete(String username) {
        String sql = "delete from admin where username=?";

        return daoUtils.commonUpdate(
                sql,
                username
        );
    }

    public Admin select(String name) {
        String sql = "select * from admin where username=?";
        List<Admin> list = daoUtils.commonsSelect(sql, new AdminRowMapper(), name);

        if (!list.isEmpty()) {
            return list.get(0);
        }
        return null;
    }

    public List<Admin> selectAll() {
        String sql = "select * from admin";
        return daoUtils.commonsSelect(sql, new AdminRowMapper());
    }
}

```

service

```java
package admin;

public class AdminServiceImpl {
    public void register(Admin admin) {
        AdminDaoImpl adminDao = new AdminDaoImpl();
        Admin select = adminDao.select(admin.getUsername());

        if (select == null) {
            adminDao.insert(admin);
            System.out.println("注册成功");
        } else {
            System.out.println("已经注册了");
        }
    }
}

```

rowMapper

```java
package admin;

import utils.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class AdminRowMapper implements RowMapper<Admin> {
    @Override
    public Admin getRow(ResultSet resultSet) {
        Admin admin = null;

        try {
            admin = new Admin(
                    resultSet.getString("username"),
                    resultSet.getString("password"),
                    resultSet.getString("phone"),
                    resultSet.getString("address"),
                    null
            );
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }

        return admin;
    }
}

```

调用

```java
package admin;

import utils.DateUtils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class TestAdmin {
    public static void main(String[] args) {
        // insert();
        // update();
        // delete();
        // select();
        selectAll();
        // testTime();

        // register();
    }

    public static void insert() {
        AdminDaoImpl adminDao = new AdminDaoImpl();
        Admin admin = new Admin("李四2", "123456", "123400", "上饶", DateUtils.strToUtil("2002-03-03"));

        int rows = adminDao.insert(admin);
        System.out.println(rows);
    }

    public static void update() {
        AdminDaoImpl adminDao = new AdminDaoImpl();
        Admin admin = new Admin("李四", "11111", "0793-123400", "江西-上饶", DateUtils.strToUtil("2022-01-03"));

        int rows = adminDao.update(admin);
        System.out.println(rows);
    }

    public static void delete() {
        AdminDaoImpl adminDao = new AdminDaoImpl();
        int rows = adminDao.delete("李四");
        System.out.println(rows);
    }

    public static void select() {
        AdminDaoImpl adminDao = new AdminDaoImpl();
        Admin result = adminDao.select("张三");
        System.out.println(result);
    }

    public static void selectAll() {
        AdminDaoImpl adminDao = new AdminDaoImpl();
        List<Admin> admins = adminDao.selectAll();
        System.out.println(admins);
    }

    public static void testTime() {
        Date date = new Date();
        System.out.println(date);

        String str = "1990-09-23";
        // 将字符串转为 Util.Date
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        try {
            // 字符串转为 java.util.Date
            Date parse = simpleDateFormat.parse(str);
            System.out.println(parse);
            // java.util.Date对象转为字符串
            String format = simpleDateFormat.format(date);
            System.out.println(format);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        // sql.Date 不支持字符串转换，只支持毫秒
        java.sql.Date sqlDate = new java.sql.Date(date.getTime());
        System.out.println(sqlDate);
    }

    public static void register() {
        AdminServiceImpl adminService = new AdminServiceImpl();
        adminService.register(new Admin("王五", "123", "166", "65161", DateUtils.strToUtil("1990-04-04")));
    }
}

```

