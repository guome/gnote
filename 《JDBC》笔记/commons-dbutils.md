Apache提供的JDBC工具库

需要连接池

没有解决的问题是：事务

文档：https://commons.apache.org/proper/commons-dbutils/index.html

使用

DruidUtils

```java
package utils;

import admin.AdminDaoImpl;
import com.alibaba.druid.pool.DruidDataSourceFactory;

import javax.sql.DataSource;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class DruidUtils {
    public static DataSource getConnect() {
        Properties properties = new Properties();
        InputStream is = AdminDaoImpl.class.getResourceAsStream("/druid.properties");
        try {
            properties.load(is);
            return DruidDataSourceFactory.createDataSource(properties);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }
}

```

```java
package admin;

import com.alibaba.druid.pool.DruidDataSourceFactory;
import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.BeanHandler;
import utils.DaoUtils;
import utils.DruidUtils;
import utils.JDBCUtils;

import javax.sql.DataSource;
import java.io.IOException;
import java.io.InputStream;
import java.sql.SQLException;
import java.util.List;
import java.util.Properties;
import java.util.Queue;

public class AdminDaoImpl {
    public DaoUtils<Admin> daoUtils = new DaoUtils<>();

    public int insert2(Admin admin) {
        Object[] params = {
                admin.getUsername(),
                admin.getPassword(),
                admin.getPhone(),
                admin.getAddress(),
                admin.getBirthday()
        };

        String sql = "insert into admin(username, password, phone, address, birthday) VALUES (?,?,?,?,?)";

        try {
            DataSource dataSource = DruidUtils.getConnect();
            // 需要一个连接池
            QueryRunner queryRunner = new QueryRunner(dataSource);
            return queryRunner.update(sql, params);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return 0;
    }

    public Admin select2(String name) {
        String sql = "select * from admin where username=?";
        QueryRunner queryRunner = new QueryRunner(DruidUtils.getConnect());
        try {
            return queryRunner.query(sql, new BeanHandler<Admin>(Admin.class), name);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }

        return null;
    }

}

```

