```java


package utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;

public class DateUtils {
    static String format = "yyy-MM-dd";

    private static final SimpleDateFormat simpleDateFormat = new SimpleDateFormat(format);

    /**
     * 字符串转为 util.Date
     */
    public static java.util.Date strToUtil(String str) {
        try {
            return simpleDateFormat.parse(str);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return null;
    }

    /**
     * util.Date转为sql.Date
     */
    public static java.sql.Date utilToSql(java.util.Date date) {
        return new java.sql.Date(date.getTime());
    }

    /**
     * util.Date转为字符串
     */
    public static String utilToStr(java.util.Date date) {
        return simpleDateFormat.format(date);
    }
}

```

