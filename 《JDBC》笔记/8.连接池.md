### C3P0连接池

```java
package jdbc.datasource;

import com.mchange.v2.c3p0.ComboPooledDataSource;

import java.io.FileInputStream;
import java.sql.Connection;
import java.util.Properties;

public class C3P0_ {
    public static void main(String[] args) {
        try {
            testC3P0_01();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void testC3P0_01() throws Exception {
        // 1,创建数据源对象
        ComboPooledDataSource comboPooledDataSource = new ComboPooledDataSource();

        // 2,通过配置文件获取相关的信息
        Properties properties = new Properties();
        properties.load(new FileInputStream("src/mysql.properties"));
        String user = properties.getProperty("user");
        String password = properties.getProperty("password");
        String url = properties.getProperty("url");
        String driver = properties.getProperty("driver");

        // 给数据源 comboPooledDataSource 设置相关的参数
        // 注意：连接管理是由 comboPooledDataSource 来管理的
        comboPooledDataSource.setDriverClass(driver);
        comboPooledDataSource.setJdbcUrl(url);
        comboPooledDataSource.setUser(user);
        comboPooledDataSource.setPassword(password);

        // 初始化连接数
        comboPooledDataSource.setInitialPoolSize(10);
        // 最大连接数
        comboPooledDataSource.setMaxPoolSize(50);

        // 连接数据库
        Connection connection = comboPooledDataSource.getConnection();
        System.out.println("连接成功...");
        // 关闭连接
        connection.close();
    }
}

```

配置文件形式

```java
ComboPooledDataSource comboPooledDataSource = new ComboPooledDataSource();
Connection connection = comboPooledDataSource.getConnection();
System.out.println("connection" + connection);
System.out.println("连接成功");
connection.close();
```

配置文件 c3p0-config.xml 必须要放在src目录下

```xml
<?xml version="1.0" encoding="UTF-8"?>

<c3p0-config>
    <!-- 默认配置 创建连接池对象时，默认是加载该配置信息-->
    <default-config>
        <property name="jdbcUrl">jdbc:mysql://localhost:3306/hsp_db02</property>
        <property name="driverClass">com.mysql.jdbc.Driver</property>
        <property name="user">root</property>
        <property name="password">123456</property>
        <!-- 池参数 -->
        <!-- 每次增长的连接数 -->
        <property name="acquireIncrement">5</property>
        <!--初始化连接数-->
        <property name="initialPoolSize">30</property>
        <!--连接池中保留的最少连接数。-->
        <property name="minPoolSize">2</property>
        <!--连接池中保留的最大连接数。-->
        <property name="maxPoolSize">60</property>
        <!-- 可连接的最多的命令对象数 -->
        <!--<property name="maxStatements">5</property>-->
        <!--每个连接对象可连接的最多的命令对象数-->
        <!--<property name="maxStatementsPerConnection">2</property>-->
    </default-config>
    <!-- 为oracle提供的配置 创建连接池对象时，可以指定命名加载配置信息-->
    <named-config name="oracle-config">
        <property name="jdbcUrl">jdbc:oracle:thin:@地址:端口:ORCL</property>
        <property name="driverClass">oracle.jdbc.driver.OracleDriver</property>
        <property name="user">root</property>
        <property name="password">12345</property>
        <!-- 池参数 -->
        <property name="acquireIncrement">3</property>
        <property name="initialPoolSize">30</property>
        <property name="minPoolSize">2</property>
        <property name="maxPoolSize">50</property>
    </named-config>
</c3p0-config>
```

> 配置文件的形式，到目前为止，我一直失败，不知道为啥

### Druid连接池

```java
package jdbc.datasource;

import com.alibaba.druid.pool.DruidDataSource;
import com.alibaba.druid.pool.DruidDataSourceFactory;

import javax.sql.DataSource;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.sql.Connection;
import java.util.Properties;

public class Druid_ {
    public static void main(String[] args) throws Exception {
        Properties properties = new Properties();

        properties.load(new FileInputStream("src/druid.properties"));
        DataSource dataSource = DruidDataSourceFactory.createDataSource(properties);

        Connection connection = dataSource.getConnection();
        System.out.println("连接成功");
      	// 放回连接池
        connection.close();

    }
}

```

druid.properties

```properties
# 数据库连接参数
url=jdbc:mysql://localhost:3306/hsp_db02
username=root
password=123456
driverClassName=com.mysql.jdbc.Driver
# 连接池的参数
initialSize=10
maxActive=10
maxWait=2000
```

