# token 主动"失效"

一般 jwt 签发的 token 是没有主动失效机制的，只要签发了这个 token，就只能等它到期了才会失效

spring 需要使用 redis 方式使它失效

具体步骤如下：

## 登陆时存入 redis

```java
@Autowired
private UserService userService;

@Autowired
private StringRedisTemplate stringRedisTemplate;

@PostMapping("/login")
public Result<?> login(@Pattern(regexp = "^\\S{5,16}$") String username, @Pattern(regexp = "^\\S{5,16}$") String password) {
    User user = userService.findByUserName(username);

    if (user == null) {
        throw new RuntimeException("用户名错误");
    }

    if (!Md5Util.getMD5String(password).equals(user.getPassword())) {
        throw new RuntimeException("密码错误");
    }

    HashMap<String, Object> claims = new HashMap<>();
    claims.put("id", user.getId());
    claims.put("username", user.getUsername());
    String token = JwtUtil.genToken(claims);

    // 设置过期时间
    // key 设置为 token，但是这种方式感觉会存在大量的 token
    stringRedisTemplate.opsForValue().set(token, token, JwtUtil.expiresTime, TimeUnit.SECONDS);

    return Result.success(token);
}
```

## 在拦截器中校验客户端发过来的 token 是否在 redis 中存在，并且还需要解析成功

```java
@Component
public class LoginInterceptor implements HandlerInterceptor {

    @Autowired
    private StringRedisTemplate redisTemplate;

    /**
     * 该方法在控制器方法之前执行，其返回值用来表示是否中断后续操作。
     * 返回值为 true 时，表示继续向下执行；
     * 返回值为 false 时，表示中断后续的操作。
     */
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) {
        String token = request.getHeader("Authorization");

        String redisToken = redisTemplate.opsForValue().get(token);

        // redis 中如果不存在客户端传过来的token，则说明 token 失效了
        if (redisTemplate.opsForValue().get(token) == null || !Objects.equals(redisToken, token)) {
            response.setStatus(401);
            throw new RuntimeException("token 失效~~~");
        }

        try {
            Map<String, Object> claims = JwtUtil.parseToken(token);
            ThreadLocalUtil.set(claims);

            return true;
        } catch (Exception e) {
            response.setStatus(401);
            throw new RuntimeException("未登陆~~~");
        }
    }

    /**
     * 该方法会在整个请求完成后，即视图渲染结束之后执行。我们可以通过该方法实现资源清理、日志记录等工作。
     */
    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) {
        ThreadLocalUtil.remove();
    }
}
```

## 更新密码的时候需要使 token 失效

```java
@PatchMapping("/updatePwd")
public Result<?> updatePwd(@RequestBody Map<String, String> params, @RequestHeader("Authorization") String token) {
    String oldPwd = params.get("old_pwd");
    String newPwd = params.get("new_pwd");
    String rePwd = params.get("re_pwd");

    if (!StringUtils.hasLength(oldPwd)) {
        throw new RuntimeException("原密码不存在");
    }

    if (!StringUtils.hasLength(newPwd)) {
        throw new RuntimeException("新密码不存在");
    }

    if (!StringUtils.hasLength(rePwd)) {
        throw new RuntimeException("确认密码不存在");
    }

    Map<String, Object> map = ThreadLocalUtil.get();
    String username = (String) map.get("username");
    User loginUser = userService.findByUserName(username);

    if (!loginUser.getPassword().equals(Md5Util.getMD5String(oldPwd))) {
        throw new RuntimeException("原密码错误");
    }

    if (!newPwd.equals(rePwd)) {
        throw new RuntimeException("两次密码不一致");
    }

    userService.updatePwd(newPwd);

    // 删除 token
    stringRedisTemplate.opsForValue().getOperations().delete(token);

    return Result.success();
}
```

当前也可以做一个退出登陆的接口，把 token 删了
