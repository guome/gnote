# 参考

https://www.modb.pro/db/629856

# 修改过滤器

````java

public class JsonLoginFilter extends UsernamePasswordAuthenticationFilter {
    /**
     * 这段代码和父类的代码几乎是一样的，只是取参数的地方不一样罢了
     */
    @Override
    public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response) throws AuthenticationException {
        String contentType = request.getContentType();

        // 说明请求参数是 JSON
        if (MediaType.APPLICATION_JSON_VALUE.equalsIgnoreCase(contentType) || MediaType.APPLICATION_JSON_UTF8_VALUE.equalsIgnoreCase(contentType)) {
            if (!request.getMethod().equals("POST")) {
                throw new AuthenticationServiceException("authentication method not supported: " + request.getMethod());
            }

            String username;
            String password;


            try {
                ServletInputStream inputStream = request.getInputStream();
                // AccountUser 必须有无参构造，否则会报错 jackson.databind.exc.InvalidDefinitionException cannot deserialize from Object value
                AccountUser user = new ObjectMapper().readValue(inputStream, AccountUser.class);
                username = user.getUsername();
                password = user.getPassword();
                username = (username != null) ? username.trim() : "";
                password = (password != null) ? password.trim() : "";
            } catch (IOException e) {
                throw new RuntimeException(e);
            }

            // 构建登陆令牌
            UsernamePasswordAuthenticationToken authRequest = UsernamePasswordAuthenticationToken.unauthenticated(username, password);

            // Allow subclasses to set the "details" property
            // 允许子类设置 details 属性
            setDetails(request, authRequest);

            // 执行真正的登陆操作
            return this.getAuthenticationManager().authenticate(authRequest);
        } else {
            return super.attemptAuthentication(request, response);
        }
    }
}
```

自己定义用户

```java
@Data
@NoArgsConstructor
public class AccountUser implements UserDetails {

    private Long userId;

    private String password;

    private String username;

    private Collection<? extends GrantedAuthority> authorities;

    private boolean accountNonExpired;

    private boolean accountNonLocked;

    private boolean credentialsNonExpired;

    private boolean enabled;

    public AccountUser(Long userId, String username, String password, Collection<? extends GrantedAuthority> authorities) {
        this(userId, username, password, true, true, true, true, authorities);
    }

    public AccountUser(Long userId, String username, String password, boolean enabled, boolean accountNonExpired,
                       boolean credentialsNonExpired, boolean accountNonLocked,
                       Collection<? extends GrantedAuthority> authorities) {
        Assert.isTrue(username != null && !"".equals(username) && password != null,
                "Cannot pass null or empty values to constructor");
        this.userId = userId;
        this.username = username;
        this.password = password;
        this.enabled = enabled;
        this.accountNonExpired = accountNonExpired;
        this.credentialsNonExpired = credentialsNonExpired;
        this.accountNonLocked = accountNonLocked;
        this.authorities = authorities;
    }


    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return this.authorities;
    }

    @Override
    public String getPassword() {
        return this.password;
    }

    @Override
    public String getUsername() {
        return this.username;
    }

    @Override
    public boolean isAccountNonExpired() {
        return this.accountNonExpired;
    }

    @Override
    public boolean isAccountNonLocked() {
        return this.accountNonLocked;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return this.credentialsNonExpired;
    }

    @Override
    public boolean isEnabled() {
        return this.enabled;
    }
}

```

配置

```java

@Configuration
public class SecurityConfig {
    @Autowired
    private SysUserServiceImpl sysUserService;

    @Bean
    public SecurityFilterChain filterChain(HttpSecurity http) throws Exception {
        http.authorizeHttpRequests((authorizeHttpRequests) ->
                // 所有的请求都要认证之后才能访问
                authorizeHttpRequests.anyRequest().authenticated()
        );

        http.formLogin((formLogin) ->
                formLogin
                        .usernameParameter("username")
                        .passwordParameter("password")
                        .permitAll()
        );

        // 禁用 session
        http.sessionManagement((httpSecuritySessionManagementConfigurer) -> {
            httpSecuritySessionManagementConfigurer.sessionCreationPolicy(SessionCreationPolicy.STATELESS);
        });


        // 替换掉 UsernamePasswordAuthenticationFilter 过滤器
        http.addFilterAt(jsonLoginFilter(), UsernamePasswordAuthenticationFilter.class);

        // 加个过滤器，保证即使是 session 禁用了也是保持登陆状态的
        http.addFilter(jwtAuthenticationFilter());


        // 这里的代码也可以替换下面的 filter.setAuthenticationFailureHandler
        // 这个看个人习惯了
        // http.exceptionHandling((exceptionHandling) -> {
        //     // 认证失败
        //     exceptionHandling.authenticationEntryPoint((request, response, authException) -> {
        //         System.out.println("未登陆?????" + authException.getClass());
        //         String message;
        //         response.setContentType("application/json;charset=UTF-8");

        //         if (authException instanceof BadCredentialsException) {
        //             message = "用户名或密码错误";
        //         } else if (authException instanceof DisabledException) {
        //             message = "账户被禁用，登录失败";
        //         } else if (authException instanceof CredentialsExpiredException) {
        //             message = "密码过期，登录失败";
        //         } else if (authException instanceof AccountExpiredException) {
        //             message = "账户过期，登录失败";
        //         } else if (authException instanceof LockedException) {
        //             message = "账户被锁定，登录失败";
        //         } else {
        //             // 还未登陆
        //             message = authException.getMessage();
        //         }

        //         response.setStatus(HttpStatus.UNAUTHORIZED.value());
        //         response.getWriter().write(message);
        //     });

        //     // 权限不足
        //     exceptionHandling.accessDeniedHandler((request, response, accessDeniedException) -> {
        //         System.out.println("权限不足?????");
        //         response.setStatus(HttpStatus.UNAUTHORIZED.value());

        //         response.getWriter().write("权限不足");
        //     });
        // });

        // http.csrf((csrf) -> csrf.disable())
        http.csrf(AbstractHttpConfigurer::disable);

        return http.build();
    }

    // 用户1, 自定义从数据库取
    @Bean
    UserDetailsService user1() {
        return username -> {
            SysUser user = sysUserService.getById(2);
            return new User(
                    user.getUsername(),
                    "{noop}" + user.getPassword(),
                    new ArrayList<>()
            );

        };
    }

    // 用户2，自定义一个内存的用户
    @Bean
    UserDetailsService user2() {
        return new InMemoryUserDetailsManager(
                User.builder()
                        .username("u2")
                        .password("{noop}123")
                        .build()
        );
    }

    // 用户3，自定义一个内存的用户
    @Bean
    UserDetailsService user3() {
        return new InMemoryUserDetailsManager(
                User.builder()
                        .username("u3")
                        .password("{noop}123")
                        .build()
        );
    }

    // 定义 AuthenticationManager ，会被框架调用
    @Bean
    AuthenticationManager authenticationManager() {
        // 用户名密码
        // DaoAuthenticationProvider dao1 = new DaoAuthenticationProvider();
        DaoAuthenticationProvider dao1 = new DaoAuthenticationProvider();
        dao1.setUserDetailsService(user1());

        // 用户名密码
        DaoAuthenticationProvider dao2 = new DaoAuthenticationProvider();
        dao2.setUserDetailsService(user2());

        // 用户名密码
        DaoAuthenticationProvider dao3 = new DaoAuthenticationProvider();
        dao3.setUserDetailsService(user3());

        // 把 AuthenticationProvider 给 ProviderManager
        // 当系统进行身份认证操作时，就会遍历 ProviderManager 中不同的 DaoAuthenticationProvider，进而调用到不同的数据源。
        // 这个是按顺序执行的
        // ProviderManager 是 AuthenticationManager 的实现类
        return new ProviderManager(dao1, dao2, dao3);
    }

    @Bean
    JsonLoginFilter jsonLoginFilter() {
        JsonLoginFilter filter = new JsonLoginFilter();

        filter.setAuthenticationSuccessHandler((request, response, authentication) -> {
            response.setContentType(MediaType.APPLICATION_JSON_UTF8_VALUE);
            PrintWriter out = response.getWriter();
            // 获取当前登陆成功的用户对象
            User user = (User) authentication.getPrincipal();

            out.write("登陆成功!!!");
        });

        // 但是任何请求只要没有登陆就会走这里，
        // 甚至都会报这个错误: Authentication method not supported: GET
        filter.setAuthenticationFailureHandler((request, response, exception) -> {
            response.setContentType(MediaType.APPLICATION_JSON_UTF8_VALUE);
            String message = "登陆失败~~~";

            if (exception instanceof BadCredentialsException) {
                message = "用户名或密码错误";
            } else if (exception instanceof DisabledException) {
                message = "账户被禁用，登录失败";
            } else if (exception instanceof CredentialsExpiredException) {
                message = "密码过期，登录失败";
            } else if (exception instanceof AccountExpiredException) {
                message = "账户过期，登录失败";
            } else if (exception instanceof LockedException) {
                message = "账户被锁定，登录失败";
            }

            PrintWriter out = response.getWriter();
            out.write(message);
        });

        // 设置一下 AuthenticationManager
        filter.setAuthenticationManager(authenticationManager());
        // 设置登陆路径
        filter.setFilterProcessesUrl("/login");
        // 这玩意是一定要写的，否则就不能登陆成功， session 禁用后就没效果了
        filter.setSecurityContextRepository(new HttpSessionSecurityContextRepository());

        return filter;
    }
}
````

# 另一种方式

这种方式的问题是，如果没有登陆，就一定会跳转到登陆页

```java
@RestController
@RequestMapping("/authentication")
public class AuthenticationController {

    @Autowired
    private AuthenticationManager authenticationManager;

    @PostMapping("/login")
    public String login(@RequestBody AccountUser user, HttpSession session) {
        UsernamePasswordAuthenticationToken unauthenticated = UsernamePasswordAuthenticationToken.unauthenticated(user.getUsername(), user.getPassword());
        try {
            Authentication authenticate = authenticationManager.authenticate(unauthenticated);
            SecurityContextHolder.getContext().setAuthentication(authenticate);
            // 这个必须加， session 禁用后就没效果了
            session.setAttribute(HttpSessionSecurityContextRepository.SPRING_SECURITY_CONTEXT_KEY, SecurityContextHolder.getContext());
            return "success";
        } catch (AuthenticationException e) {
            return "error:" + e.getMessage();
        }
    }
}
```

再加个配置

```java

@Configuration
public class SecurityConfig {
    @Autowired
    private SysUserServiceImpl sysUserService;

    @Bean
    public SecurityFilterChain filterChain(HttpSecurity http) throws Exception {
        http.authorizeHttpRequests((authorizeHttpRequests) ->
                // 所有的请求都要认证之后才能访问
                authorizeHttpRequests
                        .requestMatchers("/authentication/login")
                        .permitAll()
                        .anyRequest().authenticated()
        );

        http.formLogin((formLogin) ->
                formLogin
                        .usernameParameter("username")
                        .passwordParameter("password")
                        .permitAll()
        );


        // http.csrf((csrf) -> csrf.disable())
        http.csrf(AbstractHttpConfigurer::disable);

        return http.build();
    }

    // 用户1, 自定义从数据库取
    @Bean
    UserDetailsService user1() {
        return username -> {
            SysUser user = sysUserService.getById(2);
            return new User(
                    user.getUsername(),
                    "{noop}" + user.getPassword(),
                    new ArrayList<>()
            );

        };
    }

    // 用户2，自定义一个内存的用户
    @Bean
    UserDetailsService user2() {
        return new InMemoryUserDetailsManager(
                User.builder()
                        .username("u2")
                        .password("{noop}123")
                        .build()
        );
    }

    // 用户3，自定义一个内存的用户
    @Bean
    UserDetailsService user3() {
        return new InMemoryUserDetailsManager(
                User.builder()
                        .username("u3")
                        .password("{noop}123")
                        .build()
        );
    }

    // 定义 AuthenticationManager ，会被框架调用
    @Bean
    AuthenticationManager authenticationManager() {
        // 用户名密码
        // DaoAuthenticationProvider dao1 = new DaoAuthenticationProvider();
        DaoAuthenticationProvider dao1 = new DaoAuthenticationProvider();
        dao1.setUserDetailsService(user1());

        // 用户名密码
        DaoAuthenticationProvider dao2 = new DaoAuthenticationProvider();
        dao2.setUserDetailsService(user2());

        // 用户名密码
        DaoAuthenticationProvider dao3 = new DaoAuthenticationProvider();
        dao3.setUserDetailsService(user3());

        // 把 AuthenticationProvider 给 ProviderManager
        // 当系统进行身份认证操作时，就会遍历 ProviderManager 中不同的 DaoAuthenticationProvider，进而调用到不同的数据源。
        // 这个是按顺序执行的
        // ProviderManager 是 AuthenticationManager 的实现类
        return new ProviderManager(dao1, dao2, dao3);
    }
}
```

```java
public class JwtAuthenticationFilter extends BasicAuthenticationFilter {

    public JwtAuthenticationFilter(AuthenticationManager authenticationManager) {
        super(authenticationManager);
    }

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain chain) throws IOException, ServletException {
        // 暂时用全局单例替代一下
        String username = UserInfo.getInstance().getUsername();

        if (username == null) {
            super.doFilterInternal(request, response, chain);
            return;
        }

        // 由于关闭浏览器登陆就会失效，所以每次经过这个过滤器其实就会"重新登陆", 就是自己构造一个 Authentication，
        // 且把它设置到上下文中, 这样后面的过滤器看到上下文中有 Authentication，就会认为是登陆了
        //
        // 过滤器的流程是：UsernamePasswordAuthenticationFilter -> ..其他过滤器.. -> UsernamePasswordAuthenticationFilter
        // 当返回到 UsernamePasswordAuthenticationFilter 时,
        // security 通过 SecurityContextHolder.getContext().setAuthentication(..) 方法将 Authentication 保存至安全上下文
        // 这里即便是随便写一个 username 也是可以登陆的,因为验证用户名和密码的阶段已经过了，这里是设置用户名和密码的阶段
        // 所以要校验的话，就需要在生成这个 token 之前校验
        // BasicAuthenticationFilter 在 UsernamePasswordAuthenticationFilter 后面
        UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(username, null, null);
        SecurityContextHolder.getContext().setAuthentication(token);

        chain.doFilter(request, response);
    }
}
```
