# Spring Security 执行流程图步骤

[](./assets/security-授权流程.png)

1. 用户提交登录请求
   - 用户在登录页面输入用户名和密码，并提交表单。
2. 过滤器链处理请求
   - 2.1 请求到达 Spring Security 的过滤器链（Filter Chain）。
   - 2.2 UsernamePasswordAuthenticationFilter 拦截请求。
3. 创建未认证的 Authentication 对象
   - 3.1 UsernamePasswordAuthenticationFilter 读取用户提交的凭据。
   - 3.2 创建 UsernamePasswordAuthenticationToken 对象，包含用户名和密码。
4. 调用 AuthenticationManager 的 authenticate 方法
   - 4.1 UsernamePasswordAuthenticationFilter 调用 - AuthenticationManager 的 authenticate 方法，传入未认证- 的 Authentication 对象。
5. 调用 AuthenticationProvider 的 authenticate 方法
   - 5.1 AuthenticationManager 遍历所有配置的 AuthenticationProvider。
   - 5.2 找到支持当前认证请求的 AuthenticationProvider。
   - 5.3 调用 AuthenticationProvider 的 authenticate 方法。
6. 自定义认证逻辑
   - 6.1 CustomAuthenticationProvider 实现具体的认证逻辑。
   - 6.2 验证用户的凭据（例如用户名和密码）。
7. 返回认证结果
   - 7.1 认证成功后，AuthenticationProvider 返回经过认证的 Authentication 对象。
   - 7.2 包含用户的详细信息和权限信息的 Authentication 对象被返回给 AuthenticationManager。
8. 处理认证结果
   - 8.1 UsernamePasswordAuthenticationFilter 收到经过认证的 Authentication 对象。
   - 8.2 设置当前安全上下文的认证信息。
   - 8.3 重定向用户到成功页面。
9. 认证失败处理
   - 9.1 如果认证失败，抛出相应的认证异常。
   - 9.2 ExceptionTranslationFilter 处理认证异常，重定向用户到失败页面。
