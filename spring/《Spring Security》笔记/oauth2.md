# keycloak 授权登陆

像 github 之类的授权都是一样操作

## keycloak 配置

在后台创建领域(realm)和客户端(client)以及用户就可以, 记得把客户端的 Client authentication 开关打开，否则是没有 secret 的

## spring-boot 项目(客户端)

加载依赖, 这里的 security 为 6.3.4

```xml
<parent>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-starter-parent</artifactId>
    <version>3.3.5</version>
    <relativePath /> <!-- lookup parent from repository -->
</parent>

<dependency>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-starter-security</artifactId>
</dependency>
<dependency>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-starter-web</artifactId>
</dependency>
<dependency>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-starter-oauth2-client</artifactId>
</dependency>
```

application.yml 配置

```yml
server:
  port: 9090

spring:
  security:
    oauth2:
      client:
        registration:
          keycloak:
            client-id: spring-client
            client-secret: UX2r15HPxQSQNAE7R8371lIEoWF6TxQu
            scope: openid,profile,email
            # authorization-grant-type: authorization_code # 授权码模式
            # redirect-uri: "http://localhost:9090/login/oauth2/code/spring-client" # 回调地址，其实一般不需要配置
            # client-authentication-method: client_secret_basic # 默认认证方式
        provider:
          keycloak:
            issuer-uri: http://localhost:9092/realms/myrealm # 这个需要写，因为在 spring-client 在 myrealm 这个领域下
```

SecurityConfig

```java

@Configuration
@EnableWebSecurity
public class SecurityConfig {
    @Bean
    public SecurityFilterChain securityFilterChain(HttpSecurity http) throws Exception {
        http
                .authorizeHttpRequests(authorizeRequests ->
                        authorizeRequests
                                .requestMatchers("/", "/public/**").permitAll()  // 公共页面
                                .anyRequest().authenticated()  // 其他页面需要认证
                )
                .oauth2Login(Customizer.withDefaults()); // 默认登陆流程

        return http.build();
    }
}
```

oauth2Login 可以自定义

```java
http.oauth2Login(oauth2Login -> oauth2Login
    .loginPage("/custom-login")  // 自定义登录页面
    .successHandler(new OAuth2AuthenticationSuccessHandler())  // 自定义认证成功处理器
    .failureHandler(new OAuth2AuthenticationFailureHandler()))  // 自定义失败处理器
```

controller

```java
@RestController
public class TestController {
    @GetMapping("/")
    public String home() {
        return "Welcome to the Keycloak secured application!";
    }

    @GetMapping("/user")
    public String user(@AuthenticationPrincipal OidcUser oidcUser) {
        return "User Info: " + oidcUser.getAttributes();
    }
}
```

流程

- 访问`/user`
- 重定向到 keyloak 登陆页进行登陆
- 登陆成功后回到 `/user` 这个接口(中间也有很多其他重定向的流程，但是框架都默默实现了)
- 成功获取用户信息


# 作为资源服务器

配置

```java
@Configuration
@EnableWebSecurity
public class SecurityConfig {

    @Resource
    private CustomAuthenticationEntryPoint customAuthenticationEntryPoint;

    @Resource
    private AccessDeniedHandler accessDeniedHandler;

    @Bean
    public SecurityFilterChain securityFilterChain(HttpSecurity http) throws Exception {
        http
                .authorizeHttpRequests(auth -> auth
                        .requestMatchers("/public/**").permitAll()
                        .anyRequest().authenticated()
                )
                .oauth2ResourceServer(oauth2 -> oauth2.jwt(
                        jwt -> jwt.jwtAuthenticationConverter(jwtAuthenticationConverter())
                ))
                .exceptionHandling(exception ->
                        exception
                                .authenticationEntryPoint(customAuthenticationEntryPoint)
                                .accessDeniedHandler(accessDeniedHandler)
                )
                // 允许跨域,这里必须要配置上
                .cors(Customizer.withDefaults())
                .csrf(AbstractHttpConfigurer::disable)
        ;
        return http.build();
    }

    private JwtAuthenticationConverter jwtAuthenticationConverter() {
        JwtAuthenticationConverter converter = new JwtAuthenticationConverter();
        converter.setJwtGrantedAuthoritiesConverter(jwt -> {
            // 将 Keycloak 的 roles 映射到 Spring Security 的权限
            List<String> realmRoles = getRealmRoles(jwt);
            List<String> clientRoles = getAzpClientRoles(jwt);
            Stream<String> rolesStream = Stream.concat(realmRoles.stream(), clientRoles.stream());

            List<GrantedAuthority> roles = rolesStream
                    .map(role -> new SimpleGrantedAuthority("ROLE_" + role))
                    .collect(Collectors.toList());

            System.out.println("All roles: " + roles);

            return roles;
        });
        return converter;
    }

    private List<String> getRealmRoles(Jwt jwt) {
        Map<String, Object> realmAccess = jwt.getClaimAsMap("realm_access");

        if (realmAccess == null) {
            return List.of();
        }

        Object realmRolesObj = realmAccess.get("roles");

        if (realmRolesObj == null) {
            return List.of();
        }

        List<String> realmRoles = convertToListOfType(realmRolesObj, String.class);

        if (realmRoles == null) return List.of();

        return realmRoles;
    }

    /**
     * 获取授权客户端角色
     */
    private List<String> getAzpClientRoles(Jwt jwt) {
        return getClientRoles(jwt, jwt.getClaimAsString("azp"));
    }

    private List<String> getClientRoles(Jwt jwt, String client) {
        List<String> emptyList = List.of();

        if (client == null) {
            return emptyList;
        }

        Map<String, Object> resourceAccess = jwt.getClaimAsMap("resource_access");

        if (resourceAccess == null) {
            return emptyList;
        }

        Object clientMap = resourceAccess.get(client);

        if (clientMap == null) {
            return emptyList;
        }

        // 客户端角色
        Object clientRoles = ((Map<String, Object>) clientMap).get("roles");

        if (clientRoles == null) {
            return emptyList;
        }

        List<String> roles = convertToListOfType(clientRoles, String.class);

        if (roles == null) {
            return emptyList;
        }

        return roles;
    }

    private <T> List<T> convertToListOfType(Object obj, Class<T> type) {
        if (obj instanceof List<?>) {
            try {
                return ((List<?>) obj).stream()
                        .map(type::cast) // 使用 Class#cast 进行类型转换
                        .toList(); // Java 16+ 方法。如果是更早版本，可以使用 Collectors.toList()
            } catch (ClassCastException e) {
                System.out.println("无法将列表中的元素转换为 " + type.getSimpleName() + "：" + e.getMessage());
            }
        }
        return null; // 不是 List 或转换失败
    }
}   
```

AccessDeniedHandler

```java
/**
 * 权限拒绝的处理
 */
@Component
public class AccessDeniedHandler implements org.springframework.security.web.access.AccessDeniedHandler {
    @Override
    public void handle(HttpServletRequest request, HttpServletResponse response, org.springframework.security.access.AccessDeniedException accessDeniedException) throws IOException, ServletException {
        response.setContentType("application/json");
        response.setStatus(HttpServletResponse.SC_FORBIDDEN); // 403
        response.getWriter().write("{\"error\": \"Forbidden\", \"message\": \"" + accessDeniedException.getMessage() + "\"}");
    }
}
```

CustomAuthenticationEntryPoint
```java
/**
 * 没有登录的处理
 */
@Component
public class CustomAuthenticationEntryPoint implements AuthenticationEntryPoint {

    @Override
    public void commence(HttpServletRequest request, HttpServletResponse response, AuthenticationException authException) throws IOException {
        response.setContentType("application/json");
        response.setStatus(HttpServletResponse.SC_UNAUTHORIZED); // 401
        response.getWriter().write("{\"error\": \"Unauthorized\", \"message\": \"" + authException.getMessage() + "\"}");
    }
}
```
