```java
package com.example.springsecurity.config;

import com.example.springsecurity.Service.impl.UserDetailServiceImpl;
import com.example.springsecurity.filter.CustomFilter;
import com.example.springsecurity.filter.MyFilter;
import handler.MyAccessDeniedHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.security.web.authentication.rememberme.JdbcTokenRepositoryImpl;
import org.springframework.security.web.authentication.rememberme.PersistentTokenRepository;

import javax.sql.DataSource;

@Configuration
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    private DataSource dataSource;

    @Autowired
    UserDetailServiceImpl userDetailService;

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.formLogin()
                // 用户名入参
                .usernameParameter("username")
                // 密码入参
                .passwordParameter("password")
                // 自定义登陆页面
                .loginPage("/login.html")
                // 登陆接口(必须和表单提交的接口一样)
                .loginProcessingUrl("/login")
                // 登陆成功跳转的页面，必须是Post请求
                .successForwardUrl("/toMain")
                // 自定义跳转逻辑
                // .successHandler(new MyAuthenticationSuccessHandler("/user.html"))
                // 登陆失败跳转的页面，必须是Post请求
                .failureForwardUrl("/loginFail")
        // 登陆失败自定义处理器
        // .failureHandler(new MyAuthenticationFailureHandler("/login_fail.html"))
        ;

        // http.authorizeRequests()
        //         // 不需要认证
        //         .antMatchers("/login.html").permitAll()
        //         // 这种方式也可以
        //         .antMatchers("/login_fail.html").access("permitAll()")
        //         // 只要其中有一个权限就可以了
        //         .antMatchers("/admin.html").hasAnyAuthority("admin", "rol")
        //         // 这种方式也可以
        //         .antMatchers("/user.html").access("hasAnyAuthority('user')")
        //         // 除了上面放行的请求，所有的请求都必须认证
        //         .anyRequest().authenticated()
        // ;

        // http.authorizeRequests()
        //         // 放行不需要认证的接口
        //         .antMatchers("/login.html", "/login_fail.html").permitAll()
        //         // 除了上面放行的请求,自定义认证
        //         .anyRequest().access("@myServiceImpl.hasPermission(request, authentication)")
        // ;

        http.authorizeRequests()
                // 不需要认证
                .antMatchers("/login.html").permitAll()
                // 这种方式也可以
                .antMatchers("/login_fail.html").access("permitAll()")
                // 除了上面放行的请求，所有的请求都必须认证
                .anyRequest().authenticated()
        ;

        http.exceptionHandling()
                // 自定义异常处理
                .accessDeniedHandler(new MyAccessDeniedHandler())
        ;

        // http.rememberMe()
        //         // 设置数据源
        //         .tokenRepository(persistentTokenRepository())
        //         // 记住密码参数
        //         // .rememberMeParameter("")
        //         // 超时时间
        //         .tokenValiditySeconds(60)
        //         // 自定义登陆逻辑
        //         .userDetailsService(userDetailService)
        // ;

        http.logout().logoutUrl("logout");

        http
                .addFilter(new CustomFilter(authenticationManager()))
                .addFilterBefore(new MyFilter(), UsernamePasswordAuthenticationFilter.class)
        ;

        http.csrf().disable();
    }

    public PersistentTokenRepository persistentTokenRepository() {
        System.out.println("persistentTokenRepository");
        JdbcTokenRepositoryImpl jdbcTokenRepository = new JdbcTokenRepositoryImpl();
        // 设置数据源
        jdbcTokenRepository.setDataSource(dataSource);
        // 自动建表,第一次启动开启，第二次启动注释掉
        // 有点傻...
        jdbcTokenRepository.setCreateTableOnStartup(true);
        return jdbcTokenRepository;
    }

}
```

自定义登陆逻辑

```java
package com.example.springsecurity.Service.impl;

import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

// 自定义登陆逻辑
@Service
public class UserDetailServiceImpl implements UserDetailsService {
    // 自定义登陆逻辑，每次登陆都会执行此逻辑
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        if (!username.equals("admin")) {
            throw new UsernameNotFoundException("用户名不存在");
        }
        BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder();
        // 这里必须要加上 {bcrypt} 前缀
        // 但是自动装配的时候就不需要，不知道为啥
        String password = "{bcrypt}" + bCryptPasswordEncoder.encode("123");

        return new User(
                username,
                password,
                // 权限
                AuthorityUtils.commaSeparatedStringToAuthorityList("admin,normal,ROLE_abc")
        );
    }
}
```

过滤器

```java
public class MyFilter extends OncePerRequestFilter {
    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
        System.out.println("自定义过滤器 MyFilter");
        filterChain.doFilter(request, response);
    }
}
```
