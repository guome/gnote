# log4j2 的各种包

Log4j2 提供了多个模块和包，以支持不同的功能和使用场景。下面是一些常用的 Log4j2 包及其功能：

### 1. **log4j-api**

- **描述**：Log4j2 的核心 API 包，提供了日志记录的基本接口。
- **用途**：定义了用于记录日志的 API，开发者通过这些 API 与 Log4j2 进行交互。
- **常用类**：
  - `org.apache.logging.log4j.Logger`：日志记录的主要接口。
  - `org.apache.logging.log4j.LogManager`：用于获取 `Logger` 实例。
- **Maven 依赖**：
  ```xml
  <dependency>
      <groupId>org.apache.logging.log4j</groupId>
      <artifactId>log4j-api</artifactId>
      <version>2.x.x</version>
  </dependency>
  ```

### 2. **log4j-core**

- **描述**：Log4j2 的核心实现包，提供了所有的日志记录逻辑和实现。
- **用途**：包含了 Log4j2 的具体实现，支持日志输出、Appender、Layout、Filter 等功能。
- **常用类**：
  - `org.apache.logging.log4j.core.Appender`：日志输出器。
  - `org.apache.logging.log4j.core.Layout`：日志格式化的布局。
  - `org.apache.logging.log4j.core.LoggerContext`：用于管理 `Logger` 和 `Appender`。
- **Maven 依赖**：
  ```xml
  <dependency>
      <groupId>org.apache.logging.log4j</groupId>
      <artifactId>log4j-core</artifactId>
      <version>2.x.x</version>
  </dependency>
  ```

### 3. **log4j-slf4j-impl**

- **描述**：提供了 SLF4J 的实现，允许使用 SLF4J API 来记录日志，而实际的日志实现由 Log4j2 提供。
- **用途**：如果你的项目中使用了 SLF4J 来记录日志，但希望 Log4j2 作为实际的日志实现，可以使用这个包。
- **Maven 依赖**：
  ```xml
  <dependency>
      <groupId>org.apache.logging.log4j</groupId>
      <artifactId>log4j-slf4j-impl</artifactId>
      <version>2.x.x</version>
  </dependency>
  ```

### 4. **log4j-jul**

- **描述**：提供了 Java Util Logging (JUL) 的桥接器，允许将 JUL 日志通过 Log4j2 进行输出。
- **用途**：如果你的项目中使用了 Java 的标准日志库（JUL），可以使用此包将其桥接到 Log4j2。
- **Maven 依赖**：
  ```xml
  <dependency>
      <groupId>org.apache.logging.log4j</groupId>
      <artifactId>log4j-jul</artifactId>
      <version>2.x.x</version>
  </dependency>
  ```

### 5. **log4j-2.x API & Tools (log4j-api-tools)**

- **描述**：为 Log4j2 提供了一些工具类和额外的 API，例如 JSON 格式的日志解析、性能监控等。
- **用途**：提供了用于配置和管理日志系统的其他工具。
- **Maven 依赖**：
  ```xml
  <dependency>
      <groupId>org.apache.logging.log4j</groupId>
      <artifactId>log4j-api-tools</artifactId>
      <version>2.x.x</version>
  </dependency>
  ```

### 6. **log4j-web**

- **描述**：为 Web 环境提供的支持包，主要是 Servlet 环境下使用 Log4j2。
- **用途**：使 Web 应用程序能够使用 Log4j2 进行日志记录，支持 Web 容器中的配置和日志管理。
- **Maven 依赖**：
  ```xml
  <dependency>
      <groupId>org.apache.logging.log4j</groupId>
      <artifactId>log4j-web</artifactId>
      <version>2.x.x</version>
  </dependency>
  ```

### 7. **log4j-flume**

- **描述**：提供与 Flume 集成的支持包，Flume 是一个用于流数据收集的服务。
- **用途**：如果你需要将 Log4j2 的日志数据发送到 Flume，可以使用这个包进行集成。
- **Maven 依赖**：
  ```xml
  <dependency>
      <groupId>org.apache.logging.log4j</groupId>
      <artifactId>log4j-flume</artifactId>
      <version>2.x.x</version>
  </dependency>
  ```

### 8. **log4j-plugins**

- **描述**：Log4j2 的插件支持包，允许通过插件机制扩展 Log4j2 的功能。
- **用途**：用于为 Log4j2 添加额外的功能，比如自定义 Appender、Layout、Filter 等。
- **Maven 依赖**：
  ```xml
  <dependency>
      <groupId>org.apache.logging.log4j</groupId>
      <artifactId>log4j-plugins</artifactId>
      <version>2.x.x</version>
  </dependency>
  ```

### 9. **log4j-spring-boot**

- **描述**：用于与 Spring Boot 集成的 Log4j2 包。
- **用途**：如果你在 Spring Boot 应用程序中使用 Log4j2，可以使用这个包来进行配置和集成。
- **Maven 依赖**：
  ```xml
  <dependency>
      <groupId>org.apache.logging.log4j</groupId>
      <artifactId>log4j-spring-boot</artifactId>
      <version>2.x.x</version>
  </dependency>
  ```

### 常见的 Maven 依赖组合

为了在项目中使用 Log4j2，你通常会依赖以下两个核心包：

```xml
<dependency>
    <groupId>org.apache.logging.log4j</groupId>
    <artifactId>log4j-api</artifactId>
    <version>2.x.x</version>
</dependency>

<dependency>
    <groupId>org.apache.logging.log4j</groupId>
    <artifactId>log4j-core</artifactId>
    <version>2.x.x</version>
</dependency>
```

### 总结

Log4j2 提供了丰富的模块支持各种不同的场景，包括核心功能、与 SLF4J、JUL、Web 集成等。具体的依赖包选择会根据你的使用场景来确定，最常见的包是 `log4j-api` 和 `log4j-core`，它们提供了基本的日志记录功能。

# SLF4J 是什么

### **SLF4J (Simple Logging Facade for Java)**

SLF4J 是一个简单的日志门面（Facade），它为 Java 提供了一个统一的日志 API。SLF4J 本身不实现日志记录功能，而是通过将日志请求转发到具体的日志实现框架（例如 Log4j、Logback、Java Util Logging 等）来实现日志功能。换句话说，SLF4J 作为日志的接口层，允许开发者在应用程序中统一使用日志 API，而具体的日志实现可以根据需求灵活替换。

### **SLF4J 的特点**

1. **抽象日志 API**：SLF4J 提供了一个通用的日志接口，让开发者的应用程序可以不依赖于具体的日志框架。
2. **与多种日志框架兼容**：SLF4J 本身不进行日志输出，而是与多个日志框架（如 Log4j、Logback）兼容，并通过绑定的方式将日志请求转发给具体的框架。
3. **灵活的日志实现**：你可以选择适合你项目需求的具体日志框架，并且可以在项目中方便地切换日志实现。

### **SLF4J 的工作原理**

- SLF4J 提供了标准化的 API，但它没有日志输出的实际实现。
- 你需要在项目中引入具体的日志框架（如 Log4j、Logback 等）作为 SLF4J 的实现。
- SLF4J 会通过 **绑定** 机制与具体的日志框架结合。例如，使用 SLF4J 和 Log4j2 一起工作时，需要引入 `log4j-slf4j-impl` 包，SLF4J 会将日志请求转发给 Log4j2 来处理。

### **SLF4J 与 Log4j2 集成**

如果你选择 Log4j2 作为日志框架，可以通过 SLF4J 来进行日志记录。你需要添加 SLF4J API 和 Log4j2 的 SLF4J 实现。

### **如何在 Maven 中引用 SLF4J 和 Log4j2**

1. **引用 SLF4J API**
   在项目中引用 SLF4J API，以便使用它提供的统一日志接口。

   ```xml
   <dependency>
       <groupId>org.slf4j</groupId>
       <artifactId>slf4j-api</artifactId>
       <version>2.x.x</version> <!-- 适合你的版本 -->
   </dependency>
   ```

2. **引用 Log4j2 实现（SLF4J 与 Log4j2）**
   如果你使用 SLF4J 和 Log4j2，你还需要引入 Log4j2 的 SLF4J 实现包。这个包将 SLF4J 的日志请求转发到 Log4j2。

   ```xml
   <dependency>
       <groupId>org.apache.logging.log4j</groupId>
       <artifactId>log4j-slf4j-impl</artifactId>
       <version>2.x.x</version> <!-- 使用适合你的 Log4j2 版本 -->
   </dependency>
   ```

3. **引用 Log4j2 核心包**
   如果你还没有引入 Log4j2 的核心实现包，需要添加 Log4j2 的核心包。

   ```xml
   <dependency>
       <groupId>org.apache.logging.log4j</groupId>
       <artifactId>log4j-core</artifactId>
       <version>2.x.x</version>
   </dependency>
   ```

### **完整的 Maven 依赖配置示例**

```xml
<dependencies>
    <!-- SLF4J API -->
    <dependency>
        <groupId>org.slf4j</groupId>
        <artifactId>slf4j-api</artifactId>
        <version>2.x.x</version> <!-- 使用合适的 SLF4J 版本 -->
    </dependency>

    <!-- Log4j2 核心包 -->
    <dependency>
        <groupId>org.apache.logging.log4j</groupId>
        <artifactId>log4j-core</artifactId>
        <version>2.x.x</version> <!-- 使用合适的 Log4j2 版本 -->
    </dependency>

    <!-- Log4j2 的 SLF4J 绑定包 -->
    <dependency>
        <groupId>org.apache.logging.log4j</groupId>
        <artifactId>log4j-slf4j-impl</artifactId>
        <version>2.x.x</version> <!-- 使用合适的 Log4j2 版本 -->
    </dependency>
</dependencies>
```

### **SLF4J 日志记录示例**

一旦你配置好了 SLF4J 和 Log4j2，你就可以在代码中使用 SLF4J API 来记录日志了。以下是一个简单的日志记录示例：

```java
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class MyApplication {
    // 获取日志记录器
    private static final Logger logger = LoggerFactory.getLogger(MyApplication.class);

    public static void main(String[] args) {
        // 记录不同级别的日志
        logger.debug("This is a debug message.");
        logger.info("This is an info message.");
        logger.warn("This is a warning message.");
        logger.error("This is an error message.");
    }
}
```

在这个示例中，`LoggerFactory.getLogger()` 用于创建日志记录器，`logger.debug()`, `logger.info()` 等方法用于记录日志。

### **总结**

- **SLF4J** 是一个日志门面，它提供了一个统一的日志 API，可以与多个日志框架一起使用。
- 在使用 SLF4J 时，你需要选择一个具体的日志实现框架（如 Log4j2、Logback）。
- 通过在项目中引入相应的 SLF4J 和日志框架的实现包，你可以使用 SLF4J 来进行日志记录，并将日志请求转发给具体的日志框架（如 Log4j2）进行处理。
- 可以认为, **SLF4J** 就是一个接口

# SLF4J 与 Spring 的关系

### **Spring Framework 中的 SLF4J 支持**

**Spring Framework** 本身也内置了对 **SLF4J** 的支持，并使用 SLF4J 作为日志门面（Logging Facade）。这意味着 Spring Framework 提供的所有日志记录功能，实际上是通过 SLF4J API 来调用的，具体的日志实现可以是 **Logback**、**Log4j** 或其他支持 SLF4J 的框架。

然而，Spring Framework 并没有强制使用某个具体的日志框架，它允许开发者选择适合的日志实现。SLF4J 是 Spring 推荐的日志接口，因为它的灵活性和与其他日志框架的兼容性。

#### **Spring Framework 和 SLF4J**

- **SLF4J 是 Spring 的默认日志门面**，这意味着在 Spring 中，日志记录使用的是 SLF4J 提供的接口。
- **Spring 使用 Logback 或其他日志框架**：默认情况下，Spring Framework 的日志实现使用 **Logback**（Spring 3.2 之后）。但 Spring 本身并不强制绑定具体的日志框架，而是通过 SLF4J API 进行日志记录，因此你可以在项目中切换使用不同的日志框架，如 **Log4j2** 或 **Java Util Logging**。

### **Spring 中日志记录的工作方式**

- Spring 使用 SLF4J 提供的日志接口，代码中通过 `Logger` 对象进行日志记录。
- **具体的日志实现** 由项目中引入的依赖决定。例如，如果你引入了 Logback 相关依赖，那么 Spring 就会通过 Logback 来实现日志记录。
- **日志实现切换**：你可以在 Spring 中很方便地切换日志框架，只需更换对应的日志框架依赖。

### **如何在 Spring Framework 中配置 SLF4J**

如果你使用的是 **Spring Framework**（而非 Spring Boot），你依然可以利用 SLF4J，但需要手动配置日志框架。以下是如何配置 SLF4J 与 Logback 或 Log4j2 的示例。

### **配置 SLF4J 和 Logback**

默认情况下，Spring 会使用 Logback，但你需要确保项目中引入了相关的依赖。

1. **引入 SLF4J 和 Logback 依赖**：

   ```xml
   <dependencies>
       <!-- SLF4J API -->
       <dependency>
           <groupId>org.slf4j</groupId>
           <artifactId>slf4j-api</artifactId>
           <version>2.x.x</version>
       </dependency>

       <!-- Logback 核心包 -->
       <dependency>
           <groupId>ch.qos.logback</groupId>
           <artifactId>logback-classic</artifactId>
           <version>1.x.x</version>
       </dependency>

       <!-- Logback 配置文件 -->
       <dependency>
           <groupId>ch.qos.logback</groupId>
           <artifactId>logback-core</artifactId>
           <version>1.x.x</version>
       </dependency>
   </dependencies>
   ```

2. **在 `logback.xml` 中配置日志级别和输出格式**：

   ```xml
   <configuration>
       <appender name="stdout" class="ch.qos.logback.core.ConsoleAppender">
           <encoder>
               <pattern>%d{yyyy-MM-dd HH:mm:ss} - %msg%n</pattern>
           </encoder>
       </appender>

       <root level="debug">
           <appender-ref ref="stdout"/>
       </root>
   </configuration>
   ```

### **配置 SLF4J 和 Log4j2**

如果你想使用 **Log4j2** 替代 Logback，步骤如下：

1. **引入 SLF4J 和 Log4j2 依赖**：

   ```xml
   <dependencies>
       <!-- SLF4J API; 应该是不需要引入的 -->
       <!-- <dependency>
           <groupId>org.slf4j</groupId>
           <artifactId>slf4j-api</artifactId>
           <version>2.x.x</version>
       </dependency> -->

       <!-- Log4j2 核心包 -->
       <dependency>
           <groupId>org.apache.logging.log4j</groupId>
           <artifactId>log4j-core</artifactId>
           <version>2.x.x</version>
       </dependency>

       <!-- Log4j2 SLF4J 绑定 -->
       <dependency>
           <groupId>org.apache.logging.log4j</groupId>
           <artifactId>log4j-slf4j-impl</artifactId>
           <version>2.x.x</version>
       </dependency>
   </dependencies>
   ```

2. **在 `log4j2.xml` 中配置日志级别和输出格式**：

   ```xml
   <?xml version="1.0" encoding="UTF-8"?>
   <Configuration status="WARN">
       <Appenders>
           <Console name="Console" target="SYSTEM_OUT">
               <PatternLayout pattern="%d{yyyy-MM-dd HH:mm:ss} - %msg%n"/>
           </Console>
       </Appenders>

       <Loggers>
           <Root level="debug">
               <AppenderRef ref="Console"/>
           </Root>
       </Loggers>
   </Configuration>
   ```

### **总结**

- **Spring Framework 内置了 SLF4J**，并推荐使用 SLF4J 作为日志门面（Facade），以便实现与具体日志框架（如 Logback、Log4j2）之间的解耦。
- 默认情况下，Spring 使用 **Logback** 作为日志框架，但你可以很方便地切换到其他日志框架。
- 你可以通过在 `pom.xml` 中引入所需的日志实现依赖，来配置 Spring 使用不同的日志框架。
